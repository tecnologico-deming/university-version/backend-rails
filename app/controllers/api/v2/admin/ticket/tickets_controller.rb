class Api::V2::Admin::Ticket::TicketsController < ApplicationController

  def create
    ticket = Admin::Support::Ticket.create!(
      period_id: params[:period_id],
      type_id: params[:type_id],
      created_by_id: params[:created_by_id],
      status: params[:status],
      title: params[:title],
      comments: params[:comments],
      created_at: params[:created_at]
    )

    Admin::Support::TicketAcademicDetail.create!(
      created_by_id: params[:created_by_id],
      ticket_id: ticket.id,
      family_description: params[:family_description],
      educative_description: params[:educative_description]
    )
  end


end
