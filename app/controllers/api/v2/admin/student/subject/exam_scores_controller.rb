class Api::V2::Admin::Student::Subject::ExamScoresController < ApplicationController

  def update_exam_scores
    @exam = Admin::Academic::Student::Exam::Regular.find_by(
      period_id: params[:period_id],
      student_id: params[:student_id],
      subject_id: params[:subject_id]
    )

    @exam.update(
      score: params[:score],
      teacher_survey_complete: true,
      dead_line: params[:dead_line],
      delayed_date: params[:delayed_date]
    )
  end


end
