class Api::V2::Admin::Student::Subject::ContentResponsesController < ApplicationController

  def create
    mod = Admin::Academic::Subject::Module.find(params[:module_id])
    content = mod.contents.first

    Admin::Academic::Student::Subject::ContentResponse.create!(
      period_id: params[:period_id],
      group_id: params[:group_id],
      subject_id: params[:subject_id],
      module_id: params[:module_id],
      content_id: content.id,
      student_id: params[:student_id],
      score: params[:score],
      created_at: params[:created_at]
    )

  end

end
