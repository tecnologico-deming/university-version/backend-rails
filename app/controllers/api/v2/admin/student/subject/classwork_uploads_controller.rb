class Api::V2::Admin::Student::Subject::ClassworkUploadsController < ApplicationController

  def create
    Admin::Academic::Student::Subject::ClassworkUpload.create!(
      id: params[:id],
      period_id: 11,
      subject_id: params[:subject_id],
      teacher_id: params[:teacher_id],
      student_id: params[:student_id],
      group_id: params[:group_id],
      classwork_id: params[:classwork_id],
      score: params[:score],
      comment: params[:comment],
      file: params[:file],
      created_at: params[:created_at]
    )
  end


end
