class Api::V2::Admin::Student::Subject::ContentsController < ApplicationController

  def create
    student = Admin::User::User.find_by(identification_number: params[:student_identification_number])
    student_id = student.present? ? student.id : User.where(role_id: Admin::User::Role.find_by(name: 'Estudiante').id).first.id

    teacher = Admin::User::User.find_by(identification_number: params[:teacher_identification_number])
    teacher_id = teacher.present? ? teacher.id : nil

    Admin::Academic::Student::Subject::Class.find_or_create_by!(
      activation_id: 0,
      period_id: 5,
      subject_id: params[:subject_id],
      group_id: params[:group_id],
      teacher_id: teacher_id,
      student_id: student_id
    )

    Admin::Academic::Student::Subject::ContentUpload.find_or_create_by!(
      student_id: student_id,
      teacher_id: teacher_id,
      period_id: 5,
      group_id: params[:group_id],
      subject_id: params[:subject_id],
      module_name: params[:module_name],
      content_type_name: params[:content_type_name],
      score: params[:score],
      teacher_comment: params[:teacher_comment],
      created_at: params[:created_at],
      updated_at: params[:updated_at]
    )

    # @module = Admin::Academic::Subject::Module.find(params[:module_id].to_i)
    # @content = @module.contents.first.id
    # @file = Admin::Academic::Student::Subject::ContentUpload.create!(
    #   id: params[:id].to_i,
    #   period_id: params[:period_id].to_i,
    #   group_id: params[:group_id].to_i,
    #   subject_id: params[:subject_id].to_i,
    #   module_id: params[:module_id].to_i,
    #   content_id: @content,
    #   content_type_id: params[:content_type_id].to_i,
    #   teacher_id: params[:teacher_id].to_i,
    #   student_id: params[:student_id].to_i,
    #   score: params[:score].to_f,
    #   teacher_comment: params[:teacher_comment],
    #   file: params[:file],
    #   created_at: params[:created_at]
    # )

    # @content.set_order(@module, @content)
    # if @file.save
    #   render json: { message: 'Successfully created' }, status: :ok
    # else
    #   render json: { message: 'Error' }, status: :unprocessable_entity
    # end
  end

  def content_params
    params.permit(
      :period_id,
      :module_id,
      :type_id,
      :is_active,
      :name,
      :file
    )
  end


end
