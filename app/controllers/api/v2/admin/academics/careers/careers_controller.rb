class Api::V2::Admin::Academics::Careers::CareersController < ApplicationController

  def index
    @careers = Admin::Academic::Career::Career.where(is_active: true).order(name: :asc)
    if @careers.present?
      render json: Admin::Academic::Career::CareerSerializer.new(@careers), status: :ok
    else
      render json: { message: 'No careers found' }, status: :unprocessable_entity
    end
  end

end
