class Api::V2::Admin::Academics::Subjects::ContentQuestionsController < ApplicationController

  def create
    mod = Admin::Academic::Subject::Module.find(params[:module_id])
    content = mod.contents.first

    Admin::Academic::Subject::ContentQuestion.create!(
      id: params[:id],
      created_by_id: 1,
      period_id: 11,
      content_id: content.id,
      title: params[:title],
      is_active: params[:status]
    )
  end


  private

  def question_params
    params.require(:admin_academic_subject_content_question).permit(
        :created_by_id,
        :period_id,
        :content_id,
        :is_active,
        :title,
        answers_attributes: %i[
        id content_question_id title is_correct _destroy
      ]
    )
  end

end
