class Api::V2::Admin::Academics::Subjects::GuidesController < ApplicationController

  def create
    Admin::Academic::Subject::Guide.create!(
      created_by_id: 1,
      id: params[:id],
      period_id: 11,
      is_active: params[:is_active],
      subject_id: params[:subject_id],
      file: params[:file]
    )
  end

end
