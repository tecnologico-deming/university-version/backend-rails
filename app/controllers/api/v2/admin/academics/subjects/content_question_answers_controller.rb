class Api::V2::Admin::Academics::Subjects::ContentQuestionAnswersController < ApplicationController

  def create

    Admin::Academic::Subject::ContentQuestionAnswer.create!(
      id: params[:id],
      is_active: params[:is_active],
      content_question_id: params[:content_question_id],
      title: params[:title],
      is_correct: params[:is_correct]
    )

  end


  private

  def question_params
    params.require(:admin_academic_subject_content_question).permit(
        :created_by_id,
        :period_id,
        :content_id,
        :is_active,
        :title,
        answers_attributes: %i[
        id content_question_id title is_correct _destroy
      ]
    )
  end

end
