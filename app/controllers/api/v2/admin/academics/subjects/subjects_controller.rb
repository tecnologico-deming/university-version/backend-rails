class Api::V2::Admin::Academics::Subjects::SubjectsController < ApplicationController

  def index
    @subjects = Admin::Academic::Subject::Subject.order(name: :asc)
    if @subjects.present?
      render json: Admin::Academic::Career::SubjectSerializer.new(@subjects), status: :ok
    else
      render json: { message: 'No subjects found' }, status: :unprocessable_entity
    end
  end

end
