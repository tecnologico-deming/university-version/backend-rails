class Api::V2::Admin::Academics::Subjects::ExamQuestionsController < ApplicationController

  def create
    Admin::Academic::Subject::ExamQuestion.create!(
      id: params[:id],
      created_by_id: 1,
      exam_id: params[:exam_id],
      title: params[:title],
      is_active: params[:is_active]
    )
  end

end
