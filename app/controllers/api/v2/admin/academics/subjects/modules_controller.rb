class Api::V2::Admin::Academics::Subjects::ModulesController < ApplicationController

  def create
    @subject = Admin::Academic::Subject::Subject.find(params[:subject_id].to_i)
    @module = Admin::Academic::Subject::Module.new(module_params)
    @module.id = params[:id]
    @module.subject_id = @subject.id
    @module.created_by_id = 1
    @module.period_id = 11
    @module.is_active = true
    @module.set_order(@subject, @module)
    if @module.save
      render json: { message: 'Successfully created' }, status: :ok
    else
      render json: { message: 'Error' }, status: :ok
    end
  end

  private

  def module_params
    params.permit(
      :created_by_id,
      :subject_id,
      :period_id,
      :is_active,
      :order,
      :name
    )
  end

end
