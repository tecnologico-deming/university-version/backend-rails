class Api::V2::Admin::Academics::Subjects::ContentsController < ApplicationController

  def create
    student = Admin::User::User.find_by(identification_number: params[:student_identification_number])
    student_id = student.present? ? student.id : User.where(role_id: Admin::User::Role.find_by(name: 'Estudiante').id).first.id

    teacher = Admin::User::User.find_by(identification_number: params[:teacher_identification_number])
    teacher_id = teacher.present? ? teacher.id : User.where(role_id: Admin::User::Role.find_by(name: 'Profesor').id).first.id

    Admin::Academic::Student::Subject::Class.find_or_create_by!(
      activation_id: 0,
      period_id: 5,
      subject_id: params[:subject_id],
      group_id: params[:group_id],
      teacher_id: teacher_id,
      student_id: student_id
    )

    Admin::Academic::Student::Subject::ContentUpload.find_or_create_by!(
      student_id: student_id,
      teacher_id: teacher_id,
      period_id: 5,
      group_id: params[:group_id],
      subject_id: params[:subject_id],
      module_name: params[:module_name],
      content_type_name: params[:content_type_name],
      score: params[:score],
      teacher_comment: params[:teacher_comment],
      created_at: params[:created_at],
      updated_at: params[:updated_at]
    )


    # @module = Admin::Academic::Subject::Module.find(params[:module_id].to_i)
    # # @content = Admin::Academic::Subject::Content.new(content_params)
    # @content = Admin::Academic::Subject::Content.create!(
    #   id: params[:id].to_i,
    #   created_by_id: 1,
    #   period_id: 11,
    #   module_id: @module.id,
    #   name: params[:name],
    #   type_id: params[:type_id].to_i,
    #   is_active: params[:status],
    #   file: params[:file]
    # )
    #
    # # @content.set_order(@module, @content)
    # if @content.save
    #   render json: { message: 'Successfully created' }, status: :ok
    # else
    #   render json: { message: 'Error' }, status: :unprocessable_entity
    # end
  end

  def content_params
    params.permit(
      :created_by_id,
      :period_id,
      :module_id,
      :type_id,
      :is_active,
      :name,
      :file
    )
  end


end
