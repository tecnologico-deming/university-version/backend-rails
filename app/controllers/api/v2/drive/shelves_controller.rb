class Api::V2::Drive::ShelvesController < ApplicationController

  def index
    @shelves = Admin::Drive::Shelf.where(department_id: params[:department_id], is_active: true).order(name: :asc)
    if @shelves.present?
      render json: Admin::Drive::ShelfSerializer.new(@shelves), status: :ok
    else
      render json: { message: 'No shelves found' }, status: :unprocessable_entity
    end
  end

  def scan_qr
    @shelf = Admin::Drive::Shelf.find params[:id]

    if @shelf.present?
      render json: Admin::Drive::ShelfSerializer.new(@shelf), status: :ok
    else
      render json: { message: 'No shelf found' }, status: :unprocessable_entity
    end
  end


end
