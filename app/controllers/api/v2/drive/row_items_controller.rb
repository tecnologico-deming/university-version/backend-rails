class Api::V2::Drive::RowItemsController < ApplicationController

  def index
    @department = Admin::Drive::Department.find_by(id: params[:department_id])
    @row = Admin::Drive::Row.find_by(id: params[:row_id])
    if @department.name == 'Academico' || @department.name == 'Talento Humano'
      @users = Admin::User::User.where(row_id: params[:row_id])
      render json: Admin::Drive::UserSerializer.new(@users), status: :ok
    elsif @department.name == 'Biblioteca'
      @books = Admin::Library::Book::Book.where(row_id: params[:row_id])
      render json: Admin::Drive::BookSerializer.new(@books), status: :ok
    else
      render json: { message: 'No data found' }, status: :unprocessable_entity
    end
  end

end
