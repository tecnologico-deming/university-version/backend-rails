class Api::V2::Drive::DepartmentsController < ApplicationController

  def index
    @departments = Admin::Drive::Department.where(is_active: true).order(name: :asc)
    if @departments.present?
      render json: Admin::Drive::DepartmentSerializer.new(@departments), status: :ok
    else
      render json: { message: 'No departments found' }, status: :unprocessable_entity
    end
  end

end
