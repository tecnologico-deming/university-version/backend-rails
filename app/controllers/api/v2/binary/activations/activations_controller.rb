class Api::V2::Binary::Activations::ActivationsController < ApplicationController

  before_action :set_activation, only: %i[show edit update destroy]

  resource_description do
    formats ['json']
    api_version "desarrollo"
  end

  api :POST, '/v2/binary/activations/activations', 'Activacion de materias a estudiantes y profesores'
  param :admin_academic_binary_activation, Hash, desc: "Path principal", required: true do
    param :subject_id, Integer, desc: 'ID de la materia', required: true
    param :studymode_id, Integer, desc: 'ID de la modalidad de estudio', required: true
    param :group_name, String, desc: 'Nombre del grupo (tçodo en mayusculas)', required: true
    param :student_identification_number, String, desc: 'Cedula del estudiante', required: true
    param :teacher_identification_number, String, desc: 'Cedula del docente', required: true
    param :student_exam_date, Date, desc: 'Fecha de examen', required: true
    param :student_extension_exam_date, Date, desc: 'Fecha del supletorio', required: true
    param :student_due_date, Date, desc: 'Fecha de cierre de la materia para estudiante', required: true
    param :teacher_due_date, Date, desc: 'Fecha de cierre de la materia para estudiante', required: true
    param :dates_attributes, Hash, desc: 'Path de fechas de las tutorias', required: true do
      param "1", Hash, desc: 'El 1 representa el path, por cada fecha tiene que incrementar (1, 2 y asi sucesivamente)', required: true do
        param :date, Date, desc: 'La fecha de la tutoria', required: true
      end
    end
  end
  def create
    @error_message = nil
    @activation = Admin::Academic::Binary::Activation.new(activation_params)
    @activation.period_id = current_period.id

    validate_data
    if @error_message.nil?
      if @activation.save
        @activation.group_classes(current_period, @activation)
        @activation.student_activations(current_period)
        @activation.teacher_activations(current_period)
        @activation.principal_surveys(current_period)
        render json: Api::Binary::ActivationSerializer.new(@activation).serialized_json, status: :ok
      end
    else
      render json: { message: @error_message }, status: :unprocessable_entity
    end
  end

  api :PUT, '/v2/binary/activations/activations/:id', 'Update de activacion de materias a estudiantes y profesores'
  param :admin_academic_binary_activation, Hash, desc: "Path principal", required: true do
    param :subject_id, Integer, desc: 'ID de la materia', required: true
    param :studymode_id, Integer, desc: 'ID de la modalidad de estudio', required: true
    param :group_name, String, desc: 'Nombre del grupo (tçodo en mayusculas)', required: true
    param :student_identification_number, String, desc: 'Cedula del estudiante', required: true
    param :teacher_identification_number, String, desc: 'Cedula del docente', required: true
    param :student_exam_date, Date, desc: 'Fecha de examen', required: true
    param :student_extension_exam_date, Date, desc: 'Fecha del supletorio', required: true
    param :student_due_date, Date, desc: 'Fecha de cierre de la materia para estudiante', required: true
    param :teacher_due_date, Date, desc: 'Fecha de cierre de la materia para estudiante', required: true
    param :dates_attributes, Hash, desc: 'Path de fechas de las tutorias', required: true do
      param "1", Hash, desc: 'El 1 representa el path, por cada fecha tiene que incrementar (1, 2 y asi sucesivamente)', required: true do
        param :date, Date, desc: 'La fecha de la tutoria', required: true
      end
    end
  end
  def update
    @error_message = nil
    validate_data
    if @activation && @error_message.nil?
      @activation.dates.delete_all
      if @activation.update(activation_params)
        @activation.group_classes(current_period, @activation)
        @activation.student_activations(current_period)
        @activation.teacher_activations(current_period)
        @activation.principal_surveys(current_period)
        render json: Api::Binary::ActivationSerializer.new(@activation).serialized_json, status: :ok
      end
    else
      render json: { message: @error_message }, status: :unprocessable_entity
    end
  end

  api :DELETE, '/v2/binary/activations/activations/:id', 'Borrar activacion'
  def destroy
    @activation.destroy
    @activation.remove_teacher_related_data
    render json: { message: 'Successfully updated' }, status: :ok
  end


  private

  def set_activation
    @activation = Admin::Academic::Binary::Activation.find(params[:id])
  end

  def validate_data
    @group = Admin::Academic::Group::Group.find_by(name: params[:admin_academic_binary_activation][:group_name])
    @subject = Admin::Academic::Subject::Subject.find_by(id: params[:admin_academic_binary_activation][:subject_id])
    @teacher = Admin::User::User.find_by(identification_number: params[:admin_academic_binary_activation][:teacher_identification_number])
    @student = Admin::User::User.find_by(identification_number: params[:admin_academic_binary_activation][:student_identification_number])

    if !@teacher && !@student && !@group && !@subject
      @error_message = 'No existe docente, estudiante, materia y grupo'
      return
    elsif !@teacher && !@student
      @error_message = 'No existe docente, no existe estudiante'
      return
    elsif !@activation.valid?
      @error_message = @activation.errors.messages
      return
    end

    @error_message = 'Group not found' unless @group
    @error_message = 'Subject not found' unless @subject
    @error_message = 'Teacher not found' unless @teacher
    @error_message = 'Student not found' unless @student
  end

  def_param_group :admin_academic_binary_activation do
    param :admin_academic_binary_activation, Hash, desc: "Path principal", required: true do
      param :subject_id, Integer, desc: 'ID de la materia', required: true
      param :studymode_id, Integer, desc: 'ID de la modalidad de estudio', required: true
      param :group_name, String, desc: 'Nombre del grupo (tçodo en mayusculas)', required: true
      param :student_identification_number, String, desc: 'Cedula del estudiante', required: true
      param :teacher_identification_number, String, desc: 'Cedula del docente', required: true
      param :student_exam_date, Date, desc: 'Fecha de examen', required: true
      param :student_extension_exam_date, Date, desc: 'Fecha del supletorio', required: true
      param :student_due_date, Date, desc: 'Fecha de cierre de la materia para estudiante', required: true
      param :teacher_due_date, Date, desc: 'Fecha de cierre de la materia para estudiante', required: true
      param :dates_attributes, Hash, desc: 'Path de fechas de las tutorias', required: true do
        param "1", Hash, desc: 'El 1 representa el path, por cada fecha tiene que incrementar (1, 2 y asi sucesivamente)', required: true do
          param :date, Date, desc: 'La fecha de la tutoria', required: true
        end
      end
    end
  end


  def activation_params
    params.require(:admin_academic_binary_activation).permit(
      :id,
      :period_id,
      :subject_id,
      :studymode_id,
      :group_name,
      :student_identification_number,
      :teacher_identification_number,
      :student_exam_date,
      :student_extension_exam_date,
      :student_due_date,
      :teacher_due_date,
      dates_attributes: %i[id activation_id date _destroy]
    )
  end

end

