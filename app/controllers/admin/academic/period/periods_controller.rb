class Admin::Academic::Period::PeriodsController < BaseDashboardController

  before_action :set_period, only: %i[show edit update destroy]

  def index
    @pagy, @periods = pagy(Admin::Academic::Period::Period.all.order(start_date: :desc), items: 5)
  end

  def new
    @period = Admin::Academic::Period::Period.new
  end

  def create
    @period = Admin::Academic::Period::Period.new(period_params)
    @period.created_by_id = current_user.id
    @period.is_active = true
    if @period.save
      redirect_to admin_academic_period_periods_path
    else
      flash[:danger] = "Existe(n) #{@period.errors.count} error(es) en los datos suministrados."
      render :new
    end
  end

  def edit; end

  private

  def set_period
    @period = Admin::Academic::Period::Period.find(params[:id])
  end

  def period_params
    params.require(:admin_academic_period_period).permit(
      :created_by_id,
      :is_active,
      :name,
      :start_date,
      :end_date
    )
  end

end
