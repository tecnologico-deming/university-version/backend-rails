class Admin::Academic::Subject::ContentQuestionsController < BaseDashboardController

  before_action :set_subject
  before_action :set_module
  before_action :set_content
  before_action :set_question, only: %i[show edit update destroy]

  def index
    @pagy, @questions = pagy(Admin::Academic::Subject::ContentQuestion.where(content_id: @content.id), items: 5)
  end

  def new
    @question = Admin::Academic::Subject::ContentQuestion.new
  end

  def create
    @question = Admin::Academic::Subject::ContentQuestion.new(question_params)
    @question.created_by_id = current_user.id
    @question.period_id = current_period.id
    @question.content_id = @content.id
    @question.is_active = true
    if @question.save
      flash[:success] = "Guardado con exito"
      redirect_to admin_academic_subject_subject_module_content_questions_path(
        content_id: @content.id,
        module_id: @module.id,
        subject_id: @subject
      )
    else
      render :new
    end
  end

  def edit; end

  def update
    if @question.update(question_params)
      flash[:success] = "Guardado con exito"
      redirect_to admin_academic_subject_subject_module_content_questions_path(
        content_id: @content.id,
        module_id: @module.id,
        subject_id: @subject
      )
    else
      render :edit
    end
  end

  def destroy
    @question.destroy
    flash[:success] = "Procesado con exito"
    redirect_to admin_academic_subject_subject_module_content_questions_path(
      content_id: @content.id,
      module_id: @module.id,
      subject_id: @subject
    )
  end

  private

  def set_subject
    @subject = Admin::Academic::Subject::Subject.find(params[:subject_id])
  end

  def set_module
    @module = Admin::Academic::Subject::Module.find(params[:module_id])
  end

  def set_content
    @content = Admin::Academic::Subject::Content.find(params[:content_id])
  end

  def set_question
    @question = Admin::Academic::Subject::ContentQuestion.find(params[:id])
  end

  def question_params
    params.require(:admin_academic_subject_content_question).permit(
      :created_by_id,
      :period_id,
      :content_id,
      :is_active,
      :title,
      answers_attributes: %i[
        id content_question_id title is_correct _destroy
      ]
    )
  end

end
