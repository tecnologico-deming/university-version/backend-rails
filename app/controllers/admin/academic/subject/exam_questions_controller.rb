class Admin::Academic::Subject::ExamQuestionsController < BaseDashboardController

  before_action :set_subject
  before_action :set_exam
  before_action :set_question, only: %i[show edit update destroy]

  def index
    @pagy, @questions = pagy(Admin::Academic::Subject::ExamQuestion.where(exam_id: @exam.id), items: 5)
  end

  def new
    @question = Admin::Academic::Subject::ExamQuestion.new
  end

  def create
    @question = Admin::Academic::Subject::ExamQuestion.new(question_params)
    @question.created_by_id = current_user.id
    @question.exam_id = @exam.id
    @question.is_active = true
    if @question.save
      flash[:success] = 'Guardado con exito'
      redirect_to admin_academic_subject_subject_exam_questions_path(subject_id: @subject, exam_id: @exam)
    else
      render :new
    end
  end

  def edit; end

  def update
    if @question.update(question_params)
      flash[:success] = 'Guardado con exito'
      redirect_to admin_academic_subject_subject_exam_questions_path(subject_id: @subject, exam_id: @exam)
    else
      render :edit
    end
  end

  def destroy
    @question.destroy
    flash[:succes] = "Procesado con exito"
    redirect_to admin_academic_subject_subject_exam_questions_path(subject_id: @subject, exam_id: @exam)
  end

  private

  def set_subject
    @subject = Admin::Academic::Subject::Subject.find(params[:subject_id])
  end

  def set_exam
    @exam = Admin::Academic::Subject::Exam.find(params[:exam_id])
  end

  def set_question
    @question = Admin::Academic::Subject::ExamQuestion.find(params[:id])
  end

  def question_params
    params.require(:admin_academic_subject_exam_question).permit(
      :created_by_id,
      :exam_id,
      :is_active,
      :title,
      answers_attributes: %i[
        id exam_question_id title is_correct _destroy
      ]
    )
  end


end
