class Admin::Academic::Subject::GuidesController < BaseDashboardController

  before_action :set_subject
  before_action :set_guide, only: %i[show edit update destroy]

  def index
    @pagy, @guides = pagy(Admin::Academic::Subject::Guide.where(subject_id: @subject), items: 5)
  end

  def new
    @guide = Admin::Academic::Subject::Guide.new
  end

  def create
    @guide = Admin::Academic::Subject::Guide.new(guide_params)
    @guide.created_by_id = current_user.id
    @guide.period_id = current_period.id
    @guide.subject_id = @subject.id
    @guide.is_active = true
    if @guide.save
      flash[:success] = "Guardado con exito"
      redirect_to admin_academic_subject_subject_guides_path(
        career_id: @career,
        semester_id: @semester,
        subject_id: @subject
      )
    else
      render :new
    end
  end

  def edit; end

  def update
    if @guide.update(guide_params)
      flash[:success] = "Guardado con exito"
      redirect_to admin_academic_subject_subject_guides_path(
        career_id: @career,
        semester_id: @semester,
        subject_id: @subject
      )
    else
      render :edit
    end
  end

  def destroy
    @guide.destroy
    flash[:success] = "Procesado con exito"
    redirect_to admin_academic_subject_subject_guides_path(
      career_id: @career,
      semester_id: @semester,
      subject_id: @subject
    )
  end

  private

  def set_subject
    @subject = Admin::Academic::Subject::Subject.find(params[:subject_id])
  end

  def set_guide
    @guide = Admin::Academic::Subject::Guide.find(params[:id])
  end

  def guide_params
    params.require(:admin_academic_subject_guide).permit(
      :created_by_id,
      :period_id,
      :subject_id,
      :teacher_id,
      :file
    )
  end

end
