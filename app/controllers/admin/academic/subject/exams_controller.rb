class Admin::Academic::Subject::ExamsController < BaseDashboardController

  before_action :set_subject
  before_action :set_exam, only: %i[show edit update destroy]

  def index
    @pagy, @exams = pagy(Admin::Academic::Subject::Exam.where(subject_id: @subject).order(name: :asc), items: 5)
  end

  def new
    @exam = Admin::Academic::Subject::Exam.new
  end

  def create
    @exam = Admin::Academic::Subject::Exam.new(exam_params)
    @exam.created_by_id = current_user.id
    @exam.period_id = current_period.id
    @exam.subject_id = @subject.id
    @exam.is_active = true
    if @exam.save
      flash[:success] = 'Guardado con exito'
      redirect_to admin_academic_subject_subject_exams_path(subject_id: @subject)
    else
      render :new
    end
  end

  def edit; end

  def update
    if @exam.update(exam_params)
      flash[:success] = 'Guardado con exito'
      redirect_to admin_academic_subject_subject_exams_path(subject_id: @subject)
    else
      render :edit
    end
  end

  def destroy
    @exam.destroy
    flash[:success] = "Procesado con exito"
    redirect_to admin_academic_subject_subject_exams_path(subject_id: @subject)
  end

  private

  def set_subject
    @subject = Admin::Academic::Subject::Subject.find(params[:subject_id])
  end

  def set_exam
    @exam = Admin::Academic::Subject::Exam.find(params[:id])
  end

  def exam_params
    params.require(:admin_academic_subject_exam).permit(
      :created_by_id,
      :period_id,
      :subject_id,
      :is_active,
      :name
    )
  end

end
