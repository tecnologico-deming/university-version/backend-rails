class Admin::Academic::Subject::ModulesController < BaseDashboardController

  before_action :set_subject
  before_action :set_module, only: %i[show edit update destroy]

  def index
    @pagy, @modules = pagy(Admin::Academic::Subject::Module.where(subject_id: @subject).order(order: :asc), items: 5)
  end

  def new
    @module = Admin::Academic::Subject::Module.new
  end

  def create
    @module = Admin::Academic::Subject::Module.new(module_params)
    @module.created_by_id = current_user.id
    @module.period_id = current_period.id
    @module.subject_id = @subject.id
    @module.is_active = true
    @module.set_order(@subject, @module)
    if @module.save
      flash[:success] = 'Guardado con exito'
      redirect_to admin_academic_subject_subject_modules_path(subject_id: @subject)
    else
      render :new
    end
  end

  def edit; end

  def update
    if @module.update(module_params)
      flash[:success] = 'Guardado con exito'
      redirect_to admin_academic_subject_subject_modules_path(subject_id: @subject)
    else
      render :edit
    end
  end

  def destroy
    @module.destroy
    flash[:success] = 'Procesado con exito'
    redirect_to admin_academic_subject_subject_modules_path(subject_id: @subject)
  end

  private

  def set_subject
    @subject = Admin::Academic::Subject::Subject.find(params[:subject_id])
  end

  def set_module
    @module = Admin::Academic::Subject::Module.find(params[:id])
  end

  def module_params
    params.require(:admin_academic_subject_module).permit(
      :created_by_id,
      :subject_id,
      :period_id,
      :is_active,
      :order,
      :name
    )
  end

end
