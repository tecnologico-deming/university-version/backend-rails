class Admin::Academic::Subject::ClassworksController < BaseDashboardController

  before_action :set_career
  before_action :set_semester
  before_action :set_subject
  before_action :set_classwork, only: %i[show edit update destroy]

  def index
    @pagy, @classworks = pagy(Admin::Academic::Subject::Classwork.where(subject_id: @subject), items: 5)
  end

  def new
    @classwork = Admin::Academic::Subject::Classwork.new
  end

  def create
    @classwork = Admin::Academic::Subject::Classwork.new(classwork_params)
    @classwork.created_by_id = current_user.id
    @classwork.period_id = current_period.id
    @classwork.subject_id = @subject.id
    @classwork.is_active = true
    if @classwork.save
      flash[:success] = "Guardado con exito"
      redirect_to admin_academic_subject_subject_classworks_path(
        career_id: @career, semester_id: @semester, subject_id: @subject)
    else
      render :new
    end
  end

  def edit; end

  def update
    if @classwork.update(classwork_params)
      flash[:success] = "Guardado con exito"
      redirect_to admin_academic_subject_subject_classworks_path(
        career_id: @career, semester_id: @semester, subject_id: @subject)
    else
      render :edit
    end
  end

  def destroy
    @classwork.destroy
    flash[:success] = "Procesado con exito"
    redirect_to admin_academic_subject_subject_classworks_path(
      career_id: @career, semester_id: @semester, subject_id: @subject
    )
  end

  private

  def set_career
    @career = Admin::Academic::Career::Career.find(params[:career_id])
  end

  def set_semester
    @semester = Admin::Academic::Career::Semester.find(params[:semester_id])
  end

  def set_subject
    @subject = Admin::Academic::Subject::Subject.find(params[:subject_id])
  end

  def set_classwork
    @classwork = Admin::Academic::Subject::Classwork.find(params[:id])
  end

  def classwork_params
    params.require(:admin_academic_subject_classwork).permit(
      :created_by_id,
      :period_id,
      :subject_id,
      :is_active,
      :file
    )
  end

end
