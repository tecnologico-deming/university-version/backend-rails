class Admin::Academic::Subject::ContentsController < BaseDashboardController

  before_action :set_subject
  before_action :set_module
  before_action :set_content, only: %i[show edit update destroy]

  def index
    @pagy, @contents = pagy(Admin::Academic::Subject::Content.where(module_id: @module).all.order(order: :asc), items: 5)
  end

  def new
    @content = Admin::Academic::Subject::Content.new
  end

  def create
    @content = Admin::Academic::Subject::Content.new(content_params)
    @content.created_by_id = current_user.id
    @content.period_id = current_period.id
    @content.module_id = @module.id
    @content.is_active = true
    @content.set_order(@module, @content)
    if @content.save
      flash[:success] = "Guardado con exito"
      redirect_to admin_academic_subject_subject_module_contents_path(subject_id: @subject.id, module_id: @module.id)
    else
      render :new
    end
  end

  def edit; end

  def update
    if @content.update(content_params)
      flash[:success] = "Guardado con exito"
      redirect_to admin_academic_subject_subject_module_contents_path(subject_id: @subject.id, module_id: @module.id)
    else
      render :edit
    end
  end

  def destroy
    @content.destroy
    flash[:success] = "Procesado con exito"
    redirect_to admin_academic_subject_subject_module_contents_path(subject_id: @subject.id, module_id: @module.id)
  end

  private

  def set_subject
    @subject = Admin::Academic::Subject::Subject.find(params[:subject_id])
  end

  def set_module
    @module = Admin::Academic::Subject::Module.find(params[:module_id])
  end

  def set_content
    @content = Admin::Academic::Subject::Content.find(params[:id])
  end

  def content_params
    params.require(:admin_academic_subject_content).permit(
      :created_by_id,
      :period_id,
      :module_id,
      :type_id,
      :is_active,
      :name,
      :file
    )
  end



end
