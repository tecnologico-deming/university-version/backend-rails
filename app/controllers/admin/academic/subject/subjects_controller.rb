class Admin::Academic::Subject::SubjectsController < BaseDashboardController

  # before_action :set_career
  # before_action :set_semester
  before_action :set_subject, only: %i[show edit update destroy]

  def index
    @pagy, @subjects = pagy(Admin::Academic::Subject::Subject.order(name: :asc), items: 5)
  end

  def new
    @subject = Admin::Academic::Subject::Subject.new
  end

  def create
    @subject = Admin::Academic::Subject::Subject.new(subject_params)
    @subject.created_by_id = current_user.id
    if @subject.save
      @subject.create_modules(params, @subject, current_user)
      flash[:success] = "Guardado con exito"
      redirect_to admin_academic_subject_subjects_path
    else
      flash[:warning] = "Existe al menos un error."
      render :new
    end
  end

  def edit; end

  def update
    if @subject.update(subject_params)
      @subject.create_modules(params, @subject, current_user)
      flash[:success] = "Guardado con exito"
      redirect_to admin_academic_subject_subjects_path
    elsle
      fash[:warning] = "Existe al menos un error."
      render :edit
    end
  end

  def destroy
    @subject.destroy
    flash[:success] = "Procesado con exito."
    redirect_to admin_academic_subject_subjects_path
  end

  private

  def set_subject
    @subject = Admin::Academic::Subject::Subject.find(params[:id])
  end

  def subject_params
    params.require(:admin_academic_subject_subject).permit(
      :created_by_id,
      :semester_id,
      :name,
      :code,
      :credits,
      :hours,
      semester_ids: [],
      modules_attributes: %i[
        id created_by_id subject_id period_id is_active name _destroy
      ]
    )
  end


end
