class Admin::Academic::Group::ClassExtensionsController < BaseDashboardController

  before_action :set_group
  before_action :set_subject
  before_action :set_class

  def new
    @extension = Admin::Academic::Student::Subject::ClassExtension.new
  end

  def create
    @extension = Admin::Academic::Student::Subject::ClassExtension.new(extension_params)
    @extension.created_by_id = current_user.id
    @extension.student_class_id = @class.id
    if @extension.save
      flash[:success] = "Guardado con exito"
      redirect_to admin_academic_group_group_subject_students_path(group_id: @group.id, subject_id: @subject.id)
    else
      render :new
    end
  end

  private

  def set_group
    @group = Admin::Academic::Group::Group.find(params[:group_id])
  end

  def set_subject
    @subject = Admin::Academic::Subject::Subject.find(params[:subject_id])
  end

  def set_class
    @class = Admin::Academic::Student::Subject::Class.find(params[:class_id])
  end

  def extension_params
    params.require(:admin_academic_student_subject_class_extension).permit(
      :created_by_id,
      :student_class_id,
      :date
    )
  end

end
