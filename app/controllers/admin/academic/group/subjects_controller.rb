class Admin::Academic::Group::SubjectsController < BaseDashboardController

  before_action :set_group

  def index
    @subjects = Admin::Academic::Teacher::Subject::Class
                .where(group_id: @group.id, period_id: current_period.id)
                .includes(:subject).order('admin_academic_subject_subjects.name ASC')
  end

  def set_group
    @group = Admin::Academic::Group::Group.find(params[:group_id])
  end

end
