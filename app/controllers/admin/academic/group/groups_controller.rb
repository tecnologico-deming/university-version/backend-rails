class Admin::Academic::Group::GroupsController < BaseDashboardController

  def index
    group_list_by_role
  end

  # Custom actions

  def group_list_by_role
    if current_user.role.name == 'Administrador' || current_user.role.name == 'Asistente' || current_user.role.name == 'Bienestar'
      @pagy, @groups = pagy(Admin::Academic::Group::Group.all.order(name: :asc), items: 5)
      @groups = Admin::Academic::Group::Group.order(name: :asc).search_groups(params[:search]) if params[:search].present?
    elsif current_user.role.name == 'Coordinador'
      user_groups = Admin::Academic::UserGroup.where(user_id: current_user.id).pluck(:group_id)
      @pagy, @groups = pagy(Admin::Academic::Group::Group.where(id: user_groups).order(name: :asc), items: 5)
      @groups = Admin::Academic::Group::Group.where(id: user_groups).order(name: :asc).search_groups(params[:search]) if params[:search].present?
    end
  end


end
