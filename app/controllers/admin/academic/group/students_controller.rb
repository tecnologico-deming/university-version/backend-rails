class Admin::Academic::Group::StudentsController < BaseDashboardController

  before_action :set_group
  before_action :set_subject
  before_action :set_student, only: %i[assign_extension_exam assign_extension_exam_submit
                                       reschedule_exam reschedule_exam_submit]

  def index
    @search = params[:search]

    @users = Admin::Academic::Student::Subject::Class.where(
      group_id: @group.id, subject_id: @subject.id, period_id: current_period.id
    ).includes(:student).order('admin_user_users.first_last_name ASC')
  end

  # Custom actions
  def assign_extension_exam; end

  def assign_extension_exam_submit
    @exam = Admin::Academic::Student::Exam::Extension.new
    @exam.created_by_id = current_user.id
    @exam.period_id = current_period.id
    @exam.subject_id = @subject.id
    @exam.student_id = @student.id
    @exam.start_date = params[:date]
    if @exam.save
      flash[:success] = "Examen supletorio activado con exito"
      redirect_to admin_academic_group_group_subject_students_path
    else
      render :assign_extension_exam
    end
  end

  def reschedule_exam; end

  def reschedule_exam_submit
    @exam = Admin::Academic::Student::Exam::Regular.find_by(
      period_id: current_period.id, student_id: @student.id, subject_id: @subject.id
    )
    if @exam.update(start_date: params[:start_date])
      flash[:success] = "Examen reprogramado con exito"
      redirect_to admin_academic_group_group_subject_students_path
    else
      render :reschedule_exam
    end
  end

  private

  def set_subject
    @subject = Admin::Academic::Subject::Subject.find(params[:subject_id])
  end

  def set_group
    @group = Admin::Academic::Group::Group.find(params[:group_id])
  end

  def set_student
    @student = Admin::User::User.find(params[:id])
  end

end
