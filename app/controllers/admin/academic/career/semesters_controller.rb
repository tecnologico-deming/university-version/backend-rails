class Admin::Academic::Career::SemestersController < BaseDashboardController

  before_action :set_career
  before_action :set_semester, only: %i[show edit update destroy subjects]

  def index
    @pagy, @semesters = pagy(Admin::Academic::Career::Semester.where(career_id: @career).order(order: :asc), items: 5)
  end

  def new
    @semester = Admin::Academic::Career::Semester.new
  end

  def create
    @semester = Admin::Academic::Career::Semester.new(semester_params)
    @semester.created_by_id = current_user.id
    @semester.career_id = @career.id
    @semester.set_order(@career, @semester)
    if @semester.save
      flash[:success] = 'Procesado con exito'
      redirect_to admin_academic_career_career_semesters_path
    else
      flash[:danger] = "Existe al menos un error."
      render :new
    end
  end

  def edit; end

  def update
    if @semester.update(semester_params)
      flash[:success] = 'Procesado con exito'
      redirect_to admin_academic_career_career_semesters_path
    else
      flash[:danger] = "Existe al menos un error."
      render :edit
    end
  end

  def destroy
    @semester.destroy
    flash[:success] = 'Procesado con exito'
    redirect_to admin_academic_career_career_semesters_path
  end

  # Custom routes
  def subjects; end

  private

  def set_career
    @career = Admin::Academic::Career::Career.find(params[:career_id])
  end

  def set_semester
    @semester = Admin::Academic::Career::Semester.find(params[:id])
  end

  def semester_params
    params.require(:admin_academic_career_semester).permit(
      :created_by_id,
      :career_id,
      :order,
      :name,
      :hours,
      :credits
    )
  end


end
