class Admin::Academic::Career::CareersController < BaseDashboardController

  before_action :set_career, only: %i[show edit update destroy]

  def index
    @pagy, @careers = pagy(Admin::Academic::Career::Career.all.order(name: :asc), items: 5)
  end

  def new
    @career = Admin::Academic::Career::Career.new
  end

  def create
    @career = Admin::Academic::Career::Career.new(career_params)
    @career.created_by_id = current_user.id
    @career.is_active = true
    @career.period_id = current_period.id
    if @career.save
      @career.semester_order_and_user(current_user)
      flash[:success] = 'Procesado con exito'
      redirect_to admin_academic_career_careers_path
    else
      flash[:danger] = "Existe(n) #{@career.errors.count} error(es) en los datos suministrados."
      render :new
    end
  end

  def edit; end

  def update
    if @career.update(career_params)
      flash[:success] = 'Procesado con exito'
      redirect_to admin_academic_career_careers_path
    else
      flash[:danger] = "Existe(n) #{@career.errors.count} error(es) en los datos suministrados."
      render :edit
    end
  end

  def destroy
    @career.destroy
    flash[:success] = 'Procesado con exito'
    redirect_to admin_academic_career_careers_path
  end

  private

  def set_career
    @career = Admin::Academic::Career::Career.find(params[:id])
  end

  def career_params
    params.require(:admin_academic_career_career).permit(
      :created_by_id,
      :period_id,
      :is_active,
      :name,
      :code,
      :credits
    )
  end

end


