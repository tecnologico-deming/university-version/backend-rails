class Admin::Academic::VirtualRoom::VirtualRoomsController < BaseDashboardController

  before_action :set_room, only: %i[edit update destroy]
  before_action :set_class, only: %i[show]

  def index
    @search = params[:search]

    if @search.present?
      group = Admin::Academic::Group::Group.order(name: :asc).search_groups(@search)
      @classes = Admin::Academic::Class::Class.where(group_id: group.pluck(:id))
    else
      @pagy, @classes = pagy(Admin::Academic::Class::Class.where(
          period_id: current_period.id
      ).includes(:group).order('admin_academic_group_groups.name ASC'), items: 5)
    end
  end

  def show
    user_names = "#{current_user.first_last_name.titleize.tr('ÁáÉéÍíÓóÚúÑñ', 'AaEeIiOoUuNn').squish!.to_s}+#{current_user.first_name.titleize.tr('ÁáÉéÍíÓóÚúÑñ', 'AaEeIiOoUuNn').squish!.to_s}"
    bbb_response(user_names)
  end

  def new
    @room = Admin::Academic::Class::Class.new
  end

  def create
    @room = Admin::Academic::Class::Class.new(room_params)
    @room.period_id = current_period.id
    if @room.save
      # @room.dates_row(@room, current_user)
      flash[:success] = 'Guardado con exito'
      redirect_to admin_academic_virtual_room_virtual_rooms_path
    else
      render :new
    end
  end

  def edit; end

  def update
    # @room.dates_row(@room, current_user)
    if @room.update(room_params)

      flash[:success] = 'Guardado con exito'
      redirect_to admin_academic_virtual_room_virtual_rooms_path
    else
      render :edit
    end
  end

  def destroy
    @room.destroy
    flash[:success] = 'Procesado con exito'
    redirect_to admin_academic_virtual_room_virtual_rooms_path
  end

  # Custom actions
  def bbb_response(user_names)
    string_to_checksum = "getMeetingInfomeetingID=#{@class.room_code}#{ENV['bbb_secret_key']}"
    get_checksum = Digest::SHA1.hexdigest string_to_checksum
    response = HTTParty.get("#{@class.server.base_url}getMeetingInfo?meetingID=#{@class.room_code}&checksum=#{get_checksum}")
    data = response.parsed_response.to_json
    json = JSON.parse(data)
    if json['response']['returncode'] == 'FAILED'
      create_meeting

      if current_user.role.name == 'Profesor'
        teacher_access(user_names, @class.room_code)
      elsif current_user.role.name == 'Estudiante'
        student_access(user_names, @class.room_code)
      end
    elsif json['response']['returncode'] == 'SUCCESS'
      if current_user.role.name == 'Profesor'
        teacher_access(user_names, @class.room_code)
      elsif current_user.role.name == 'Estudiante'
        student_access(user_names, @class.room_code)
      end

    end
  end

  def create_meeting
    string = "createmeetingID=#{@class.room_code}&moderatorPW=#{ENV['bbb_moderator_password']}&attendeePW=#{ENV['bbb_attendee_password']}#{@class.server.secret_key}"
    check_sum = Digest::SHA1.hexdigest string
    HTTParty.post("#{@class.server.base_url}create?meetingID=#{@class.room_code}&moderatorPW=#{ENV['bbb_moderator_password']}&attendeePW=#{ENV['bbb_attendee_password']}&checksum=#{check_sum}")
  end

  def teacher_access(user_names, meeting_id)
    string = "joinfullName=#{user_names}&meetingID=#{meeting_id}&password=#{ENV['bbb_moderator_password']}&redirect=true#{@class.server.secret_key}"
    check_sum = Digest::SHA1.hexdigest string
    redirect_to "#{@class.server.base_url}join?fullName=#{user_names}&meetingID=#{meeting_id}&password=#{ENV['bbb_moderator_password']}&redirect=true&checksum=#{check_sum}"
  end

  def student_access(user_names, meeting_id)
    string = "joinfullName=#{user_names}&meetingID=#{meeting_id}&password=#{ENV['bbb_attendee_password']}&redirect=true#{@class.server.secret_key}"
    check_sum = Digest::SHA1.hexdigest string
    redirect_to "#{@class.server.base_url}join?fullName=#{user_names}&meetingID=#{meeting_id}&password=#{ENV['bbb_attendee_password']}&redirect=true&checksum=#{check_sum}"
  end

  private

  def set_room
    @room = Admin::Academic::Class::Class.find(params[:id])
  end

  def set_class
    @class = Admin::Academic::Class::ClassDate.find(params[:id])
  end

  def room_params
    params.require(:admin_academic_class_class).permit(
      :activation_id,
      :period_id,
      :group_id,
      :subject_id,
      :teacher_id,
      dates_attributes: %i[
        id
        created_by_id
        class_id
        server_id
        date
        room_code
        _destroy
      ]
    )
  end








end
