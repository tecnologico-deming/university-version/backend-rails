class Admin::User::ReportsController < BaseDashboardController

  before_action :set_user

  def student_progress
    @periods = Admin::Academic::Period::Period.where(id: [11, 10, 5]).order(end_date: :desc)
    @period = current_period
    @selected_period = params[:period_id]

    @period = Admin::Academic::Period::Period.find @selected_period if @selected_period

    @subjects = Admin::Academic::Student::Subject::Class
                .where(period_id: @period.id, student_id: @user.id)
                .includes(:subject).order('admin_academic_subject_subjects.name ASC')
  end

  def student_subject_progress
    @search = params[:search]
    student_progress_queries if @search
  end

  def student_progress_queries
    @subject = Admin::Academic::Subject::Subject.find(@search)
    @class_work = Admin::Academic::Student::Subject::ClassworkUpload
                  .find_by(period_id: params[:period_id], student_id: @user.id, subject_id: @search)
    @exam = Admin::Academic::Student::Exam::Regular
            .find_by(period_id: params[:period_id], student_id: @user.id, subject_id: @search)

    @extension_exam = Admin::Academic::Student::Exam::Extension
                      .find_by(period_id: params[:period_id], student_id: @user.id, subject_id: @search)
  end

  private

  def set_user
    @user = Admin::User::User.find(params[:user_id])
  end



end
