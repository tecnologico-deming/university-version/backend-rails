class Admin::User::PrintsController < BaseDashboardController

  before_action :set_exam, only: %i[student_exam]
  before_action :set_extension_exam, only: %i[student_extension_exam]

  def student_exam
    @question_ids = @exam.details.pluck(:question_id).uniq

    @questions = Admin::Academic::Subject::ExamQuestion.where(id: @question_ids)

    render pdf: 'Examen final',
           layout: 'pdf.html',
           title: "#{@exam.student.full_name.upcase} - #{@exam.subject.name.upcase}",
           viewport_size: '1280x1024',
           template: "admin/user/prints/student_exam.html.erb",
           encoding: "utf8",
           page_size: 'A4',
           # page_width: 1000,
           orientation: 'Portrait',
           dpi: '600',
           margin: {
             bottom: 15
           }
  end

  def student_extension_exam
    @question_ids = @extension_exam.details.pluck(:question_id).uniq

    @questions = Admin::Academic::Subject::ExamQuestion.where(id: @question_ids)

    render pdf: 'Examen final',
           viewport_size: '1280x1024',
           template: "admin/user/prints/student_extension_exam.html.erb",
           encoding: "utf8",
           page_size: 'A4',
           orientation: 'Portrait',
           dpi: '600',
           layout: 'pdf.html.erb'
  end

  private

  def set_exam
    @exam = Admin::Academic::Student::Exam::Regular.find(params[:exam_id])
  end

  def set_extension_exam
    @extension_exam = Admin::Academic::Student::Exam::Extension.find(params[:exam_id])
  end

end
