class Admin::User::UsersController < BaseDashboardController

  before_action :set_user, only: %i[show edit update destroy
                                    assign_group assign_group_submit
                                    assign_shelf assign_shelf_submit
                                    send_score_to_binary send_score_to_binary_submit
                                    reset_password_submit update_info_submit]

  def index
    user_list_by_role
  end

  def new
    @user = Admin::User::User.new
  end

  def create
    @user = Admin::User::User.new(user_params)
    @user.created_by_admin = true
    @user.password = @user.identification_number
    @user.status_id = Admin::User::Status.find_by(name: 'Activo').id
    @user.custom_token = SecureRandom.hex(4)
    if @user.save
      flash[:success] = 'Guardado con exito'
      redirect_to admin_user_users_path
    else
      render :new
    end
  end

  def edit; end

  def update
    if @user.update(user_params)
      flash[:success] = 'Guardado con exito'
      redirect_to admin_user_users_path
    else
      render :edit
    end
  end

  def destroy
    @user.destroy
    flash[:success] = 'Procesado con exito'
    redirect_to admin_user_users_path
  end

  # Custom actions
  def user_list_by_role
    if current_user.role.name == 'Administrador'
      @pagy, @users = pagy(Admin::User::User.all.order(first_last_name: :asc), items: 3)
      @users = Admin::User::User.order(first_last_name: :asc, second_last_name: :asc).search_users(params[:search]) if params[:search].present?
    elsif current_user.role.name == 'Asistente'
      @pagy, @users = pagy(Admin::User::User.where(role_id: [1, 2]).order(first_last_name: :asc), items: 3)
      @users = Admin::User::User.where(role_id: [1, 2]).order(first_last_name: :asc, second_last_name: :asc).search_users(params[:search]) if params[:search]
    elsif current_user.role.name == 'Coordinador' || current_user.role.name == 'Bienestar' || current_user.role.name == 'Invitado'
      @users = Admin::User::User.where(role_id: 1).order(first_last_name: :asc)
      @users = Admin::User::User.where(role_id: 1).order(first_last_name: :asc, second_last_name: :asc).search_users(params[:search]) if params[:search]
    end
  end

  def assign_group; end

  def assign_group_submit
    if @user.update(user_params)
      flash[:success] = 'Guardado con exito'
      redirect_to admin_user_users_path
    else
      render :assign_group
    end
  end

  def assign_shelf; end

  def assign_shelf_submit
    if @user.update(user_params)
      flash[:success] = 'Guardado con exito'
      redirect_to admin_user_users_path
    else
      render :assign_shelf
    end
  end

  def update_info_submit
    @user.update(need_update: true)
    flash[:success] = 'Procesado con exito'
    redirect_to admin_user_users_path
  end

  def reset_password_submit
    @user.update(password: @user.identification_number, password_changed: false)
    flash[:success] = 'Contraseña reestablecida con exito'
    redirect_to admin_user_users_path
  end

  def send_score_to_binary
    @subjects = Admin::Academic::Student::Subject::Class.where(
      period_id: current_period.id, student_id: @user.id
    ).includes(:subject).order('admin_academic_subject_subjects.name ASC')
  end

  def send_score_to_binary_submit
    data_to_binary(params)
    redirect_to admin_user_users_path
  end

  private

  def data_to_binary(params)
    if params[:score] == 'Examen final'
      score = Admin::Academic::Student::Exam::Regular.find_by(period_id: current_period.id, student_id: @user.id, subject_id: params[:subject_id])
      score.send_score_to_binary
      flash[:success] = score.send_score_to_binary
    elsif params[:score] == 'Retos'
      score = Admin::Academic::Student::Score::Score.find_by(period_id: current_period.id, student_id: @user.id, subject_id: params[:subject_id])
      data_to_send = {data:{id_materia: params[:subject_id], cedula: @user.identification_number, calificacion: score.online_activities_score}}
      response = Communicator::Binary::HTTP.send_online_activities_score(data_to_send)
    elsif params[:score] == 'Taller'
      score = Admin::Academic::Student::Subject::ClassworkUpload.find_by(period_id: current_period.id, student_id: @user.id, subject_id: params[:subject_id])
      data_to_send = {data:{id_materia: params[:subject_id], cedula: @user.identification_number, calificacion: score.score}}
      response = Communicator::Binary::HTTP.send_classwork_score(data_to_send)
    end

    response
  end

  def set_user
    @user = Admin::User::User.find(params[:id])
  end

  def user_params
    params.require(:admin_user_user).permit(
      :status_id,
      :role_id,
      :gender_id,
      :identification_type_id,
      :row_id,
      :created_by_admin,
      :identification_number,
      :password,
      :birth_date,
      :custom_token,
      :mobile_number,
      :house_number,
      :email,
      :first_last_name,
      :second_last_name,
      :first_name,
      :second_name,
      group_ids: []
    )
  end

end

