class Admin::Support::TicketsController < BaseDashboardController

  before_action :set_ticket, only: %i[show edit update destroy
                                      assign assign_submit details
                                      complete_submit send_message_submit]

  def index
    @message = Admin::Support::TicketDetail.new
    @periods = Admin::Academic::Period::Period.order(end_date: :desc)
    @period = current_period
    @selected_period = params[:period_id]

    @period = Admin::Academic::Period::Period.find @selected_period if @selected_period

    ticket_detail_search
    tickets_by_user
    ticket_queries
  end

  def show; end

  def new
    @ticket = Admin::Support::Ticket.new
  end

  def create
    @ticket = Admin::Support::Ticket.new(ticket_params)
    @ticket.period_id = current_period.id
    @ticket.created_by_id = current_user.id
    @ticket.status = 'Por procesar'
    @ticket.code = SecureRandom.hex(2)
    if @ticket.save
      flash[:success] = "Guardado con exito"
      redirect_to admin_support_tickets_path
    else
      render :new
    end
  end

  def edit; end

  def update
    if @ticket.update(ticket_params)
      flash[:success] = "Guardado con exito"
      redirect_to admin_support_tickets_path
    else
      render :edit
    end
  end

  def destroy
    @ticket.destroy
    flash[:success] = "Procesado con exito"
    redirect_to admin_support_tickets_path
  end

  # Custom actions
  def assign; end

  def assign_submit
    if @ticket.update(ticket_params)
      flash[:success] = "Asignado con exito"
      redirect_to admin_support_tickets_path
    else
      render :assign
    end
  end

  def complete_submit
    @ticket.update(status: 'Solucionado')
    flash[:success] = "Completado con exito"
    redirect_to admin_support_tickets_path
  end

  def details; end

  def tickets_by_user
    @tickets = if current_user.role.name == 'Coordinador'
                 Admin::Support::Ticket.where(created_by_id: current_user.id, status: 'Por procesar').order(created_at: :desc)
               else
                 Admin::Support::Ticket.where(period_id: @period.id, status: 'Por procesar').order(created_at: :desc)
               end
  end

  def ticket_period_search
    @period = Admin::Academic::Period::Period.find params[:period_id]
  end

  private

  def ticket_detail_search
    @search = params[:ticket_id]
    @ticket = Admin::Support::Ticket.find(@search) if @search.present?
  end

  def ticket_queries
    @assigned_tickets = Admin::Support::Ticket.where(
      period_id: @period.id, assigned_to_id: current_user.id, status: 'En proceso'
    ).order(created_at: :desc)

    @closed_tickets = Admin::Support::Ticket.where(
      period_id: @period.id, assigned_to_id: current_user.id, status: 'Solucionado'
    ).order(created_at: :desc)

    @coordinator_in_process_tickets = Admin::Support::Ticket.where(
      period_id: @period.id, created_by_id: current_user.id, status: 'En proceso'
    ).order(created_at: :desc)

    @coordinator_in_closed_tickets = Admin::Support::Ticket.where(
      period_id: @period.id, created_by_id: current_user.id, status: 'Solucionado'
    ).order(created_at: :desc)
  end

  def set_ticket
    @ticket = Admin::Support::Ticket.find(params[:id])
  end

  def ticket_params
    params.require(:admin_support_ticket).permit(
      :period_id,
      :created_by_id,
      :type_id,
      :assigned_to_id,
      :status,
      :title,
      :file,
      :comments
    )
  end

end
