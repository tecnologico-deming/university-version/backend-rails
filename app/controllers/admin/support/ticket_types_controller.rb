class Admin::Support::TicketTypesController < BaseDashboardController

  before_action :set_type, only: %i[show edit update destroy]

  def index
    @pagy, @types = pagy(Admin::Support::TicketType.all.order(title: :asc), items: 5)
  end

  def new
    @type = Admin::Support::TicketType.new
  end

  def create
    @type = Admin::Support::TicketType.new(type_params)
    @type.created_by_id = current_user.id
    @type.is_active = true
    if @type.save
      flash[:success] = "Guardado con exito"
      redirect_to admin_support_ticket_types_path
    else
      render :new
    end
  end

  def edit; end

  def update
    if @type.update(type_params)
      flash[:success] = "Guardado con exito"
      redirect_to admin_support_ticket_types_path
    else
      render :edit
    end
  end

  def destroy
    @type.destroy
    flash[:success] = "Procesado con exito"
    redirect_to admin_support_ticket_types_path
  end

  private

  def set_type
    @type = Admin::Support::TicketType.find(params[:id])
  end

  def type_params
    params.require(:admin_support_ticket_type).permit(
      :created_by_id,
      :is_active,
      :title
    )
  end

end
