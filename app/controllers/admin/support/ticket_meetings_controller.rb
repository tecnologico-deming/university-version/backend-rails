class Admin::Support::TicketMeetingsController < BaseDashboardController

  before_action :set_ticket

  def create
    @meeting = Admin::Support::TicketMeeting.new(meeting_params)
    @meeting.created_by_id = current_user.id
    @meeting.ticket_id = @ticket.id
    if @meeting.save
      redirect_to admin_support_ticket_path(id: @ticket)
    end
  end

  private

  def set_ticket
    @ticket = Admin::Support::Ticket.find(params[:ticket_id])
  end

  def meeting_params
    params.require(:admin_support_ticket_meeting).permit(
      :created_by_id,
      :ticket_id,
      :date
    )

  end

end
