class Admin::Support::TicketDetailsController < BaseDashboardController

  before_action :set_ticket

  def create
    @message = Admin::Support::TicketDetail.new(detail_params)
    @message.ticket_id = @ticket.id
    @message.created_by_id = current_user.id
    if @message.save
      ActionCable.server.broadcast 'ticket_room_channel',
                                   message: render_message(@message),
                                   ticket_id: @message.ticket_id,
                                   sender_id: @message.created_by_id,
                                   current_user_id: current_user.id
    end
  end

  private

  def render_message(detail)
    render(partial: 'admin/support/tickets/partials/detail/messages/message_detail', locals: { detail: detail })
  end

  def set_ticket
    @ticket = Admin::Support::Ticket.find(params[:ticket_id])
  end

  def detail_params
    params.require(:admin_support_ticket_detail).permit(
      :ticket_id,
      :created_by_id,
      :comment
    )
  end

end
