class Admin::Server::ServersController < BaseDashboardController

  before_action :set_server, only:  %i[show edit update destroy]

  def index
    @servers = Admin::Server::Server.all.order(name: :asc)
  end

  def new
    @server = Admin::Server::Server.new
  end

  def create
    @server = Admin::Server::Server.new(server_params)
    @server.created_by_id = current_user.id
    @server.is_active = true
    if @server.save
      flash[:success] = "Guardado con exito"
      redirect_to admin_server_servers_path
    else
      render :new
    end
  end

  def edit; end

  def update
    if @server.update(server_params)
      flash[:success] = "Guardado con exito"
      redirect_to admin_server_servers_path
    else
      render :edit
    end
  end

  private

  def set_server
    @server = Admin::Server::Server.find(params[:id])
  end

  def server_params
    params.require(:admin_server_server).permit(
      :created_by_id,
      :is_active,
      :name,
      :base_url,
      :secret_key
    )
  end

end
