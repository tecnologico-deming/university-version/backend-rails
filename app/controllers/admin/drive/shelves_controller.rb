class Admin::Drive::ShelvesController < BaseDashboardController

  before_action :set_shelf, only: %i[show edit update destroy]

  def index
    @pagy, @shelves = pagy(Admin::Drive::Shelf.all.order(name: :asc), items: 8)
  end

  def new
    @shelf = Admin::Drive::Shelf.new
  end

  def create
    @shelf = Admin::Drive::Shelf.new(shelf_params)
    @shelf.created_by_id = current_user.id
    @shelf.period_id = current_period.id
    @shelf.is_active = true
    if @shelf.save
      flash[:success] = "Guardado con exito"
      redirect_to admin_drive_shelves_path
    else
      render :new
    end
  end

  def edit; end

  def update
    if @shelf.update(shelf_params)
      flash[:success] = "Guardado con exito"
      redirect_to admin_drive_shelves_path
    else
      render :edit
    end
  end

  def destroy
    @shelf.destroy
    flash[:success] = "Procesado con exito"
    redirect_to admin_drive_shelves_path
  end

  # Custom actions

  def print_qr
    @shelf = Admin::Drive::Shelf.find params[:shelf_id]
    qrcode = RQRCode::QRCode.new("/api/v2/drive/shelves/#{@shelf.id}/scan_qr", size: 10)

    @svg = qrcode.as_svg(
      offset: 0,
      color: '000',
      shape_rendering: 'crispEdges',
      module_size: 6,
      standalone: true
    )

    render pdf: 'Codigo QR',
           viewport_size: '1280x1024',
           template: "/admin/drive/shelves/print_qr.html.erb",
           encoding: "utf8",
           page_size: 'A4',
           orientation: 'Portrait',
           dpi: '600'
  end


  private

  def set_shelf
    @shelf = Admin::Drive::Shelf.find(params[:id])
  end

  def shelf_params
    params.require(:admin_drive_shelf).permit(
      :created_by_id,
      :period_id,
      :department_id,
      :is_active,
      :name,
      rows_attributes: %i[id shelf_id name _destroy]
    )
  end



end
