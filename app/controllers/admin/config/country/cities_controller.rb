class Admin::Config::Country::CitiesController < BaseDashboardController

  before_action :set_country
  before_action :set_state
  before_action :set_city, only: %i[show edit update destroy]

  def index
    @search = params[:search]
    @pagy, @cities = pagy(Admin::Config::City.where(state_id: @state).order(name: :asc), items: 5)
    @cities = Admin::Config::City.order(name: :asc).search_cities(@search) if @search
  end

  def new
    @city = Admin::Config::City.new
  end

  def create
    @city = Admin::Config::City.new(city_params)
    @city.created_by_id = current_user.id
    @city.state_id = @state.id
    if @city.save
      flash[:success] = "Guardado con exito"
      redirect_to admin_config_country_country_state_cities_path
    else
      render :new
    end
  end

  def edit; end

  def update
    if @city.update(city_params)
      flash[:success] = "Guardado con exito"
      redirect_to admin_config_country_country_state_cities_path
    else
      render :edit
    end
  end

  def destroy
    @city.destroy
    flash[:success] = "Procesado con exito"
    redirect_to admin_config_country_country_state_cities_path
  end

  private

  def set_country
    @country = Admin::Config::Country.find(params[:country_id])
  end

  def set_state
    @state = Admin::Config::State.find(params[:state_id])
  end

  def set_city
    @city = Admin::Config::City.find(params[:id])
  end

  def city_params
    params.require(:admin_config_city).permit(
      :created_by_id,
      :state_id,
      :name
    )
  end

end
