class Admin::Config::Country::CountriesController < BaseDashboardController

  before_action :set_country, only: %i[show edit update destroy]

  def index
    @search = params[:search]

    if @search.present?
      @countries = Admin::Config::Country.order(name: :asc).search_countries(@search)
    else
      @pagy, @countries = pagy(Admin::Config::Country.all.order(name: :asc), items: 5)
    end
  end

  def new
    @country = Admin::Config::Country.new
  end

  def create
    @country = Admin::Config::Country.new(country_params)
    @country.created_by_id = current_user.id
    @country.is_active = true
    if @country.save
      flash[:success] = "Guardado con exito"
      redirect_to admin_config_country_countries_path
    else
      render :new
    end
  end

  def edit; end

  def update
    if @country.update(country_params)
      flash[:success] = "Guardado con exito"
      redirect_to admin_config_country_countries_path
    else
      render :edit
    end
  end

  def destroy
    @country.destroy
    flash[:success] = "Procesado con exito"
    redirect_to admin_config_country_countries_path
  end

  private

  def set_country
    @country = Admin::Config::Country.find(params[:id])
  end

  def country_params
    params.require(:admin_config_country).permit(
      :created_by_id,
      :is_active,
      :name
    )
  end

end
