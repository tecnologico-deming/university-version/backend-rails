class Admin::Config::Country::StatesController < BaseDashboardController

  before_action :set_country
  before_action :set_state, only: %i[show edit update destroy]

  def index
    @search = params[:search]
    @pagy, @states = pagy(Admin::Config::State.where(country_id: @country.id).order(name: :asc), items: 5)
    @states = Admin::Config::State.order(name: :asc).search_states(@search) if @search.present?
  end

  def new
    @state = Admin::Config::State.new
  end

  def create
    @state = Admin::Config::State.new(state_params)
    @state.created_by_id = current_user.id
    @state.country_id = @country.id
    if @state.save
      flash[:success] = "Creado con exito"
      redirect_to admin_config_country_country_states_path
    else
      render :new
    end
  end

  def edit; end

  def update
    if @state.update(state_params)
      flash[:success] = "Creado con exito"
      redirect_to admin_config_country_country_states_path
    else
      render :edit
    end
  end

  def destroy
    @state.destroy
    flash[:success] = "Procesado con exito"
    redirect_to admin_config_country_country_states_path
  end

  private

  def set_country
    @country = Admin::Config::Country.find(params[:country_id])
  end

  def set_state
    @state = Admin::Config::State.find(params[:id])
  end

  def state_params
    params.require(:admin_config_state).permit(
      :created_by_id,
      :country_id,
      :name
    )
  end

end
