class Admin::Library::Authors::AuthorsController < BaseDashboardController

  before_action :set_author, only: %i[show edit update destroy]

  def index
    @pagy, @authors = pagy(Admin::Library::Author::Author.all.order(full_name: :asc), items: 5)
    if params[:search].present?
      @authors = Admin::Library::Author::Author.order(full_name: :asc).search_authors(params[:search])
    end
  end

  def new
    @author = Admin::Library::Author::Author.new
  end

  def create
    @author = Admin::Library::Author::Author.new(author_params)
    @author.created_by_id = current_user.id
    @author.is_active = true
    if @author.save
      flash[:success] = "Guardado con exito"
      redirect_to admin_library_authors_authors_path
    else
      render :new
    end
  end

  def edit; end

  def update
    if @author.update(author_params)
      flash[:success] = "Procesado con exito"
      redirect_to admin_library_authors_authors_path
    else
      render :edit
    end
  end

  def destroy
    @author.destroy
    flash[:success] = "Procesado con exito"
    redirect_to admin_library_authors_authors_path
  end

  # Custom routes
  def author_excel_upload
    @author = Admin::Library::Author::Author.new
  end

  def author_excel_upload_submit
    xlsx = Roo::Spreadsheet.open("#{params[:admin_library_author_author][:file].path}")
    header = xlsx.row(1)
    xlsx.each_with_index do |row, idx|
      next if idx == 0
      data = Hash[[header, row].transpose]
      @author = Admin::Library::Author::Author.find_or_create_by(full_name: data['AUTOR'])
      @author.update(created_by_id: current_user.id, is_active: true, code: data['CODIGO CUTTER'])
    end

    flash[:success] = "Procesado con exito"
    redirect_to admin_library_authors_authors_path
  end

  private

  def set_author
    @author = Admin::Library::Author::Author.find(params[:id])
  end

  def author_params
    params.require(:admin_library_author_author).permit(
      :created_by_id,
      :is_active,
      :code,
      :full_name
    )
  end

end
