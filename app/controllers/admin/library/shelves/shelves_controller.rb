class Admin::Library::Shelves::ShelvesController < BaseDashboardController

  before_action :set_shelf, only: %i[show edit update destroy]

  def index
    @pagy, @shelves = pagy(Admin::Drive::Shelf.where(created_by_id: current_user.id, department_id: 2).order(name: :asc), items: 8)
  end

  def new
    @shelf = Admin::Drive::Shelf.new
  end

  def create
    @shelf = Admin::Drive::Shelf.new(shelf_params)
    @shelf.created_by_id = current_user.id
    @shelf.is_active = true
    @shelf.department_id = 2
    if @shelf.save
      flash[:success] = "Guardado con exito"
      redirect_to admin_library_shelves_shelves_path
    else
      render :new
    end
  end

  def edit; end

  def update
    if @shelf.update(shelf_params)
      flash[:success] = "Guardado con exito"
      redirect_to admin_library_shelves_shelves_path
    else
      render :edit
    end
  end

  def destroy
    @shelf.destroy
    flash[:success] = "Procesado con exito"
    redirect_to admin_library_shelves_shelves_path
  end


  private

  def set_shelf
    @shelf = Admin::Drive::Shelf.find(params[:id])
  end

  def shelf_params
    params.require(:admin_drive_shelf).permit(
      :created_by_id,
      :period_id,
      :department_id,
      :is_active,
      :name,
      rows_attributes: %i[id shelf_id name _destroy]
    )
  end

end
