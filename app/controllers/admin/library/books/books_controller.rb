class Admin::Library::Books::BooksController < BaseDashboardController

  before_action :set_book, only: %i[show edit update destroy]

  def index
    @pagy, @books = pagy(Admin::Library::Book::Book.all.order(title: :asc), items: 5)
    if params[:search].present?
      @books = Admin::Library::Book::Book.order(title: :asc).where(is_active: true).search_books(params[:search])
    end
  end

  def new
    set_rows
    @book = Admin::Library::Book::Book.new
  end

  def create
    @book = Admin::Library::Book::Book.new(book_params)
    @book.created_by_id = current_user.id
    @book.period_id = current_period.id
    @book.is_active = true
    if @book.save
      @book.create_copies(@book, current_user)
      flash[:success] = "Guardado con exito"
      redirect_to admin_library_books_books_path
    else
      render :new
    end
  end

  def edit
    set_rows
  end

  def update
    if @book.update(book_params)
      flash[:success] = "Guardado con exito"
      redirect_to admin_library_books_books_path
    else
      render :edit
    end
  end

  def destroy
    @book.destroy
    flash[:success] = "Procesado con exito"
    redirect_to admin_library_books_books_path
  end

  # Custom actions

  def excel_upload; end

  def book_excel_upload_submit
    xlsx = Roo::Spreadsheet.open("#{params[:file].path}")
    header = xlsx.row(1)
    xlsx.each_with_index do |row, idx|
      next if idx == 0 # Skip header
      data = Hash[[header, row].transpose]
      @book = Admin::Library::Book::Book.new
      @book.created_by_id = current_user.id
      @book.period_id = current_period.id
      @book.language_id = Admin::Config::Language.find_by(name: data['IDIOMA'].titleize).try(:id) if data['IDIOMA'].present?
      @book.main_author_id = Admin::Library::Author::Author.find_by(full_name: data['AUTOR  ']).try(:id) if data['AUTOR  '].present?
      @book.secondary_author_id = Admin::Library::Author::Author.find_by(full_name: data['AUTOR SECUNDARIO ']).try(:id) if data['AUTOR SECUNDARIO '].present?
      @book.category_id = Admin::Library::Category::Category.find_by(code: data['CLASIFICACION ']).try(:id) if data['CLASIFICACION '].present?
      @book.is_active = true
      @book.title = data['TITULO'] if data['TITULO'].present?
      @book.pages = data['PAGINAS'] if data['PAGINAS'].present?
      @book.height = data['TAMAÑO'] if data['TAMAÑO'].present?
      @book.isbn_code = data['ISBN'] if data['ISBN'].present?
      @book.copies = data['N. EJEMPLAR'] if data['N. EJEMPLAR'].present?
      @book.create_copies(@book, current_user, data) if @book.save
    end

    flash[:success] = "Procesado con exito"
    redirect_to admin_library_books_books_path
  end

  private

  def set_rows
    @department = Admin::Drive::Department.find_by(name: 'Biblioteca')
    @department.shelves.each do |shelf|
      @rows = shelf.rows
    end
  end

  def set_book
    @book = Admin::Library::Book::Book.find(params[:id])
  end

  def book_params
    params.require(:admin_library_book_book).permit(
      :created_by_id,
      :period_id,
      :campus_id,
      :category_id,
      :editorial_id,
      :language_id,
      :main_author_id,
      :origin_id,
      :publish_type_id,
      :row_id,
      :secondary_author_id,
      :type_id,
      :is_active,
      :code_dewey,
      :copies,
      :isbn_code,
      :title,
      :subtitle,
      :pages,
      :height,
      :publish_date,
      :publish_place,
      :comment,
      :description,
      :resume,
      :physic_details,
      :file,
      :image,
      tag_ids: [],
      subject_ids: []
    )
  end

end
