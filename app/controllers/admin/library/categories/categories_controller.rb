class Admin::Library::Categories::CategoriesController < BaseDashboardController

  before_action :set_category, only: %i[show edit update destroy]

  def index
    @pagy, @categories = pagy(Admin::Library::Category::Category.all.order(name: :asc), items: 5)
    if params[:search].present?
      @categories = Admin::Library::Category::Category.order(name: :asc).search_categories(params[:search])
    end
  end

  def new
    @category = Admin::Library::Category::Category.new
  end

  def create
    @category = Admin::Library::Category::Category.new(category_params)
    @category.created_by_id = current_user.id
    @category.is_active = true
    if @category.save
      flash[:success] = "Guardado con exito"
      redirect_to admin_library_categories_categories_path
    else
      render :new
    end
  end

  def edit; end

  def update
    if @category.update(category_params)
      flash[:success] = "Guardado con exito"
      redirect_to admin_library_categories_categories_path
    else
      render :edit
    end
  end

  def destroy
    @category.destroy
    flash[:success] = "Procesado con exito"
    redirect_to admin_library_categories_categories_path
  end

  # Custom routes
  def category_excel_upload
    @category = Admin::Library::Category::Category.new
  end

  def category_excel_upload_submit
    xlsx = Roo::Spreadsheet.open("#{params[:admin_library_category_category][:file].path}")
    header = xlsx.row(1)
    xlsx.each_with_index do |row, idx|
      next if idx == 0
      data = Hash[[header, row].transpose]
      @category = Admin::Library::Category::Category.find_or_create_by(name: data['NOMBRE'])
      @category.update(created_by_id: current_user.id, is_active: true, code: data['CODIGO DEWEY'])
    end

    flash[:success] = "Procesado con exito"
    redirect_to admin_library_categories_categories_path
  end

  private

  def set_category
    @category = Admin::Library::Category::Category.find(params[:id])
  end

  def category_params
    params.require(:admin_library_category_category).permit(
      :created_by_id,
      :is_active,
      :code,
      :name
    )
  end

end
