class Admin::Library::Categories::TagsController < BaseDashboardController

  before_action :set_tag, only: %i[show edit update destroy]

  def index
    @pagy, @tags = pagy(Admin::Library::Category::Tag.order(name: :asc), items: 5)
    if params[:search].present?
      @tags = Admin::Library::Category::Tag.order(name: :asc).search_tags(params[:search])
    end
  end

  def new
    @tag = Admin::Library::Category::Tag.new
  end

  def create
    @tag = Admin::Library::Category::Tag.new(tag_params)
    @tag.created_by_id = current_user.id
    @tag.is_active = true
    if @tag.save
      flash[:success] = "Guardado con exito"
      redirect_to admin_library_categories_tags_path
    else
      render :new
    end
  end

  def edit; end

  def update
    if @tag.update(tag_params)
      flash[:success] = "Guardado con exito"
      redirect_to admin_library_categories_tags_path
    else
      render :edit
    end
  end

  def destroy
    @tag.destroy
    flash[:success] = "Procesado con exito"
    redirect_to admin_library_categories_tags_path
  end

  private

  def set_tag
    @tag = Admin::Library::Category::Tag.find(params[:id])
  end

  def tag_params
    params.require(:admin_library_category_tag).permit(
      :created_by_id,
      :is_active,
      :name
    )
  end

end
