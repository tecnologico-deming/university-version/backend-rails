class Admin::Library::Editorials::EditorialsController < BaseDashboardController

  before_action :set_editorial, only: %i[edit update destroy]

  def index
    @pagy, @editorials = pagy(Admin::Library::Editorial::Editorial.all.order(name: :asc), items: 5)
    if params[:search].present?
      @editorials = Admin::Library::Editorial::Editorial.order(name: :asc).search_editorials(params[:search])
    end
  end

  def new
    @editorial = Admin::Library::Editorial::Editorial.new
  end

  def create
    @editorial = Admin::Library::Editorial::Editorial.new(editorial_params)
    @editorial.created_by_id = current_user.id
    @editorial.is_active = true
    if @editorial.save
      flash[:success] = "Guardado con exito"
      redirect_to admin_library_editorials_editorials_path
    else
      render :new
    end
  end

  def edit; end

  def update
    if @editorial.update(editorial_params)
      flash[:success] = "Guardado con exito"
      redirect_to admin_library_editorials_editorials_path
    else
      render :edit
    end
  end

  def destroy
    @editorial.destroy
    flash[:success] = "Procesado con exito"
    redirect_to admin_library_editorials_editorials_path
  end

  # Custom routes
  def editorial_excel_upload
    @editorial = Admin::Library::Editorial::Editorial.new
  end

  def editorial_excel_upload_submit
    xlsx = Roo::Spreadsheet.open("#{params[:admin_library_editorial_editorial][:file].path}")
    header = xlsx.row(1)
    xlsx.each_with_index do |row, idx|
      next if idx == 0
      data = Hash[[header, row].transpose]
      city = Admin::Config::City.find_by(name: data['CIUDAD']).try(:id)
      @editorial = Admin::Library::Editorial::Editorial.find_or_create_by(name: data['EDITORIAL'])
      @editorial.update(created_by_id: current_user.id, city_id: city, is_active: true)
    end

    flash[:success] = "Procesado con exito"
    redirect_to admin_library_editorials_editorials_path
  end

  private

  def set_editorial
    @editorial = Admin::Library::Editorial::Editorial.find(params[:id])
  end

  def editorial_params
    params.require(:admin_library_editorial_editorial).permit(
      :created_by_id,
      :city_id,
      :is_active,
      :name
    )
  end

end


