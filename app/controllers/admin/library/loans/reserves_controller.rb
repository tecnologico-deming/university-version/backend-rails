class Admin::Library::Loans::ReservesController < BaseDashboardController

  before_action :set_reserve, only: %i[edit update destroy]

  def index
    @pagy, @reserves = pagy(Admin::Library::Loan::Reserve.order(created_at: :desc), items: 5)
  end

  def edit; end

  def update
    @reserve.update(reserve_params)
    @reserve.is_approved_by_id = current_user.id
    if @reserve.save
      @reserve.updated_reserve(@reserve, ecuador_time)
      flash[:success] = "Reserva procesada con exito"
      redirect_to admin_library_loans_reserves_path
    else
      render :edit
    end
  end

  def destroy
    @reserve.deleted_reserve(@reserve)
    @reserve.destroy
    flash[:success] = "Procesado con exito"
    redirect_to admin_library_loans_reserves_path
  end

  private

  def set_reserve
    @reserve = Admin::Library::Loan::Reserve.find(params[:id])
  end

  def reserve_params
    params.require(:admin_library_loan_reserve).permit(
      :user_id,
      :copy_id,
      :is_approved_by_id,
      :is_approved,
      :comments
    )
  end

end
