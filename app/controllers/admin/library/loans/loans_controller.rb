class Admin::Library::Loans::LoansController < BaseDashboardController

  before_action :set_loan, only: %i[edit update]

  def index
    @pagy, @loans = pagy(Admin::Library::Loan::Loan.order(created_at: :desc), items: 5)
  end

  def edit; end

  def update
    @loan.was_received_by_id = current_user.id
    @loan.was_received = true
    @loan.was_received_date = ecuador_time
    if @loan.save
      flash[:success] = "Gestionado con exito"
      redirect_to admin_library_loans_loans_path
    else
      render :edit
    end
  end

  private

  def set_loan
    @loan = Admin::Library::Loan::Loan.find(params[:id])
  end

  def loan_params
    params.require(:admin_library_loan_loan).permit(
      :reserve_id,
      :was_received_by_id,
      :was_received,
      :was_received_date,
      :return_date
    )
  end

end
