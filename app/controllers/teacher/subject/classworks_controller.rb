class Teacher::Subject::ClassworksController < BaseDashboardController

  before_action :set_subject
  before_action :set_group
  before_action :set_class_work, only: %i[show edit update destroy]

  def new
    @class_work = Admin::Academic::Teacher::Subject::Classwork.new
  end

  def create
    @class_work = Admin::Academic::Teacher::Subject::Classwork.new(class_work_params)
    @class_work.period_id = current_period.id
    @class_work.teacher_id = current_user.id
    @class_work.group_id = @group.id
    @class_work.subject_id = @subject.id
    if @class_work.save
      flash[:success] = 'Cargado con exito'
      redirect_to teacher_subject_online_activities_path(subject_id: @subject, group_id: @group)
    else
      render :new
    end
  end

  def edit; end

  def update
    if @class_work.update(class_work_params)
      flash[:success] = 'Cargado con exito'
      redirect_to teacher_subject_online_activities_path(subject_id: @subject, group_id: @group)
    else
      render :edit
    end
  end

  # Custom routes
  def uploads
    @search = params[:search].to_i
    @teacher_class_work = Admin::Academic::Teacher::Subject::Classwork.find(@search)
    @uploads = @teacher_class_work.student_uploads.includes(:student).order(
      'admin_user_users.first_last_name ASC, admin_user_users.second_last_name ASC'
    )
  end

  def uploads_submit
    params[:scores].each do |content_id, data|
      content = Admin::Academic::Student::Subject::TeacherClassworkUpload.find content_id
      content.update(score: data[:content_score], comments: data[:teacher_comment])

      content.calculate_score(@group, @subject, current_period)
    end

    flash[:success] = 'Notas guardadas con exito'
    redirect_to teacher_subject_online_activities_path(subject_id: @subject, group_id: @group)
  end

  private

  def set_subject
    @subject = Admin::Academic::Subject::Subject.find(params[:subject_id])
  end

  def set_group
    @group = Admin::Academic::Group::Group.find(params[:group_id])
  end

  def set_class_work
    @class_work = Admin::Academic::Teacher::Subject::Classwork.find(params[:id])
  end

  def class_work_params
    params.require(:admin_academic_teacher_subject_classwork).permit(
      :period_id,
      :teacher_id,
      :group_id,
      :subject_id,
      :title,
      :file,
      :description
    )
  end

end
