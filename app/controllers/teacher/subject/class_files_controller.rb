class Teacher::Subject::ClassFilesController < BaseDashboardController

  before_action :set_subject
  before_action :set_group
  before_action :set_file, only: %i[show edit update destroy]

  def show

  end

  def new
    @file = Admin::Academic::Teacher::Subject::ClassFile.new
  end

  def create
    @file = Admin::Academic::Teacher::Subject::ClassFile.new(file_params)
    @file.period_id = current_period.id
    @file.subject_id = @subject.id
    @file.group_id = @group.id
    @file.teacher_id = current_user.id
    if @file.save
      flash[:success] = 'Guardado con exito'
      redirect_to teacher_subject_online_activities_path(subject_id: @subject, group_id: @group)
    else
      render :new
    end
  end

  def edit; end

  def update
    if @file.update(file_params)
      flash[:success] = 'Guardado con exito'
      redirect_to teacher_subject_online_activities_path(subject_id: @subject, group_id: @group)
    else
      render :edit
    end
  end

  def destroy
    @file.destroy
    flash[:success] = 'Guardado con exito'
    redirect_to teacher_subject_online_activities_path(subject_id: @subject, group_id: @group)
  end

  private

  def set_subject
    @subject = Admin::Academic::Subject::Subject.find(params[:subject_id])
  end

  def set_group
    @group = Admin::Academic::Group::Group.find(params[:group_id])
  end

  def set_file
    @file = Admin::Academic::Teacher::Subject::ClassFile.find(params[:id])
  end

  def file_params
    params.require(:admin_academic_teacher_subject_class_file).permit(
      :period_id,
      :group_id,
      :subject_id,
      :teacher_id,
      :title,
      :description,
      :file
    )
  end

end
