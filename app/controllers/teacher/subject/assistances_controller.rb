class Teacher::Subject::AssistancesController < BaseDashboardController

  before_action :set_subject
  before_action :set_group

  def create
    params[:scores].each do |row_id, data|
      row = Admin::Academic::Student::Score::Score.find row_id
      row.update!(assistance_score: data[:assistance_score])
      row.send_score_to_binary
    end

    flash[:success] = 'Notas guardadas con exito'
    redirect_to teacher_subject_online_activities_path(subject_id: @subject, group_id: @group)
  end

  private

  def set_subject
    @subject = Admin::Academic::Subject::Subject.find(params[:subject_id])
  end

  def set_group
    @group = Admin::Academic::Group::Group.find(params[:group_id])
  end

end
