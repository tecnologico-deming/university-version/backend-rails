class Teacher::Subject::OnlineActivitiesController < BaseDashboardController

  before_action :set_subject
  before_action :set_group
  before_action :set_content, only: %i[new create]

  def show
    assistance_list

    @classes = Admin::Academic::Class::Class
               .where(subject_id: @subject.id, group_id: @group.id, period_id: current_period.id).order(date: :asc)

    @class_files = Admin::Academic::Teacher::Subject::ClassFile
                   .where(period_id: current_period.id, group_id: @group.id, subject_id: @subject.id)

    @class_works = Admin::Academic::Teacher::Subject::Classwork
                   .where(period_id: current_period.id, group_id: @group.id, subject_id: @subject.id)
                   .order(created_at: :asc)

    @uploads = Admin::Academic::Student::Subject::ClassworkUpload
               .where(period_id: current_period.id, group_id: @group.id, subject_id: @subject.id)
               .includes(:student).order('admin_user_users.first_last_name ASC, admin_user_users.second_last_name ASC')
  end

  def new
    @contents = Admin::Academic::Student::Subject::ContentUpload.where(
      period_id: current_period.id,
      group_id: @group.id,
      subject_id: @subject.id,
      content_id: @content.id
    ).includes(:student).order('admin_user_users.first_last_name ASC, admin_user_users.second_last_name ASC')
  end

  def create
    params[:scores].each do |content_id, data|
      content = Admin::Academic::Student::Subject::ContentUpload.find content_id
      content.update(teacher_id: current_user.id, score: data[:content_score], teacher_comment: data[:teacher_comment])

      content.send_to_student_score
    end

    flash[:success] = 'Notas guardadas con exito'
    redirect_to teacher_subject_online_activities_path(subject_id: @subject, group_id: @group)
  end

  private

  def assistance_list
    @students = Admin::Academic::Student::Subject::Class
                .where(period_id: current_period.id, group_id: @group.id, subject_id: @subject.id)

    @assistance_list = Admin::Academic::Student::Score::Score
                       .where(period_id: current_period.id, subject_id: @subject.id, student_id: @students.pluck(:student_id))
                       .includes(:student).order('admin_user_users.first_last_name ASC, admin_user_users.second_last_name ASC')
  end

  def set_subject
    @subject = Admin::Academic::Subject::Subject.find(params[:subject_id])
  end

  def set_group
    @group = Admin::Academic::Group::Group.find(params[:group_id])
  end

  def set_content
    @content = Admin::Academic::Subject::Content.find(params[:content_id])
  end

end
