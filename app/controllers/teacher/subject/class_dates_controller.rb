class Teacher::Subject::ClassDatesController < BaseDashboardController

  before_action :set_subject
  before_action :set_group

  def index
    @dates = Admin::Academic::Class::Class.where(
      period_id: current_period.id, subject_id: @subject.id, group_id: @group.id
    ).order(date: :asc)
  end

  private

  def set_subject
    @subject = Admin::Academic::Subject::Subject.find(params[:subject_id])
  end

  def set_group
    @group = Admin::Academic::Group::Group.find(params[:group_id])
  end
end
