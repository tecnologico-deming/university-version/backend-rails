class BaseDashboardController < ApplicationController

  require 'roo'

  include Pagy::Backend

  layout 'dashboard'

  before_action :require_user

end
