class ApplicationController < ActionController::Base

  require 'rqrcode'

  include Pagy::Backend

  skip_before_action :verify_authenticity_token

  helper_method :current_period, :current_user, :ecuador_time, :logged_in?, :require_user

  def current_user
    @current_user ||= Admin::User::User.find(session[:user_id]) if session[:user_id]
  end

  def current_period
    @current_period ||= Admin::Academic::Period::Period.find_by(is_active: true)
  end

  def ecuador_time
    @ecuador_time = Time.zone.now - 5.hours
  end

  def logged_in?
    !!current_user
  end

  def require_user
    unless logged_in?
      flash[:danger] = "Debe iniciar sesión."
      redirect_to root_path
    end
  end

end
