class Student::Subject::ModulesController < BaseDashboardController

  before_action :set_group
  before_action :set_subject
  before_action :set_module, only: %i[new create]
  before_action :set_content, only: %i[new create edit update]
  before_action :set_upload, only: %i[edit update]

  def show
    @class_files = Admin::Academic::Teacher::Subject::ClassFile.where(period_id: current_period.id, group_id: @group.id, subject_id: @subject.id)
    @class_works = Admin::Academic::Teacher::Subject::Classwork
                   .where(period_id: current_period.id, group_id: @group.id, subject_id: @subject).order(created_at: :asc)
    @classes = Admin::Academic::Class::Class.where(subject_id: @subject.id, group_id: @group.id, period_id: current_period.id).order(date: :asc)
  end

  def new
    @upload = Admin::Academic::Student::Subject::ContentUpload.new
  end

  def create
    @upload = Admin::Academic::Student::Subject::ContentUpload.new(upload_params)
    @upload.period_id = current_period.id
    @upload.group_id = @group.id
    @upload.subject_id = @subject.id
    @upload.module_id = @module.id
    @upload.content_id = @content.id
    @upload.student_id = current_user.id
    @upload.content_type_id = @content.type.id
    if @upload.save
      flash[:success] = "Cargado con exito"
      redirect_to student_subject_modules_path(subject_id: @subject, group_id: @group)
    else
      render :new
    end
  end

  def edit; end

  def update
    if @upload.update(upload_params)
      flash[:success] = "Cargado con exito"
      redirect_to student_subject_modules_path(subject_id: @subject, group_id: @group)
    else
      render :edit
    end
  end

  private

  def set_group
    @group = Admin::Academic::Group::Group.find(params[:group_id])
  end

  def set_subject
    @subject = Admin::Academic::Subject::Subject.find(params[:subject_id])
  end

  def set_module
    @module = Admin::Academic::Subject::Module.find(params[:module_id])
  end

  def set_content
    @content = Admin::Academic::Subject::Content.find(params[:content_id])
  end

  def set_upload
    @upload = Admin::Academic::Student::Subject::ContentUpload.find(params[:upload_id])
  end

  def upload_params
    params.require(:admin_academic_student_subject_content_upload).permit(
      :period_id,
      :group_id,
      :subject_id,
      :module_id,
      :content_id,
      :teacher_id,
      :student_id,
      :content_type_id,
      :file
    )
  end

end
