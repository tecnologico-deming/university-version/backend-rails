class Student::Subject::ClassesController < BaseDashboardController

  before_action :set_subject
  before_action :set_group
  before_action :set_classes

  def show; end

  private

  def set_subject
    @subject = Admin::Academic::Subject::Subject.find(params[:subject_id])
  end

  def set_group
    @group = Admin::Academic::Group::Group.find(params[:group_id])
  end

  def set_classes
    @classes = Admin::Academic::Class::Class.where(subject_id: @subject.id, group_id: @group.id, period_id: current_period.id).order(date: :asc)
  end

end
