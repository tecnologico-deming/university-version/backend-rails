class Student::Subject::OnlineContentsController < ApplicationController

  before_action :set_group
  before_action :set_subject
  before_action :set_content

  def new; end

  def create
    @response = Admin::Academic::Student::Subject::ContentResponse.create(
      period_id: current_period.id, group_id: @group.id, subject_id: @subject.id,
      module_id: @content.module.id, content_id: @content.id, student_id: current_user.id
    )

    params[:content].each do |question_id, data|
      answer = Admin::Academic::Subject::ContentQuestionAnswer.find data[:answer].to_i
      @detail = Admin::Academic::Student::Subject::ContentResponseDetail.create(
        response_id: @response.id, question_id: question_id,
        answer_id: answer.id, is_correct: answer.is_correct
      )
    end

    @response.calculate_score
    @response.send_to_student_score

    flash[:success] = "Reto completado con exito"
    redirect_to student_subject_modules_path(subject_id: @subject.id, group_id: @group.id)

  end

  private

  def set_group
    @group = Admin::Academic::Group::Group.find(params[:group_id])
  end

  def set_subject
    @subject = Admin::Academic::Subject::Subject.find(params[:subject_id])
  end

  def set_content
    @content = Admin::Academic::Subject::Content.find(params[:content_id])
  end

  # t.integer :period_id
  # t.integer :group_id
  # t.integer :subject_id
  # t.integer :module_id
  # t.integer :content_id
  # t.integer :student_id
  # t.float :score

end
