class Student::Subject::ClassworksController < BaseDashboardController

  before_action :set_subject
  before_action :set_group
  before_action :set_classwork
  before_action :set_upload, only:  %i[edit update]

  def new
    @upload = Admin::Academic::Student::Subject::TeacherClassworkUpload.new
  end

  def create
    @upload = Admin::Academic::Student::Subject::TeacherClassworkUpload.new(classwork_params)
    @upload.student_id = current_user.id
    @upload.classwork_id = @classwork.id
    if @upload.save
      flash[:success] = "Archivo cargado con exito"
      redirect_to student_subject_modules_path(subject_id: @subject, group_id: @group)
    else
      render :new
    end
  end

  def edit; end

  def update
    if @upload.update(classwork_params)
      flash[:success] = "Archivo cargado con exito"
      redirect_to student_subject_modules_path(subject_id: @subject, group_id: @group)
    else
      render :edit
    end
  end

  private

  def set_subject
    @subject = Admin::Academic::Subject::Subject.find(params[:subject_id])
  end

  def set_group
    @group = Admin::Academic::Group::Group.find(params[:group_id])
  end

  def set_classwork
    @classwork = Admin::Academic::Teacher::Subject::Classwork.find(params[:classwork_id])
  end

  def set_upload
    @upload = Admin::Academic::Student::Subject::TeacherClassworkUpload.find(params[:upload_id])
  end

  def classwork_params
    params.require(:admin_academic_student_subject_teacher_classwork_upload).permit(
      :classwork_id,
      :student_id,
      :file
    )
  end







end
