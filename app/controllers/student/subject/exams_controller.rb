class Student::Subject::ExamsController < BaseDashboardController

  before_action :set_survey, only: %i[teacher_survey teacher_survey_submit]
  before_action :set_exam, only: %i[new create teacher_survey teacher_survey_submit]
  before_action :set_extension_exam, only: %i[extension_exam extension_exam_submit]

  def show
    @regular_exams = Admin::Academic::Student::Exam::Regular.where(
      period_id: current_period.id,
      student_id: current_user.id
    ).order(start_date: :asc)

    @extension_exams = Admin::Academic::Student::Exam::Extension.where(
      period_id: current_period.id,
      student_id: current_user.id
    )
  end

  def new
    Admin::Academic::Student::Exam::RegularDetail.where(exam_id: @exam.id).delete_all
    @exam.update(start_date: ecuador_time, dead_line: ecuador_time + 30.minutes)
    render layout: 'application'
  end

  def create
    submit_exams(@exam)
  end

  # Custom routes

  def results
    if params[:exam_id].present?
      @exam = Admin::Academic::Student::Exam::Regular.find(params[:exam_id])
    elsif params[:extension_exam_id].present?
      @exam = Admin::Academic::Student::Exam::Extension.find(params[:extension_exam_id])
    end
  end

  def extension_exam
    Admin::Academic::Student::Exam::ExtensionDetail.where(exam_id: @extension_exam.id).delete_all
    @extension_exam.update(start_date: ecuador_time, dead_line: ecuador_time + 30.minutes)
    render layout: 'application'
  end

  def extension_exam_submit
    submit_exams(@extension_exam)
  end

  def teacher_survey
    survey_type = Admin::Survey::Teacher::Survey.find_by(name: 'Estudiante')
    @questions = Admin::Survey::Teacher::Question.where(survey_id: survey_type.id)
    render layout: 'application'
  end

  def teacher_survey_submit
    params[:answers].each do |question_id, data|
      answer = Admin::Survey::Teacher::Answer.find data[:survey_answer].to_i

      @details = Admin::Academic::Student::Survey::TeacherResponseDetail.create(
        response_id: @survey.id, question_id: question_id, answer_id: answer.id, score: answer.score
      )
    end

    @survey.update!(fill_date: ecuador_time.to_date)
    @exam.update!(teacher_survey_complete: true)
    flash[:success] = "Gracias por completar"
    redirect_to new_student_subject_modules_exams_path(exam_id: @exam.id)
  end

  private

  # Custom actions
  def submit_exam_details(exam, question_id, answer)
    if @exam.present?
      Admin::Academic::Student::Exam::RegularDetail.find_by(
        exam_id: @exam.id, question_id: question_id, answer_id: answer.id
      ).update(is_correct: answer.is_correct)
    elsif @extension_exam.present?
      Admin::Academic::Student::Exam::ExtensionDetail.find_by(
        exam_id: exam.id, question_id: question_id, answer_id: answer.id
      ).update(is_correct: answer.is_correct)
    end
  end

  def submit_exams(exam)
    params[:answers].each do |question_id, data|
      answer = Admin::Academic::Subject::ExamQuestionAnswer.find data[:question_answer].to_i
      submit_exam_details(exam, question_id, answer)
    end

    exam.update!(score: exam.details.where(is_correct: true).count, end_date: ecuador_time)
    exam.send_to_student_score
    exam.send_score_to_binary
    flash[:success] = "Haz completado tu examen con exito."
    redirect_to student_subject_modules_exams_path
  end


  def set_exam
    @exam = Admin::Academic::Student::Exam::Regular.find(params[:exam_id])
  end

  def set_extension_exam
    @extension_exam = Admin::Academic::Student::Exam::Extension.find(params[:extension_exam_id])
  end

  def set_survey
    @subject = Admin::Academic::Subject::Subject.find(params[:subject_id])

    @survey = Admin::Academic::Student::Survey::TeacherResponse.find_or_create_by(
      period_id: current_period.id,
      student_id: current_user.id,
      subject_id: @subject.id
    )
  end

end
