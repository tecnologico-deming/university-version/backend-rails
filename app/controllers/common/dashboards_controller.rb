class Common::DashboardsController < BaseDashboardController

  before_action :set_user, only: %i[change_password change_password_submit update_info update_info_submit]
  before_action :set_subject, only: %i[subject_dates]
  before_action :set_group, only: %i[subject_dates]

  def index; end

  # Custom actions
  def change_password
    render layout: 'application'
    # @user = Admin::User::User.find(params[:user_id])
  end

  def change_password_submit
    # @user = Admin::User::User.find(params[:user_id])
    if @user.update(password: params[:password], password_changed: true)
      flash[:success] = 'Contraseña cambiada con exito.'
      redirect_to common_dashboards_path
    else
      render :change_password
    end
  end

  def update_info
    render layout: 'application'
  end

  def update_info_submit
    if @user.update(email: params[:email], mobile_number: params[:mobile_number],
                    house_number: params[:house_number], birth_date: params[:birth_date].to_s,
                    need_update: false)

      flash[:success] = 'Muchas gracias por actualizar tu información.'
      redirect_to common_dashboards_path
    else
      render :update_info
    end
  end

  def subject_dates
    @dates = Admin::Academic::Class::Class.find_by(period_id: current_period.id, subject_id: @subject.id, group_id: @group.id)
  end

  private

  def set_user
    @user = Admin::User::User.find(params[:user_id])
  end

  def set_subject
    @subject = Admin::Academic::Subject::Subject.find(params[:subject_id])
  end

  def set_group
    @group = Admin::Academic::Group::Group.find(params[:group_id])
  end

end
