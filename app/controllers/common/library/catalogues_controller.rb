class Common::Library::CataloguesController < BaseDashboardController

  before_action :set_book, only: %i[show reserve_book]
  before_action :set_copy, only: %i[reserve_book]

  def index
    @pagy, @books = pagy(Admin::Library::Book::Book.where(is_active: true).order(title: :asc), items: 8)
    if params[:search].present?
      @books = Admin::Library::Book::Book.order(title: :asc).where(is_active: true).search_books(params[:search])
    end
  end

  def show; end

  # Custom routes

  def reserve_book
    @copy.update(status: 'Reservado')
    Admin::Library::Loan::Reserve.create!(period_id: current_period.id, user_id: current_user.id, copy_id: @copy.id)
    flash[:success] = "Haz reservado este libro con exito."
    redirect_to common_library_catalogues_path
  end

  private

  def set_book
    @book = Admin::Library::Book::Book.find(params[:id])
  end

  def set_copy
    @copy = Admin::Library::Book::Copy.find(params[:copy_id])
  end

end
