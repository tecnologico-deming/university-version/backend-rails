class Common::PublicLibraryController < ApplicationController

  def index
    @pagy, @books = pagy(Admin::Library::Book::Book.where(is_active: true).order(title: :asc), items: 8)
    if params[:search].present?
      @books = Admin::Library::Book::Book.order(title: :asc).where(is_active: true).search_books(params[:search])
    end
  end

  def show
    @book = Admin::Library::Book::Book.find(params[:id])
  end

end
