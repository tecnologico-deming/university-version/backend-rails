class Common::SessionsController < ApplicationController

  def new
    if session[:user_id].present?
      redirect_to common_dashboards_path
    elsif params[:online_catalogue].present?
      redirect_to public_library_path
    end
  end

  def create
    user = Admin::User::User.find_by(identification_number: params[:identification_number])
    if user&.authenticate(params[:password])
      session_message(user)
    else
      flash[:success] = "Por favor revise su usuario y contraseña."
      render :new
    end
  end

  def destroy
    session[:user_id] = nil
    flash[:success] = '¡Hasta la próxima!'
    redirect_to root_path
  end

  # Custom routes

  def private_login
    user = Admin::User::User.find_by(custom_token: params[:custom_token])
    if user
      session[:user_id] = user.id
      redirect_to common_dashboards_path
    end
  end

  # Custom actions

  def session_message(user)
    if user.id && user.status.name == 'Activo'
      session_status(user)
    else
      flash[:success] = "Su usuario se encuentra bloqueado."
      redirect_to root_path
    end
  end

  def session_status(user)
    session[:user_id] = user.id
    if !user.password_changed
      redirect_to change_password_common_dashboards_path(user_id: current_user.id)
    elsif user.need_update?
      redirect_to update_info_common_dashboards_path(user_id: current_user.id)
    elsif params[:ebsco].present?
      redirect_to "https://search.ebscohost.com/login.aspx?authtype=url,uid&custid=ns294178&groupid=main&profile=ehost"
    else
      redirect_to common_dashboards_path
    end
  end

end
