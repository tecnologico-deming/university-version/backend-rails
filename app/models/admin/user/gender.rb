# == Schema Information
#
# Table name: admin_user_genders
#
#  id            :bigint           not null, primary key
#  created_by_id :integer
#  is_active     :boolean
#  name          :string
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#
class Admin::User::Gender < ApplicationRecord

  validates :name, uniqueness: true, presence: true

  has_many :users, class_name: 'Admin::User::User'

end
