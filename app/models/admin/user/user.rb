# == Schema Information
#
# Table name: admin_user_users
#
#  id                     :bigint           not null, primary key
#  status_id              :integer
#  role_id                :integer
#  gender_id              :integer
#  identification_type_id :integer
#  created_by_admin       :boolean
#  identification_number  :string
#  password_digest        :string
#  birth_date             :date
#  custom_token           :string
#  mobile_number          :string
#  house_number           :string
#  email                  :string
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#  first_last_name        :string
#  second_last_name       :string
#  first_name             :string
#  second_name            :string
#  password_changed       :boolean
#  row_id                 :integer
#  need_update            :boolean
#
class Admin::User::User < ApplicationRecord

  has_secure_password

  validates :identification_number, presence: true, uniqueness: true
  validates :first_name, presence: true
  validates :first_last_name, presence: true
  validates :email, presence: true
  validates :mobile_number, presence: true
  validates :birth_date, presence: true

  pg_search_scope :search_users,
                  against: %i[first_last_name second_last_name first_name second_name identification_number],
                  using: { tsearch: { prefix: true } }

  # Academic relations
  has_many :careers, class_name: 'Admin::Academic::Career::Career'
  has_many :contents, class_name: 'Admin::Academic::Subject::Content'
  has_many :content_questions, class_name: 'Admin::Academic::Subject::ContentQuestion'
  has_many :exams, class_name: 'Admin::Academic::Subject::Exam'
  has_many :exam_questions, class_name: 'Admin::Academic::Subject::ExamQuestion'
  has_many :guides, class_name: 'Admin::Academic::Subject::Guide'
  has_many :modules, class_name: 'Admin::Academic::Subject::Module'
  has_many :periods, class_name: 'Admin::Academic::Period::Period'
  has_many :semesters, class_name: 'Admin::Academic::Career::Semester'
  has_many :subjects, class_name: 'Admin::Academic::Subject::Subject'

  has_many :user_groups, class_name: 'Admin::Academic::UserGroup', dependent: :destroy
  has_many :groups, class_name: 'Admin::Academic::Group::Group', through: :user_groups

  # Drive relations
  has_many :shelves, class_name: 'Admin::Drive::Shelf'
  belongs_to :row, class_name: 'Admin::Drive::Row', foreign_key: :row_id, optional: true

  # Library relations
  has_many :authors, class_name: 'Admin::Library::Author::Author'
  has_many :categories, class_name: 'Admin::Library::Category::Category'
  has_many :editorials, class_name: 'Admin::Library::Editorial::Editorial'
  has_many :books, class_name: 'Admin::Library::Book::Book'
  has_many :book_copies, class_name: 'Admin::Library::Book::Copy'
  has_many :book_reserves, class_name: 'Admin::Library::Loan::Reserve'

  # Student relations
  has_many :student_subjects, class_name: 'Admin::Academic::Student::Subject::Class'
  has_many :student_content_uploads, class_name: 'Admin::Academic::Student::Subject::ContentUpload', foreign_key: :student_id

  # Support ticket relations
  has_many :tickets, class_name: 'Admin::Support::Ticket'
  has_many :ticket_details, class_name: 'Admin::Support::TicketDetail'
  has_many :ticket_meetings, class_name: 'Admin::Support::TicketMeeting'
  has_many :ticket_types, class_name: 'Admin::Support::TicketType'

  # Teacher relations
  has_many :teacher_subjects, class_name: 'Admin::Academic::Teacher::Subject::Class'

  # User relations
  belongs_to :role, class_name: 'Admin::User::Role', foreign_key: :role_id
  belongs_to :status, class_name: 'Admin::User::Status', foreign_key: :status_id
  belongs_to :identification_type, class_name: 'Admin::User::IdentificationType', foreign_key: :identification_type_id
  belongs_to :gender, class_name: 'Admin::User::Gender', foreign_key: :gender_id

  def full_name
    "#{first_last_name} #{second_last_name} #{first_name} #{second_name}"
  end

  def numbers
    if house_number.present?
      "#{mobile_number} / #{house_number}"
    else
      "#{mobile_number} / No registra convencional"
    end
  end


end
