# == Schema Information
#
# Table name: admin_user_statuses
#
#  id         :bigint           not null, primary key
#  name       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
class Admin::User::Status < ApplicationRecord

  validates :name, presence: true, uniqueness: true

  has_many :users, class_name: 'Admin::User::User', foreign_key: :status_id

end
