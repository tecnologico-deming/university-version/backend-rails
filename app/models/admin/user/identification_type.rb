# == Schema Information
#
# Table name: admin_user_identification_types
#
#  id            :bigint           not null, primary key
#  created_by_id :integer
#  is_active     :boolean
#  name          :string
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#
class Admin::User::IdentificationType < ApplicationRecord

  validates :name, presence: true, uniqueness: true

  has_many :users, class_name: 'Admin::User::User', foreign_key: :identification_type_id

end
