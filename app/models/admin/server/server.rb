# == Schema Information
#
# Table name: admin_server_servers
#
#  id            :bigint           not null, primary key
#  created_by_id :integer
#  is_active     :boolean
#  name          :string
#  base_url      :string
#  secret_key    :string
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#
class Admin::Server::Server < ApplicationRecord

  belongs_to :created_by, class_name: 'Admin::User::User', foreign_key: :created_by_id

  # has_many :class_dates, class_name: 'Admin::Academic::Class::ClassDate'

end
