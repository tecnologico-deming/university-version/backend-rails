# == Schema Information
#
# Table name: admin_survey_teacher_question_types
#
#  id         :bigint           not null, primary key
#  name       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
class Admin::Survey::Teacher::QuestionType < ApplicationRecord
end
