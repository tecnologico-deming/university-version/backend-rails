# == Schema Information
#
# Table name: admin_survey_teacher_answers
#
#  id          :bigint           not null, primary key
#  question_id :integer
#  title       :text
#  score       :float
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#
class Admin::Survey::Teacher::Answer < ApplicationRecord

  belongs_to :question, class_name: 'Admin::Survey::Teacher::Question', foreign_key: :question_id

end
