# == Schema Information
#
# Table name: admin_survey_teacher_questions
#
#  id               :bigint           not null, primary key
#  survey_id        :integer
#  question_type_id :integer
#  is_active        :boolean
#  title            :text
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#
class Admin::Survey::Teacher::Question < ApplicationRecord

  has_many :answers, class_name: 'Admin::Survey::Teacher::Answer', dependent: :destroy

end
