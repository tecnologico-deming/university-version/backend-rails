# == Schema Information
#
# Table name: admin_support_ticket_details
#
#  id            :bigint           not null, primary key
#  ticket_id     :integer
#  created_by_id :integer
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#
class Admin::Support::TicketDetail < ApplicationRecord

  has_rich_text :comment

  belongs_to :created_by, class_name: 'Admin::User::User', foreign_key: :created_by_id
  belongs_to :ticket, class_name: 'Admin::Support::Ticket', foreign_key: :ticket_id

end
