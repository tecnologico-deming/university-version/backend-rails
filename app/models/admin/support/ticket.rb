# == Schema Information
#
# Table name: admin_support_tickets
#
#  id             :bigint           not null, primary key
#  period_id      :integer
#  type_id        :integer
#  created_by_id  :integer
#  assigned_to_id :integer
#  code           :string
#  status         :string
#  title          :string
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#
class Admin::Support::Ticket < ApplicationRecord

  has_one_attached :file
  has_rich_text :comments

  validates :title, presence: true
  validates :file, size: { less_than: 2.megabytes }

  belongs_to :assigned_to, class_name: 'Admin::User::User', foreign_key: :assigned_to_id, optional: true
  belongs_to :created_by, class_name: 'Admin::User::User', foreign_key: :created_by_id
  belongs_to :period, class_name: 'Admin::Academic::Period::Period', foreign_key: :period_id
  belongs_to :type, class_name: 'Admin::Support::TicketType', foreign_key: :type_id

  has_many :details, class_name: 'Admin::Support::TicketDetail', dependent: :destroy
  has_many :meetings, class_name: 'Admin::Support::TicketMeeting', dependent: :destroy

end
