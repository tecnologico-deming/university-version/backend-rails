# == Schema Information
#
# Table name: admin_support_ticket_types
#
#  id            :bigint           not null, primary key
#  created_by_id :integer
#  is_active     :boolean
#  title         :string
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#
class Admin::Support::TicketType < ApplicationRecord

  belongs_to :created_by, class_name: 'Admin::User::User', foreign_key: :created_by_id

  has_many :tickets, class_name: 'Admin::Support::Ticket'

end
