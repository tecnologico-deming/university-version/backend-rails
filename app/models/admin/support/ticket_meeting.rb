# == Schema Information
#
# Table name: admin_support_ticket_meetings
#
#  id            :bigint           not null, primary key
#  created_by_id :integer
#  ticket_id     :integer
#  date          :datetime
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#
class Admin::Support::TicketMeeting < ApplicationRecord

  belongs_to :created_by, class_name: 'Admin::User::User', foreign_key: :created_by_id
  belongs_to :ticket, class_name: 'Admin::Support::Ticket', foreign_key: :ticket_id

end
