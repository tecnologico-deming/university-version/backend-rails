# == Schema Information
#
# Table name: admin_support_ticket_academic_details
#
#  id            :bigint           not null, primary key
#  created_by_id :integer
#  ticket_id     :integer
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#
class Admin::Support::TicketAcademicDetail < ApplicationRecord

  has_rich_text :family_description
  has_rich_text :educative_description

end
