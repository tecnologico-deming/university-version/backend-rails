# == Schema Information
#
# Table name: admin_drive_shelves
#
#  id            :bigint           not null, primary key
#  created_by_id :integer
#  period_id     :integer
#  department_id :integer
#  is_active     :boolean
#  name          :string
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#
class Admin::Drive::Shelf < ApplicationRecord

  belongs_to :created_by, class_name: 'Admin::User::User', foreign_key: :created_by_id
  belongs_to :department, class_name: 'Admin::Drive::Department', foreign_key: :department_id

  has_many :rows, class_name: 'Admin::Drive::Row', dependent: :destroy
  accepts_nested_attributes_for :rows, allow_destroy: true

end
