# == Schema Information
#
# Table name: admin_drive_departments
#
#  id         :bigint           not null, primary key
#  is_active  :boolean
#  name       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
class Admin::Drive::Department < ApplicationRecord

  validates :name, uniqueness: true

  has_many :shelves, class_name: 'Admin::Drive::Shelf'

end
