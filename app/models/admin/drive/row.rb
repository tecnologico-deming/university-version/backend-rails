# == Schema Information
#
# Table name: admin_drive_rows
#
#  id         :bigint           not null, primary key
#  shelf_id   :integer
#  name       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
class Admin::Drive::Row < ApplicationRecord

  belongs_to :shelf, class_name: 'Admin::Drive::Shelf', foreign_key: :shelf_id, optional: true

  has_many :books, class_name: 'Admin::Library::Book::Book'
  has_many :users, class_name: 'Admin::User::User'

  def full_text
    "Departamento: #{shelf.department.name.titleize} - Estante: #{shelf.name.titleize} - Fila: #{name.titleize}"
  end

end
