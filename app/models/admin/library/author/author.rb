# == Schema Information
#
# Table name: admin_library_author_authors
#
#  id            :bigint           not null, primary key
#  created_by_id :integer
#  is_active     :boolean
#  code          :string
#  full_name     :string
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#
class Admin::Library::Author::Author < ApplicationRecord

  pg_search_scope :search_authors,
                  against: %i[full_name code],
                  using: { tsearch: { prefix: true } }

  validates :full_name, uniqueness: true

  belongs_to :created_by, class_name: 'Admin::User::User', foreign_key: :created_by_id

  has_many :books, class_name: 'Admin::Library::Book::Book'

  def author_full_name
    if full_name.present? && code.present?
      "#{full_name.titleize} - #{code.upcase}"
    else
      "S/A"
    end
  end

end
