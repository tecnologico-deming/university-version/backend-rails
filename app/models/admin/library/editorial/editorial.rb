# == Schema Information
#
# Table name: admin_library_editorial_editorials
#
#  id            :bigint           not null, primary key
#  created_by_id :integer
#  is_active     :boolean
#  name          :string
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#  city_id       :integer
#
class Admin::Library::Editorial::Editorial < ApplicationRecord

  pg_search_scope :search_editorials,
                  against: %i[name],
                  associated_against: { city: %i[name] },
                  using: { tsearch: { prefix: true } }

  belongs_to :created_by, class_name: 'Admin::User::User', foreign_key: :created_by_id
  belongs_to :city, class_name: 'Admin::Config::City', foreign_key: :city_id, optional: true

  has_many :books, class_name: 'Admin::Library::Book::Book'

  def editorial_city_full_name
    if city.present?
      "#{city.name.titleize} - #{city.state.country.name.titleize}"
    else
      'Por asignar'
    end
  end

end
