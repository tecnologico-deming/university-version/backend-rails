# == Schema Information
#
# Table name: admin_library_category_tags
#
#  id            :bigint           not null, primary key
#  created_by_id :integer
#  is_active     :boolean
#  name          :string
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#
class Admin::Library::Category::Tag < ApplicationRecord

  pg_search_scope :search_tags,
                  against: %i[name],
                  using: { tsearch: { prefix: true } }

  belongs_to :created_by, class_name: 'Admin::User::User', foreign_key: :created_by_id

  has_many :book_tags, class_name: 'Admin::Library::Book::BookTag', dependent: :destroy
  has_many :admin_library_book_books, :class_name => 'Admin::Library::Book::Book', through: :book_tags

end
