# == Schema Information
#
# Table name: admin_library_category_categories
#
#  id            :bigint           not null, primary key
#  created_by_id :integer
#  is_active     :boolean
#  code          :string
#  name          :string
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#
class Admin::Library::Category::Category < ApplicationRecord

  validates :name, uniqueness: true, presence: true

  pg_search_scope :search_categories,
                  against: %i[name code],
                  using: { tsearch: { prefix: true } }

  belongs_to :created_by, class_name: 'Admin::User::User', foreign_key: :created_by_id

  has_many :book_categories, class_name: 'Admin::Library::Book::BookCategory', dependent: :destroy
  has_many :books, class_name: 'Admin::Library::Book::Book', through: :book_categories

  has_many :books_category, class_name: 'Admin::Library::Book::Book'


end
