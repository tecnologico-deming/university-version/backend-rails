# == Schema Information
#
# Table name: admin_library_book_books
#
#  id                  :bigint           not null, primary key
#  created_by_id       :integer
#  period_id           :integer
#  campus_id           :integer
#  editorial_id        :integer
#  language_id         :integer
#  main_author_id      :integer
#  origin_id           :integer
#  publish_type_id     :integer
#  row_id              :integer
#  secondary_author_id :integer
#  type_id             :integer
#  is_active           :boolean
#  code_dewey          :string
#  title               :string
#  subtitle            :string
#  pages               :integer
#  height              :float
#  publish_place       :string
#  created_at          :datetime         not null
#  updated_at          :datetime         not null
#  copies              :integer
#  publish_date        :integer
#  category_id         :integer
#  isbn_code           :string
#
class Admin::Library::Book::Book < ApplicationRecord

  pg_search_scope :search_books,
                  against: %i[title],
                  associated_against: {
                    tags: %i[name],
                    editorial: %i[name],
                    main_author: %i[full_name],
                    secondary_author: %i[full_name]
                  },
                  using: { tsearch: { prefix: true } }

  has_rich_text :comment
  has_rich_text :description
  has_rich_text :resume
  has_rich_text :physic_details

  has_one_attached :file
  has_one_attached :image

  belongs_to :category, class_name: 'Admin::Library::Category::Category', foreign_key: :category_id, optional: true
  belongs_to :created_by, class_name: 'Admin::User::User', foreign_key: :created_by_id, optional: true
  belongs_to :editorial, class_name: 'Admin::Library::Editorial::Editorial', foreign_key: :editorial_id, optional: true
  belongs_to :headquarter, class_name: 'Admin::Academic::Headquarter::Headquarter', foreign_key: :campus_id, optional: true
  belongs_to :language, class_name: 'Admin::Config::Language', foreign_key: :language_id, optional: true
  belongs_to :main_author, class_name: 'Admin::Library::Author::Author', foreign_key: :main_author_id, optional: true
  belongs_to :origin, class_name: 'Admin::Library::Book::Origin', foreign_key: :origin_id, optional: true
  belongs_to :period, class_name: 'Admin::Academic::Period::Period', foreign_key: :period_id, optional: true
  belongs_to :publish_type, class_name: 'Admin::Library::Book::PublishType', foreign_key: :publish_type_id, optional: true
  belongs_to :row, class_name: 'Admin::Drive::Row', foreign_key: :row_id, optional: true
  belongs_to :secondary_author, class_name: 'Admin::Library::Author::Author', foreign_key: :secondary_author_id, optional: true
  belongs_to :type, class_name: 'Admin::Library::Book::Type', foreign_key: :type_id, optional: true

  has_many :book_copies, class_name: 'Admin::Library::Book::Copy', dependent: :destroy

  has_many :book_tags, class_name: 'Admin::Library::Book::BookTag', dependent: :destroy
  has_many :tags, class_name: 'Admin::Library::Category::Tag', through: :book_tags

  has_many :book_subjects, class_name: 'Admin::Library::Book::BookSubject', dependent: :destroy
  has_many :subjects, class_name: 'Admin::Academic::Subject::Subject', through: :book_subjects

  def create_copies(book, current_user, data)
    copies = book.copies
    code = data['COD. INVENTARIO'] if data['COD. INVENTARIO'].present?
    edition_number = data['N° EDICION'] if data['N° EDICION'].present?

    copies.times do
      book = Admin::Library::Book::Copy.create!(
        created_by_id: current_user.id,
        book_id: book.id,
        is_active: true,
        status: 'Disponible',
        stock_code: code,
        edition_number: edition_number
      )
    end
  end

  def book_shelf_name
    if row.present?
      "#{row.shelf.name.humanize} - #{row.name.humanize}"
    else
      "Asignar ubicación"
    end
  end

end
