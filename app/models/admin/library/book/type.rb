# == Schema Information
#
# Table name: admin_library_book_types
#
#  id            :bigint           not null, primary key
#  created_by_id :integer
#  name          :string
#  is_active     :boolean
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#
class Admin::Library::Book::Type < ApplicationRecord

  validates :name, uniqueness: true, presence: true

end
