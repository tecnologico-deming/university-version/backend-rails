# == Schema Information
#
# Table name: admin_library_book_book_categories
#
#  id          :bigint           not null, primary key
#  book_id     :integer
#  category_id :integer
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#
class Admin::Library::Book::BookCategory < ApplicationRecord

  belongs_to :book, class_name: 'Admin::Library::Book::Book', optional: true
  belongs_to :category, class_name: 'Admin::Library::Category::Category', optional: true

end
