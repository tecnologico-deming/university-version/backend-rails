# == Schema Information
#
# Table name: admin_library_book_copies
#
#  id             :bigint           not null, primary key
#  created_by_id  :integer
#  book_id        :integer
#  is_active      :boolean
#  status         :string
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#  stock_code     :integer
#  edition_number :string
#
class Admin::Library::Book::Copy < ApplicationRecord

  belongs_to :book, class_name: 'Admin::Library::Book::Book', foreign_key: :book_id
  belongs_to :created_by, class_name: 'Admin::User::User', foreign_key: :created_by_id

  has_many :reserves, class_name: 'Admin::Library::Loan::Reserve', foreign_key: :copy_id, dependent: :destroy

end
