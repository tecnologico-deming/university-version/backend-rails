# == Schema Information
#
# Table name: admin_library_book_book_subjects
#
#  id         :bigint           not null, primary key
#  book_id    :integer
#  subject_id :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
class Admin::Library::Book::BookSubject < ApplicationRecord

  belongs_to :subject, class_name: 'Admin::Academic::Subject::Subject', optional: true
  belongs_to :book, class_name: 'Admin::Library::Book::Book', optional: true

end
