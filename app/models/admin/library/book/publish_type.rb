# == Schema Information
#
# Table name: admin_library_book_publish_types
#
#  id            :bigint           not null, primary key
#  created_by_id :integer
#  name          :string
#  is_active     :boolean
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#
class Admin::Library::Book::PublishType < ApplicationRecord

  validates :name, presence: true, uniqueness: true

  has_many :books, class_name: 'Admin::Library::Book::Book'

end
