# == Schema Information
#
# Table name: admin_library_book_book_tags
#
#  id         :bigint           not null, primary key
#  book_id    :integer
#  tag_id     :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
class Admin::Library::Book::BookTag < ApplicationRecord

  belongs_to :book, class_name: 'Admin::Library::Book::Book', foreign_key: :book_id, optional: true
  belongs_to :tag, class_name: 'Admin::Library::Category::Tag', foreign_key: :tag_id, optional: true

end
