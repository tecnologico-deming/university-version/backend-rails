# == Schema Information
#
# Table name: admin_library_loan_reserves
#
#  id                :bigint           not null, primary key
#  period_id         :integer
#  user_id           :integer
#  copy_id           :integer
#  is_approved_by_id :integer
#  is_approved       :boolean
#  comments          :text
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#
class Admin::Library::Loan::Reserve < ApplicationRecord

  belongs_to :copy, class_name: 'Admin::Library::Book::Copy', foreign_key: :copy_id
  belongs_to :user, class_name: 'Admin::User::User', foreign_key: :user_id

  has_one :loan, class_name: 'Admin::Library::Loan::Loan', dependent: :destroy

  def updated_reserve(reserve, ecuador_time)
    if reserve.is_approved?
      Admin::Library::Book::Copy.find(reserve.copy_id).update(status: 'Prestado')
      Admin::Library::Loan::Loan.create!(reserve_id: reserve.id, was_received: false, return_date: ecuador_time.to_date + 3.days)
    else
      reserve.loan.destroy if reserve.loan.present?
      Admin::Library::Book::Copy.find(reserve.copy_id).update(status: 'Disponible')
    end
  end

  def deleted_reserve(reserve)
    Admin::Library::Book::Copy.find(reserve.copy_id).update(status: 'Disponible')
  end

end
