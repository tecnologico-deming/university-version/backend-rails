# == Schema Information
#
# Table name: admin_library_loan_loans
#
#  id                 :bigint           not null, primary key
#  reserve_id         :integer
#  was_received_by_id :integer
#  was_received       :boolean
#  was_received_date  :datetime
#  return_date        :date
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#
class Admin::Library::Loan::Loan < ApplicationRecord

  belongs_to :reserve, class_name: 'Admin::Library::Loan::Reserve', foreign_key: :reserve_id

end
