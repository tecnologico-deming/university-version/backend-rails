# == Schema Information
#
# Table name: admin_config_countries
#
#  id            :bigint           not null, primary key
#  created_by_id :integer
#  is_active     :boolean
#  name          :string
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#
class Admin::Config::Country < ApplicationRecord

  validates :name, uniqueness: true, presence: true

  pg_search_scope :search_countries,
                  against: %i[name],
                  using: { tsearch: { prefix: true } }

  belongs_to :created_by, class_name: 'Admin::User::User', foreign_key: :created_by_id

  has_many :states, class_name: 'Admin::Config::State', dependent: :destroy

  # Library relations
  has_many :editorials, class_name: 'Admin::Library::Editorial::Editorial', foreign_key: :country_id

end
