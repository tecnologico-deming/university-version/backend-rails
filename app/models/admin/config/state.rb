# == Schema Information
#
# Table name: admin_config_states
#
#  id            :bigint           not null, primary key
#  created_by_id :integer
#  country_id    :integer
#  name          :string
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#
class Admin::Config::State < ApplicationRecord

  validates :name, uniqueness: true, presence: true

  pg_search_scope :search_states,
                  against: %i[name],
                  using: { tsearch: { prefix: true } }

  belongs_to :country, class_name: 'Admin::Config::Country', foreign_key: :country_id
  belongs_to :created_by, class_name: 'Admin::User::User', foreign_key: :created_by_id

  has_many :cities, class_name: 'Admin::Config::City', dependent: :destroy

end
