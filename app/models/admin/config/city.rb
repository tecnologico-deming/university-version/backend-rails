# == Schema Information
#
# Table name: admin_config_cities
#
#  id            :bigint           not null, primary key
#  created_by_id :integer
#  state_id      :integer
#  name          :string
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#
class Admin::Config::City < ApplicationRecord

  pg_search_scope :search_cities,
                  against: %i[name],
                  using: { tsearch: { prefix: true } }

  belongs_to :created_by, class_name: 'Admin::User::User', foreign_key: :created_by_id
  belongs_to :state, class_name: 'Admin::Config::State', foreign_key: :state_id

end
