# == Schema Information
#
# Table name: admin_config_languages
#
#  id            :bigint           not null, primary key
#  created_by_id :integer
#  name          :string
#  is_active     :boolean
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#
class Admin::Config::Language < ApplicationRecord

  has_many :books, class_name: 'Admin::Library::Book::Book'

end
