# == Schema Information
#
# Table name: admin_academic_user_groups
#
#  id            :bigint           not null, primary key
#  created_by_id :integer
#  user_id       :integer
#  group_id      :integer
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#
class Admin::Academic::UserGroup < ApplicationRecord

  belongs_to :group, class_name: 'Admin::Academic::Group::Group', foreign_key: :group_id, optional: true
  belongs_to :user, class_name: 'Admin::User::User', foreign_key: :user_id, optional: true

end
