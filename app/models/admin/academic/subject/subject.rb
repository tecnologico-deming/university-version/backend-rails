# == Schema Information
#
# Table name: admin_academic_subject_subjects
#
#  id            :bigint           not null, primary key
#  created_by_id :integer
#  semester_id   :integer
#  name          :string
#  code          :string
#  credits       :integer
#  hours         :float
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#
class Admin::Academic::Subject::Subject < ApplicationRecord

  belongs_to :created_by, class_name: 'Admin::User::User', foreign_key: :created_by_id
  belongs_to :semester, class_name: 'Admin::Academic::Career::Semester', foreign_key: :semester_id, optional: true

  # Activation relations
  has_many :activations, class_name: 'Admin::Academic::Binary::Activation'

  # Career relations
  has_many :subject_semesters, class_name: 'Admin::Academic::Subject::SubjectSemester', dependent: :destroy
  has_many :semesters, class_name: 'Admin::Academic::Career::Semester', through: :subject_semesters

  # Classes relations
  has_many :classes, class_name: 'Admin::Academic::Class::Class', foreign_key: :subject_id

  # Library relations
  has_many :book_subjects, class_name: 'Admin::Library::Book::BookSubject', dependent: :destroy
  has_many :books, class_name: 'Admin::Library::Book::Book', through: :book_subjects

  # Subject relations
  has_many :exams, class_name: 'Admin::Academic::Subject::Exam', dependent: :destroy
  has_many :guides, class_name: 'Admin::Academic::Subject::Guide', dependent: :destroy
  has_many :classworks, class_name: 'Admin::Academic::Subject::Classwork', dependent: :destroy

  has_many :modules, class_name: 'Admin::Academic::Subject::Module', dependent: :destroy
  accepts_nested_attributes_for :modules, allow_destroy: true

  # Student relations
  has_many :student_subjects, class_name: 'Admin::Academic::Student::Subject::Class', dependent: :destroy

  def full_name
    "#{name.titleize} - #{code.upcase}"
  end

  def create_modules(params, subject, current_user)
    if params[:admin_academic_subject_subject][:modules_attributes].present?
      subject.modules.each do |mod|
        mod.update(is_active: true, created_by_id: current_user.id)
      end
    end
  end

end
