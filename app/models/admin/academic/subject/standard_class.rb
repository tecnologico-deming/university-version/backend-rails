# == Schema Information
#
# Table name: admin_academic_subject_standard_classes
#
#  id            :bigint           not null, primary key
#  created_by_id :integer
#  period_id     :integer
#  subject_id    :integer
#  teacher_id    :integer
#  is_active     :boolean
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#
class Admin::Academic::Subject::StandardClass < ApplicationRecord
end
