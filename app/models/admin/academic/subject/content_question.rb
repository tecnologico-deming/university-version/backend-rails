# == Schema Information
#
# Table name: admin_academic_subject_content_questions
#
#  id            :bigint           not null, primary key
#  created_by_id :integer
#  period_id     :integer
#  is_active     :boolean
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#  content_id    :integer
#
class Admin::Academic::Subject::ContentQuestion < ApplicationRecord

  has_rich_text :title

  belongs_to :created_by, class_name: 'Admin::User::User', foreign_key: :created_by_id
  belongs_to :content, class_name: 'Admin::Academic::Subject::Content', foreign_key: :content_id

  has_many :answers, class_name: 'Admin::Academic::Subject::ContentQuestionAnswer', dependent: :destroy
  accepts_nested_attributes_for :answers, allow_destroy: true


end
