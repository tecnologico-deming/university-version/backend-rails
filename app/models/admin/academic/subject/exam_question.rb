# == Schema Information
#
# Table name: admin_academic_subject_exam_questions
#
#  id            :bigint           not null, primary key
#  created_by_id :integer
#  exam_id       :integer
#  is_active     :boolean
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#
class Admin::Academic::Subject::ExamQuestion < ApplicationRecord

  has_rich_text :title

  belongs_to :created_by, class_name: 'Admin::User::User', foreign_key: :created_by_id
  belongs_to :exam, class_name: 'Admin::Academic::Subject::Exam', foreign_key: :exam_id

  has_many :answers, class_name: 'Admin::Academic::Subject::ExamQuestionAnswer', dependent: :destroy
  accepts_nested_attributes_for :answers, allow_destroy: true

end
