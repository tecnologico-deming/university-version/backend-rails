# == Schema Information
#
# Table name: admin_academic_subject_exam_question_answers
#
#  id               :bigint           not null, primary key
#  exam_question_id :integer
#  is_correct       :boolean
#  title            :text
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#
class Admin::Academic::Subject::ExamQuestionAnswer < ApplicationRecord

  belongs_to :question, class_name: 'Admin::Academic::Subject::ExamQuestion', foreign_key: :exam_question_id, optional: true

end
