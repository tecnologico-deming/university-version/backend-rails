# == Schema Information
#
# Table name: admin_academic_subject_exams
#
#  id            :bigint           not null, primary key
#  created_by_id :integer
#  period_id     :integer
#  subject_id    :integer
#  is_active     :boolean
#  name          :string
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#
class Admin::Academic::Subject::Exam < ApplicationRecord

  belongs_to :created_by, class_name: 'Admin::User::User', foreign_key: :created_by_id
  belongs_to :period, class_name: 'Admin::Academic::Period::Period', foreign_key: :period_id
  belongs_to :subject, class_name: 'Admin::Academic::Subject::Subject', foreign_key: :subject_id

  has_many :questions, class_name: 'Admin::Academic::Subject::ExamQuestion'

end
