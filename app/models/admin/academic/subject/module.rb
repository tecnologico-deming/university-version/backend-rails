# == Schema Information
#
# Table name: admin_academic_subject_modules
#
#  id            :bigint           not null, primary key
#  created_by_id :integer
#  subject_id    :integer
#  period_id     :integer
#  is_active     :boolean
#  order         :integer
#  name          :string
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#
class Admin::Academic::Subject::Module < ApplicationRecord

  belongs_to :created_by, class_name: 'Admin::User::User', foreign_key: :created_by_id, optional: true
  belongs_to :subject, class_name: 'Admin::Academic::Subject::Subject', foreign_key: :subject_id, optional: true

  has_many :contents, class_name: 'Admin::Academic::Subject::Content', dependent: :destroy

  def set_order(subject, mod)
    last_item = Admin::Academic::Subject::Module.where(subject_id: subject.id).last
    if last_item.present?
      mod.update(order: last_item.order + 1)
    else
      mod.update(order: 1)
    end
  end

end
