# == Schema Information
#
# Table name: admin_academic_subject_content_types
#
#  id         :bigint           not null, primary key
#  is_active  :boolean
#  name       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
class Admin::Academic::Subject::ContentType < ApplicationRecord

  validates :name, uniqueness: true

  has_many :contents, class_name: 'Admin::Academic::Subject::Content'

end
