# == Schema Information
#
# Table name: admin_academic_subject_contents
#
#  id            :bigint           not null, primary key
#  created_by_id :integer
#  period_id     :integer
#  module_id     :integer
#  type_id       :integer
#  is_active     :boolean
#  name          :string
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#  order         :integer
#
class Admin::Academic::Subject::Content < ApplicationRecord

  has_one_attached :file

  # validates :file, size: { less_than: 5.megabytes }

  belongs_to :created_by, class_name: 'Admin::User::User', foreign_key: :created_by_id
  belongs_to :module, class_name: 'Admin::Academic::Subject::Module', foreign_key: :module_id
  belongs_to :type, class_name: 'Admin::Academic::Subject::ContentType', foreign_key: :type_id

  has_many :content_questions, class_name: 'Admin::Academic::Subject::ContentQuestion'
  has_many :student_uploads, class_name: 'Admin::Academic::Student::Subject::ContentUpload'
  has_many :student_content_responses, class_name: 'Admin::Academic::Student::Subject::ContentResponse'

  def set_order(mod, content)
    last_item = Admin::Academic::Subject::Content.where(module_id: mod.id).last
    if last_item.present?
      content.update(order: last_item.order + 1)
    else
      content.update(order: 1)
    end
  end

end
