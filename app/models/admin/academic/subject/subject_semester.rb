# == Schema Information
#
# Table name: admin_academic_subject_subject_semesters
#
#  id          :bigint           not null, primary key
#  subject_id  :integer
#  semester_id :integer
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#
class Admin::Academic::Subject::SubjectSemester < ApplicationRecord

  belongs_to :subject, class_name: 'Admin::Academic::Subject::Subject', foreign_key: :subject_id, optional: true
  belongs_to :semester, class_name: 'Admin::Academic::Career::Semester', foreign_key: :semester_id, optional: true

end
