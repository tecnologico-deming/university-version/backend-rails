# == Schema Information
#
# Table name: admin_academic_subject_content_question_answers
#
#  id                  :bigint           not null, primary key
#  is_active           :boolean
#  title               :string
#  created_at          :datetime         not null
#  updated_at          :datetime         not null
#  content_question_id :integer
#  is_correct          :boolean
#
class Admin::Academic::Subject::ContentQuestionAnswer < ApplicationRecord

  belongs_to :question, class_name: 'Admin::Academic::Subject::ContentQuestion', foreign_key: :content_question_id, optional: true

end
