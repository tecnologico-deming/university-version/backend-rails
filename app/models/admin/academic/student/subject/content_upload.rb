# == Schema Information
#
# Table name: admin_academic_student_subject_content_uploads
#
#  id                :bigint           not null, primary key
#  period_id         :integer
#  group_id          :integer
#  subject_id        :integer
#  module_id         :integer
#  content_id        :integer
#  teacher_id        :integer
#  student_id        :integer
#  score             :float
#  teacher_comment   :text
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#  content_type_id   :integer
#  content_type_name :string
#  module_name       :string
#
class Admin::Academic::Student::Subject::ContentUpload < ApplicationRecord

  has_one_attached :file

  belongs_to :content, class_name: 'Admin::Academic::Subject::Content', foreign_key: :content_id, optional: true
  belongs_to :student, class_name: 'Admin::User::User', foreign_key: :student_id
  belongs_to :subject, class_name: 'Admin::Academic::Subject::Subject', foreign_key: :subject_id

  def send_score_to_binary
    data_to_send = {
      data: {
        id_materia: subject_id,
        cedula: student.identification_number,
        calificacion: score.to_f
      }
    }

    response = Communicator::Binary::HTTP.send_online_activities_score(
      data_to_send
    )

    response
  end


  def send_to_student_score
    file_final_score = 0
    online_final_score = 0
    project_final_score = 0

    modules = Admin::Academic::Subject::Module.where(subject_id: subject_id)

    file_module_count(modules)
    project_module_count(modules)
    online_module_count(modules)

    if file_module_count(modules).positive?
      file_rows = Admin::Academic::Student::Subject::ContentUpload.where(
        student_id: student_id,
        subject_id: subject_id,
        content_type_id: Admin::Academic::Subject::ContentType.find_by(name: 'Reto').id
      )

      file_score = file_rows.sum(:score).to_f
      file_final_score = file_score
    end

    if online_module_count(modules).positive?
      online_rows = Admin::Academic::Student::Subject::ContentResponse.where(student_id: student_id, subject_id: subject_id)
      score = online_rows.sum(:score).to_f
      online_final_score = score
    end

    if project_module_count(modules).positive?
      project_row = Admin::Academic::Student::Subject::ContentUpload.where(
        student_id: student_id,
        subject_id: subject_id,
        content_type_id: Admin::Academic::Subject::ContentType.find_by(name: 'Proyecto').id
      )

      project_score = project_row.sum(:score).to_f * 0.6
      project_final_score = project_score
    end

    global_module_count = file_module_count(modules) + online_module_count(modules)
    online_activities_final_score = ((file_final_score + online_final_score) / global_module_count).to_f * 0.4

    final_score = online_activities_final_score + project_final_score

    online_activities_score = Admin::Academic::Student::Score::Score.find_by(
      period_id: period_id, student_id: student_id, subject_id: subject_id
    )

    online_activities_score.update(online_activities_score: final_score)

    data_to_send = {
      data: {
        id_materia: subject_id,
        cedula: student.identification_number,
        calificacion: final_score.to_f
      }
    }

    response = Communicator::Binary::HTTP.send_online_activities_score(
      data_to_send
    )

    response

  end

  def file_module_count(modules)
    Admin::Academic::Subject::Content.where(
      module_id: modules.ids,
      type_id: Admin::Academic::Subject::ContentType.find_by(name: 'Reto').id,
      is_active: true
    ).count
  end

  def project_module_count(modules)
    Admin::Academic::Subject::Content.where(
      module_id: modules.ids,
      type_id: Admin::Academic::Subject::ContentType.find_by(name: 'Proyecto').id,
      is_active: true
    ).count
  end

  def online_module_count(modules)
    Admin::Academic::Subject::Content.where(
      module_id: modules.ids,
      type_id: Admin::Academic::Subject::ContentType.find_by(name: 'Reto en linea').id,
      is_active: true
    ).count.to_i
  end

end
