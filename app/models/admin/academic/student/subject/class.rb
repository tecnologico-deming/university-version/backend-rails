# == Schema Information
#
# Table name: admin_academic_student_subject_classes
#
#  id                  :bigint           not null, primary key
#  activation_id       :integer
#  period_id           :integer
#  subject_id          :integer
#  group_id            :integer
#  teacher_id          :integer
#  student_id          :integer
#  due_date            :date
#  exam_date           :date
#  extension_exam_date :date
#  created_at          :datetime         not null
#  updated_at          :datetime         not null
#
class Admin::Academic::Student::Subject::Class < ApplicationRecord

  belongs_to :activation, class_name: 'Admin::Academic::Binary::Activation', foreign_key: :activation_id, optional: true
  belongs_to :group, class_name: 'Admin::Academic::Group::Group', foreign_key: :group_id, optional: true
  belongs_to :student, class_name: 'Admin::User::User', foreign_key: :student_id
  belongs_to :subject, class_name: 'Admin::Academic::Subject::Subject', foreign_key: :subject_id
  belongs_to :teacher, class_name: 'Admin::User::User', foreign_key: :teacher_id

  has_many :classes, class_name: 'Admin::Academic::Class::Class', foreign_key: :subject_id
  has_many :extensions, class_name: 'Admin::Academic::Student::Subject::ClassExtension', dependent: :destroy, foreign_key: :student_class_id

end
