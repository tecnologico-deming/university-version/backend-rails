# == Schema Information
#
# Table name: admin_academic_student_subject_class_extensions
#
#  id               :bigint           not null, primary key
#  created_by_id    :integer
#  student_class_id :integer
#  date             :date
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#
class Admin::Academic::Student::Subject::ClassExtension < ApplicationRecord

  belongs_to :student_class, class_name: 'Admin::Academic::Student::Subject::Class', foreign_key: :student_class_id

end
