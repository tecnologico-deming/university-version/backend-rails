# == Schema Information
#
# Table name: admin_academic_student_subject_teacher_classwork_uploads
#
#  id           :bigint           not null, primary key
#  classwork_id :integer
#  student_id   :integer
#  score        :float
#  comments     :text
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#
class Admin::Academic::Student::Subject::TeacherClassworkUpload < ApplicationRecord

  has_one_attached :file

  belongs_to :classwork, class_name: 'Admin::Academic::Teacher::Subject::Classwork', foreign_key: :classwork_id
  belongs_to :student, class_name: 'Admin::User::User', foreign_key: :student_id

  def calculate_score(group, subject, period)
    teacher_uploads = Admin::Academic::Teacher::Subject::Classwork
                      .where(period_id: period.id, group_id: group.id, subject_id: subject.id)
    student_uploads = Admin::Academic::Student::Subject::TeacherClassworkUpload.where(classwork_id: teacher_uploads.pluck(:id))
    student_uploads_score = student_uploads.sum(:score).to_f
    final_score = student_uploads_score / teacher_uploads.count
    prom_final_score = final_score * 0.3
    row = Admin::Academic::Student::Score::Score
          .find_by(period_id: period.id, subject_id: subject.id, student_id: student_uploads.first.student_id)
    row.update(class_score: prom_final_score)
    send_score_to_binary(row)
  end

  def send_score_to_binary(row)
    data_to_send = {
      data: {
        id_materia: row.subject_id,
        cedula: row.student.identification_number,
        calificacion: row.class_score.to_f
      }
    }

    response = Communicator::Binary::HTTP.send_classwork_score(data_to_send)

    response
  end

end
