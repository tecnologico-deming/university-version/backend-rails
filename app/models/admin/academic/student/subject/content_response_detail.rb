# == Schema Information
#
# Table name: admin_academic_student_subject_content_response_details
#
#  id          :bigint           not null, primary key
#  response_id :integer
#  question_id :integer
#  answer_id   :integer
#  is_correct  :boolean
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#
class Admin::Academic::Student::Subject::ContentResponseDetail < ApplicationRecord

  belongs_to :response, class_name: 'Admin::Academic::Student::Subject::ContentResponse', foreign_key: :response_id

end
