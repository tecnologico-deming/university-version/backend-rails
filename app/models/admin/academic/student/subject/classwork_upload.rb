# == Schema Information
#
# Table name: admin_academic_student_subject_classwork_uploads
#
#  id           :bigint           not null, primary key
#  period_id    :integer
#  subject_id   :integer
#  teacher_id   :integer
#  student_id   :integer
#  group_id     :integer
#  classwork_id :integer
#  score        :float
#  comment      :text
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#
class Admin::Academic::Student::Subject::ClassworkUpload < ApplicationRecord

  has_one_attached :file

  belongs_to :student, class_name: 'Admin::User::User', foreign_key: :student_id
  belongs_to :subject, class_name: 'Admin::Academic::Subject::Subject', foreign_key: :subject_id

  def send_score_to_binary
    data_to_send = {
      data: {
        id_materia: subject_id,
        cedula: student.identification_number,
        calificacion: score.to_f
      }
    }

    response = Communicator::Binary::HTTP.send_classwork_score(
      data_to_send
    )

    response
  end

  def send_to_student_subject_scores(content, data)
    row = Admin::Academic::Student::Score::Score.find_by(
      period_id: content.period_id,
      subject_id: content.subject_id,
      student_id: content.student_id
    )

    row.update(class_score: data[:content_score])
  end

end
