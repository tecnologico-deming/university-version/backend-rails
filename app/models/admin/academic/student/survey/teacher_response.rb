# == Schema Information
#
# Table name: admin_academic_student_survey_teacher_responses
#
#  id            :bigint           not null, primary key
#  activation_id :integer
#  period_id     :integer
#  survey_id     :integer
#  subject_id    :integer
#  group_id      :integer
#  teacher_id    :integer
#  student_id    :integer
#  start_date    :date
#  fill_date     :datetime
#  score         :float
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#
class Admin::Academic::Student::Survey::TeacherResponse < ApplicationRecord

  belongs_to :period, class_name: 'Admin::Academic::Period::Period', foreign_key: :period_id
  belongs_to :subject, class_name: 'Admin::Academic::Subject::Subject', foreign_key: :subject_id
  belongs_to :teacher, class_name: 'Admin::User::User', foreign_key: :teacher_id
  belongs_to :student, class_name: 'Admin::User::User', foreign_key: :student_id

  has_many :details, class_name: 'Admin::Academic::Student::Survey::TeacherResponseDetail', dependent: :destroy, foreign_key: :response_id

  def calculate_score
    scores = details.sum(:score)
    update(score: scores)
  end

end
