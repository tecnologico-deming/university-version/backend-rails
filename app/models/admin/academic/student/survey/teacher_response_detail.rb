# == Schema Information
#
# Table name: admin_academic_student_survey_teacher_response_details
#
#  id          :bigint           not null, primary key
#  response_id :integer
#  question_id :integer
#  answer_id   :integer
#  score       :integer
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#
class Admin::Academic::Student::Survey::TeacherResponseDetail < ApplicationRecord
end
