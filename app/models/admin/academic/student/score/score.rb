# == Schema Information
#
# Table name: admin_academic_student_score_scores
#
#  id                      :bigint           not null, primary key
#  activation_id           :integer
#  period_id               :integer
#  subject_id              :integer
#  teacher_id              :integer
#  student_id              :integer
#  final_status            :string
#  class_score             :float
#  assistance_score        :float
#  online_activities_score :float
#  exam_score              :float
#  extension_exam_score    :float
#  remedial_exam_score     :float
#  total_score             :float
#  created_at              :datetime         not null
#  updated_at              :datetime         not null
#
class Admin::Academic::Student::Score::Score < ApplicationRecord

  belongs_to :student, class_name: 'Admin::User::User', foreign_key: :student_id
  belongs_to :subject, class_name: 'Admin::Academic::Subject::Subject', foreign_key: :subject_id

  def send_score_to_binary
    data_to_send = {
      data: {
        id_materia: subject_id,
        cedula: student.identification_number,
        calificacion: assistance_score.to_f
      }
    }

    response = Communicator::Binary::HTTP.send_classwork_score(data_to_send)

    response
  end

  def calculate_score
    update(total_score: (class_score.to_f * 0.3) + (online_activities_score.to_f * 0.4) + (exam_score.to_f * 0.3))
  end

  def self.nil_online_activities_score
    scores = Admin::Academic::Student::Score::Score
             .where(activation_id: binary_activations, online_activities_score: nil)

    scores.each do |score|
      score.update(online_activities_score: 0) if content_uploads(score).blank? && content_responses(score).blank?
      send_online_activities_data(score) if score.online_activities_score.present?
    end
  end

  def self.nil_classwork_uploads
    scores = Admin::Academic::Student::Score::Score
             .where(activation_id: binary_activations, class_score: nil)
    scores.each do |score|
      score.update(class_score: 0) if classwork_uploads(score).blank?
      send_classwork_data(score) if score.class_score.present?
    end
  end

  def self.binary_activations
    current_period = Admin::Academic::Period::Period.find_by(is_active: true)
    Admin::Academic::Binary::Activation
      .where('student_due_date < ?', Date.today)
      .where(period_id: current_period).pluck(:id)
  end

  def self.content_uploads(score)
    Admin::Academic::Student::Subject::ContentUpload
      .where(student_id: score.student_id, subject_id: score.subject_id, period_id: score.period_id)
  end

  def self.content_responses(score)
    Admin::Academic::Student::Subject::ContentResponse
      .where(student_id: score.student_id, subject_id: score.subject_id, period_id: score.period_id)
  end

  def self.classwork_uploads(score)
    Admin::Academic::Student::Subject::ClassworkUpload
      .where(student_id: score.student_id, subject_id: score.subject_id, period_id: score.period_id)
  end

  def self.send_classwork_data(score)
    data_to_send = {
      data: {
        id_materia: score.subject.id,
        cedula: score.student.identification_number,
        calificacion: score.class_score.to_f
      }
    }

    response = Communicator::Binary::HTTP.send_classwork_score(data_to_send)
    response
  end

  def self.send_online_activities_data(score)
    data_to_send = {
      data: {
        id_materia: score.subject.id,
        cedula: score.student.identification_number,
        calificacion: score.online_activities_score.to_f
      }
    }

    response = Communicator::Binary::HTTP.send_online_activities_score(data_to_send)
    response
  end

end
