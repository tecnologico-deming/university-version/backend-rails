# == Schema Information
#
# Table name: admin_academic_student_exam_regular_details
#
#  id          :bigint           not null, primary key
#  exam_id     :integer
#  question_id :integer
#  answer_id   :integer
#  is_correct  :boolean
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#
class Admin::Academic::Student::Exam::RegularDetail < ApplicationRecord

  belongs_to :exam, class_name: 'Admin::Academic::Student::Exam::Regular', foreign_key: :exam_id
  belongs_to :question, class_name: 'Admin::Academic::Subject::ExamQuestion', foreign_key: :question_id
  belongs_to :answer, class_name: 'Admin::Academic::Subject::ExamQuestionAnswer', foreign_key: :answer_id

end
