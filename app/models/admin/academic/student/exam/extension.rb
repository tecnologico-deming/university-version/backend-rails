# == Schema Information
#
# Table name: admin_academic_student_exam_extensions
#
#  id            :bigint           not null, primary key
#  created_by_id :integer
#  period_id     :integer
#  subject_id    :integer
#  student_id    :integer
#  start_date    :datetime
#  end_date      :datetime
#  dead_line     :datetime
#  score         :float
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#
class Admin::Academic::Student::Exam::Extension < ApplicationRecord

  belongs_to :student, class_name: 'Admin::User::User', foreign_key: :student_id
  belongs_to :subject, class_name: 'Admin::Academic::Subject::Subject', foreign_key: :subject_id

  has_many :details, class_name: 'Admin::Academic::Student::Exam::ExtensionDetail', dependent: :destroy, foreign_key: :exam_id

  def send_to_student_score
    row = Admin::Academic::Student::Score::Score.find_by(subject_id: subject_id, period_id: period_id, student_id: student_id)
    row&.update(exam_score: score)
  end

  def send_score_to_binary
    data_to_send = {
      data: {
        id_materia: subject.id,
        cedula: student.identification_number,
        calificacion: score
      }
    }

    response = Communicator::Binary::HTTP.send_extension_exam_score(
      data_to_send
    )

    response
  end

end
