# == Schema Information
#
# Table name: admin_academic_student_exam_extension_details
#
#  id          :bigint           not null, primary key
#  exam_id     :integer
#  question_id :integer
#  answer_id   :integer
#  is_correct  :boolean
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#
class Admin::Academic::Student::Exam::ExtensionDetail < ApplicationRecord

  belongs_to :exam, class_name: 'Admin::Academic::Student::Exam::Extension', foreign_key: :exam_id, optional: true

end
