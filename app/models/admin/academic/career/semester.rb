# == Schema Information
#
# Table name: admin_academic_career_semesters
#
#  id            :bigint           not null, primary key
#  created_by_id :integer
#  career_id     :integer
#  order         :integer
#  name          :string
#  hours         :float
#  credits       :integer
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#
class Admin::Academic::Career::Semester < ApplicationRecord

  validates :created_by_id, presence: true
  validates :career_id, presence: true
  validates :name, presence: true, uniqueness: { scope: :career_id, message: 'Ya existe un semestre con ese nombre' }
  validates :hours, presence: true
  validates :credits, presence: true

  # has_many :subjects, class_name: 'Admin::Academic::Subject::Subject'

  has_many :subject_semesters, class_name: 'Admin::Academic::Subject::SubjectSemester', dependent: :destroy
  has_many :subjects, class_name: 'Admin::Academic::Subject::Subject', through: :subject_semesters

  belongs_to :career, class_name: 'Admin::Academic::Career::Career', foreign_key: :career_id, optional: true
  belongs_to :created_by, class_name: 'Admin::User::User', foreign_key: :created_by_id

  def set_order(career, semester)
    last_semester = Admin::Academic::Career::Semester.where(career_id: career.id).last
    if last_semester.present?
      semester.update(order: last_semester.order + 1)
    else
      semester.update(order: 1)
    end
  end

end
