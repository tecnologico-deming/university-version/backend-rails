# == Schema Information
#
# Table name: admin_academic_career_careers
#
#  id            :bigint           not null, primary key
#  created_by_id :integer
#  period_id     :integer
#  is_active     :boolean
#  name          :string
#  code          :string
#  credits       :string
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#
class Admin::Academic::Career::Career < ApplicationRecord

  validates :name, uniqueness: true, presence: true
  validates :code, uniqueness: true, presence: true
  validates :credits, presence: true

  belongs_to :created_by, class_name: 'Admin::User::User', foreign_key: :created_by_id
  belongs_to :period, class_name: 'Admin::Academic::Period::Period', foreign_key: :period_id

  has_many :semesters, class_name: 'Admin::Academic::Career::Semester', foreign_key: :career_id, dependent: :destroy

  def semester_order_and_user(user)
    semesters = Admin::Academic::Career::Semester.where(career_id: id)
    semesters.each_with_index do |semester, idx|
      semester.update(created_by_id: user.id, order: idx + 1)
    end
  end

end
