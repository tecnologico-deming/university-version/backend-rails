# == Schema Information
#
# Table name: admin_academic_period_periods
#
#  id            :bigint           not null, primary key
#  created_by_id :integer
#  is_active     :boolean
#  name          :string
#  start_date    :date
#  end_date      :date
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#
class Admin::Academic::Period::Period < ApplicationRecord

  validates :created_by_id, presence: true
  validates :name, presence: true, uniqueness: true
  validates :start_date, uniqueness: true, presence: true
  validates :end_date, uniqueness: true, presence: true

  # Academic relations
  has_many :careers, class_name: 'Admin::Academic::Career::Career'
  has_many :guides, class_name: 'Admin::Academic::Subject::Guide'
  has_many :exams, class_name: 'Admin::Academic::Subject::Exam'

  # Activation relations
  has_many :activations, class_name: 'Admin::Academic::Binary::Activation'

  # Library relations
  has_many :books, class_name: 'Admin::Library::Book::Book'

  # User relations
  belongs_to :created_by, class_name: 'Admin::User::User', foreign_key: :created_by_id

end
