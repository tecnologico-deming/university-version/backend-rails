# == Schema Information
#
# Table name: admin_academic_group_groups
#
#  id            :bigint           not null, primary key
#  created_by_id :integer
#  period_id     :integer
#  city_id       :integer
#  name          :string
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#
class Admin::Academic::Group::Group < ApplicationRecord

  pg_search_scope :search_groups,
                  against: %i[name],
                  using: { tsearch: { prefix: true } }

  belongs_to :city, class_name: 'Admin::Config::City', foreign_key: :city_id
  belongs_to :created_by, :class_name => 'Admin::User::User', foreign_key: :created_by_id
  belongs_to :period, class_name: 'Admin::Academic::Period::Period', foreign_key: :period_id

  # Academic relations
  has_many :user_groups, class_name: 'Admin::Academic::UserGroup', dependent: :destroy
  has_many :users, class_name: 'Admin::User::User', through: :user_groups

  # Classroom relations
  has_many :classrooms, class_name: 'Admin::Academic::Class::Class'

  # Student relations
  has_many :student_subjects, class_name: 'Admin::Academic::Student::Subject::Class'

  # Teacher relations
  has_many :teacher_subjects, class_name: 'Admin::Academic::Teacher::Subject::Class'

end
