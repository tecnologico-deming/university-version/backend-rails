# == Schema Information
#
# Table name: admin_academic_headquarter_headquarters
#
#  id            :bigint           not null, primary key
#  created_by_id :integer
#  is_active     :boolean
#  name          :string
#  address       :string
#  lat           :float
#  long          :float
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#
class Admin::Academic::Headquarter::Headquarter < ApplicationRecord

  validates :name, uniqueness: true, presence: true

  # Library relations
  has_many :books, :class_name => 'Admin::Library::Book::Book'

end
