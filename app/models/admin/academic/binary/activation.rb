# == Schema Information
#
# Table name: admin_academic_binary_activations
#
#  id                            :bigint           not null, primary key
#  period_id                     :integer
#  subject_id                    :integer
#  studymode_id                  :integer
#  group_name                    :string
#  student_identification_number :string
#  teacher_identification_number :string
#  student_exam_date             :string
#  student_extension_exam_date   :string
#  student_due_date              :string
#  teacher_due_date              :string
#  created_at                    :datetime         not null
#  updated_at                    :datetime         not null
#
class Admin::Academic::Binary::Activation < ApplicationRecord

  validates_uniqueness_of :student_identification_number,
                          scope: %i[period_id subject_id],
                          message: 'Subject is already activated', on: %i[create]

  belongs_to :period, class_name: 'Admin::Academic::Period::Period', foreign_key: :period_id
  belongs_to :subject, class_name: 'Admin::Academic::Subject::Subject', foreign_key: :subject_id

  has_many :dates, class_name: 'Admin::Academic::Binary::ActivationDate', dependent: :destroy
  accepts_nested_attributes_for :dates, allow_destroy: true

  has_many :student_exams, class_name: 'Admin::Academic::Student::Exam::Regular', dependent: :destroy
  has_many :student_scores, class_name: 'Admin::Academic::Student::Score::Score', dependent: :destroy
  has_many :student_subjects, class_name: 'Admin::Academic::Student::Subject::Class', dependent: :destroy
  has_many :student_teacher_surveys, class_name: 'Admin::Academic::Student::Survey::TeacherResponse', dependent: :destroy

  has_many :teacher_subjects, class_name: 'Admin::Academic::Teacher::Subject::Class'
  has_many :teacher_auto_surveys, class_name: 'Admin::Academic::Teacher::Survey::AutoResponse'

  has_many :principal_teacher_surveys, class_name: 'Admin::Academic::Principal::Survey::TeacherResponse'

  def group_classes(current_period, activation)
    group = Admin::Academic::Group::Group.find_by(name: group_name)
    teacher = Admin::User::User.find_by(identification_number: teacher_identification_number)
    classes = Admin::Academic::Class::Class
              .find_or_create_by(period_id: current_period.id, group_id: group.id, subject_id: subject_id)
    classes.update(period_id: current_period.id, group_id: group.id, subject_id: subject_id, teacher_id: teacher.id)
    classes.dates.delete_all
    activation.dates.each do |date|
      dates = Admin::Academic::Class::ClassDate.find_or_create_by(class_id: classes.id, date: date.date)
      dates.update(room_code: SecureRandom.hex(6))
    end
  end

  # Student activations

  def student_activations(current_period)
    group = Admin::Academic::Group::Group.find_by(name: group_name)
    teacher = Admin::User::User.find_by(identification_number: teacher_identification_number)
    student = Admin::User::User.find_by(identification_number: student_identification_number)

    subject = Admin::Academic::Student::Subject::Class.find_or_create_by(activation_id: id)
    subject.update(activation_id: id, period_id: current_period.id, subject_id: subject_id,
                   group_id: group.id, teacher_id: teacher.id, student_id: student.id,
                   due_date: student_due_date, exam_date: student_exam_date,
                   extension_exam_date: student_extension_exam_date)

    student_exams(current_period)
    student_scores(current_period)
    student_survey(current_period)
  end

  def student_exams(current_period)
    group = Admin::Academic::Group::Group.find_by(name: group_name)
    teacher = Admin::User::User.find_by(identification_number: teacher_identification_number)
    student = Admin::User::User.find_by(identification_number: student_identification_number)

    exam = Admin::Academic::Student::Exam::Regular.find_or_create_by(activation_id: id)
    exam.update(activation_id: id, period_id: current_period.id, group_id: group.id, subject_id: subject_id,
                teacher_id: teacher.id, student_id: student.id, teacher_survey_complete: false,
                start_date: student_exam_date)
  end

  def student_scores(current_period)
    teacher = Admin::User::User.find_by(identification_number: teacher_identification_number)
    student = Admin::User::User.find_by(identification_number: student_identification_number)

    score = Admin::Academic::Student::Score::Score.find_or_create_by(activation_id: id)
    score.update(activation_id: id, period_id: current_period.id, subject_id: subject_id,
                 teacher_id: teacher.id, student_id: student.id)
  end

  def student_survey(current_period)
    group = Admin::Academic::Group::Group.find_by(name: group_name)
    teacher = Admin::User::User.find_by(identification_number: teacher_identification_number)
    student = Admin::User::User.find_by(identification_number: student_identification_number)
    survey = Admin::Survey::Teacher::Survey.find_by(name: 'Estudiante')
    response = Admin::Academic::Student::Survey::TeacherResponse.find_or_create_by(activation_id: id)

    response.update(period_id: current_period.id, activation_id: id, survey_id: survey.id,
                    subject_id: subject_id, group_id: group.id, teacher_id: teacher.id,
                    student_id: student.id, start_date: student_exam_date)
  end

  # Teacher activations

  def teacher_activations(current_period)
    group = Admin::Academic::Group::Group.find_by(name: group_name)
    teacher = Admin::User::User.find_by(identification_number: teacher_identification_number)

    subject = Admin::Academic::Teacher::Subject::Class.find_or_create_by(
      period_id: current_period.id, group_id: group.id, subject_id: subject_id
    )

    subject.update(activation_id: id, period_id: current_period.id, subject_id: subject_id,
                   group_id: group.id, teacher_id: teacher.id, due_date: teacher_due_date,
                   exam_date: student_exam_date)

    teacher_survey(current_period)
  end

  def teacher_survey(current_period)
    group = Admin::Academic::Group::Group.find_by(name: group_name)
    teacher = Admin::User::User.find_by(identification_number: teacher_identification_number)
    survey = Admin::Survey::Teacher::Survey.find_by(name: 'Auto evaluacion')

    response = Admin::Academic::Teacher::Survey::AutoResponse.find_or_create_by(
      period_id: current_period.id, group_id: group.id, subject_id: subject_id
    )
    response.update(
      activation_id: id,
      period_id: current_period.id,
      survey_id: survey.id,
      subject_id: subject_id,
      group_id: group.id,
      teacher_id: teacher.id,
      start_date: student_exam_date
    )
  end

  def remove_teacher_related_data
    group = Admin::Academic::Group::Group.find_by(name: group_name)
    if only_student
      Admin::Academic::Teacher::Subject::Class.find_by(group_id: group.id, subject_id: subject_id, period_id: period_id).destroy
      Admin::Academic::Teacher::Survey::AutoResponse.find_by(group_id: group.id, subject_id: subject_id, period_id: period_id).destroy
    end
  end

  def only_student
    Admin::Academic::Binary::Activation.where(group_name: group_name, subject_id: subject_id, period_id: period_id).count < 1
  end

  # Principal activations

  def principal_surveys(current_period)
    teacher = Admin::User::User.find_by(identification_number: teacher_identification_number)
    survey = Admin::Survey::Teacher::Survey.find_by(name: 'Autoridad')

    response = Admin::Academic::Principal::Survey::TeacherResponse.find_or_create_by(activation_id: id)
    response.update(
      period_id: current_period.id,
      activation_id: id,
      survey_id: survey.id,
      teacher_id: teacher.id
    )
  end

end
