# == Schema Information
#
# Table name: admin_academic_binary_activation_dates
#
#  id            :bigint           not null, primary key
#  activation_id :integer
#  date          :datetime
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#
class Admin::Academic::Binary::ActivationDate < ApplicationRecord

  belongs_to :activation, class_name: 'Admin::Academic::Binary::Activation', foreign_key: :activation_id, optional: true

end
