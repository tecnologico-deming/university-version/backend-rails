# == Schema Information
#
# Table name: admin_academic_teacher_survey_auto_responses
#
#  id            :bigint           not null, primary key
#  activation_id :integer
#  period_id     :integer
#  survey_id     :integer
#  subject_id    :integer
#  group_id      :integer
#  teacher_id    :integer
#  start_date    :date
#  fill_date     :datetime
#  score         :float
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#
class Admin::Academic::Teacher::Survey::AutoResponse < ApplicationRecord
end
