# == Schema Information
#
# Table name: admin_academic_teacher_subject_classes
#
#  id            :bigint           not null, primary key
#  activation_id :integer
#  period_id     :integer
#  subject_id    :integer
#  group_id      :integer
#  teacher_id    :integer
#  due_date      :date
#  exam_date     :date
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#
class Admin::Academic::Teacher::Subject::Class < ApplicationRecord

  belongs_to :group, class_name: 'Admin::Academic::Group::Group', foreign_key: :group_id
  belongs_to :subject, class_name: 'Admin::Academic::Subject::Subject', foreign_key: :subject_id
  belongs_to :teacher, class_name: 'Admin::User::User', foreign_key: :teacher_id

end
