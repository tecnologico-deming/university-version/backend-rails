# == Schema Information
#
# Table name: admin_academic_teacher_subject_classworks
#
#  id         :bigint           not null, primary key
#  period_id  :integer
#  teacher_id :integer
#  group_id   :integer
#  subject_id :integer
#  title      :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
class Admin::Academic::Teacher::Subject::Classwork < ApplicationRecord

  has_one_attached :file

  has_rich_text :description

  belongs_to :period, class_name: 'Admin::Academic::Period::Period', foreign_key: :period_id
  belongs_to :teacher, class_name: 'Admin::User::User', foreign_key: :teacher_id
  belongs_to :group, class_name: 'Admin::Academic::Group::Group', foreign_key: :group_id
  belongs_to :subject, class_name: 'Admin::Academic::Subject::Subject', foreign_key: :subject_id

  has_many :student_uploads, class_name: 'Admin::Academic::Student::Subject::TeacherClassworkUpload', dependent: :destroy

end
