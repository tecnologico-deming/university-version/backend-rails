# == Schema Information
#
# Table name: admin_academic_teacher_subject_class_files
#
#  id         :bigint           not null, primary key
#  period_id  :integer
#  group_id   :integer
#  subject_id :integer
#  teacher_id :integer
#  title      :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
class Admin::Academic::Teacher::Subject::ClassFile < ApplicationRecord

  has_one_attached :file

  has_rich_text :description

end
