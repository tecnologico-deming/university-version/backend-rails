# == Schema Information
#
# Table name: admin_academic_class_classes
#
#  id            :bigint           not null, primary key
#  activation_id :integer
#  period_id     :integer
#  group_id      :integer
#  subject_id    :integer
#  teacher_id    :integer
#  date          :datetime
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#  room_code     :string
#  server_id     :integer
#
class Admin::Academic::Class::Class < ApplicationRecord

  validates :group_id, presence: true
  validates :subject_id, presence: true
  validates :teacher_id, presence: true
  # validates :date, presence: true

  belongs_to :group, class_name: 'Admin::Academic::Group::Group', foreign_key: :group_id
  belongs_to :teacher, class_name: 'Admin::User::User', foreign_key: :teacher_id
  belongs_to :subject, class_name: 'Admin::Academic::Subject::Subject', foreign_key: :subject_id

  has_many :dates, class_name: 'Admin::Academic::Class::ClassDate', dependent: :destroy
  accepts_nested_attributes_for :dates, allow_destroy: true

  def dates_row(room, current_user)
    room.dates.each do |date|
      date.update(room_code: SecureRandom.hex(6), created_by_id: current_user.id)
    end
  end

end
