# == Schema Information
#
# Table name: admin_academic_class_class_dates
#
#  id            :bigint           not null, primary key
#  created_by_id :integer
#  class_id      :integer
#  server_id     :integer
#  room_code     :string
#  date          :date
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#
class Admin::Academic::Class::ClassDate < ApplicationRecord

  # belongs_to :class, class_name: 'Admin::Academic::Class::Class', foreign_key: :class_id, optional: true
  belongs_to :server, class_name: 'Admin::Server::Server', optional: true, foreign_key: :server_id

end
