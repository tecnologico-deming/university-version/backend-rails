import { Controller } from "stimulus";

export default class extends Controller {

  static targets = ["dropdown", "field_div"]

  connect() {
    const dropdown = document.getElementById('content-dropdown')
    dropdown.addEventListener('change', (event) => {
      if (event.target.value === "2" || event.target.value === "4") {
        $('.content-type-div').show();
        $('.content-file-field').attr('required', true);
      } else {
        $('.content-type-div').hide();
        $('.content-file-field').removeAttr('required')
      }
    })
  }

}
