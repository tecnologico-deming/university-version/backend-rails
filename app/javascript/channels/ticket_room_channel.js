import consumer from "./consumer"

consumer.subscriptions.create("TicketRoomChannel", {
  connected() {
    // Called when the subscription is ready for use on the server
  },

  disconnected() {
    // Called when the subscription has been terminated by the server
  },

  received(data) {
    let comment = $('.ticket-detail-comment');
    const input = document.getElementsByClassName('ticket-message-input');
    const audio = new Audio('/ticket-alert.mp3')

    if (comment.data('value') === data.ticket_id) {
      comment.prepend(data.message);
      $('.ticket-message-form')[0].reset()
    }

    if (data.sender_id !== data.current_user_id) {
      audio.play()
    }


  }
});
