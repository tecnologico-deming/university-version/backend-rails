student_regular_exam = function () {
  const
    second = 1000,
    minute = second * 60,
    hour = minute * 60;

  let limit_date = document.getElementById('limitDate').innerText;
  let start_date = document.getElementById('startDate').innerText;

  let parsed_limit_date = new Date(Date.parse(limit_date));
  let parsed_start_date = new Date(Date.parse(start_date));

  // We read current time each second and display remainder
  x = setInterval(function() {

    let now = new Date().getTime(),
      distance = parsed_limit_date - now;

    document.getElementById('minutes').innerText = Math.floor((distance % (hour)) / (minute));
    document.getElementById('seconds').innerText = Math.floor((distance % (minute)) / second);

  }, second);

  let currentTime = new Date().getTime(),
    time_remaining = parsed_limit_date - currentTime;
  // Calculate 300 seconds before ending time to show an alert.

  // We calculate the remaining time and set a limit with setTimeout()
  // setTimeout(function(){
  //     $( "#examForm" ).submit();
  // }, time_remaining);
}
