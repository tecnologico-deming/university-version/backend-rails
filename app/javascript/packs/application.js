// This file is automatically compiled by Webpack, along with any other files
// present in this directory. You're encouraged to place your actual application logic in
// a relevant structure within app/javascript and only use these pack files to reference
// that code so it'll be compiled.

require("@rails/ujs").start()
require("turbolinks").start()
require("@rails/activestorage").start()
require("channels")
require("trix")
require("@rails/actiontext")
require("semantic-ui-sass")


import "controllers"
import 'sweetalert'
import './student_exams'

$(document).on('turbolinks:load', function(){
  $('.ui.dropdown').dropdown();
  // $('.dropdown').dropdown({action: 'hide'});
  $('.searchDropdown').dropdown({showOnFocus: false, fullTextSearch: true, clearable: true, placeholder: 'Seleccione una opción', minCharacters: 3});
  $('.ui.accordion').accordion();
  $('.tabular.menu .item').tab();
  // $('.ui.modal').modal('show');
  $('.ui.modal').modal('show').modal({closable: false});
  $('.large.modal').modal('show');
  $('.fullscreen.modal').modal({closable: false}).modal('show');
  $('.message .close').on('click', function() {$(this).closest('.message').transition('fade');});
  $('.menu .item').tab();
});


// Uncomment to copy all static images under ../images to the output folder and reference
// them with the image_pack_tag helper in views (e.g <%= image_pack_tag 'rails.png' %>)
// or the `imagePath` JavaScript helper below.
//
// const images = require.context('../images', true)
// const imagePath = (name) => images(name, true)
