import swal from 'sweetalert2'
import Rails from '@rails/ujs'

Rails.confirm = function (message, element) {

  const alert = swal.mixin({
    customClass: {
      confirmButton: 'ui green button',
      cancelButton: 'ui red button'
    },
    buttonsStyling: false
  })

  alert.fire({
    icon: 'warning',
    html: message,
    showCancelButton: true,
    confirmButtonText: 'Aceptar',
    cancelButtonText: 'Cancelar',
    reverseButtons: true
  }).then((result) => {
    if (result.value) {
      element.removeAttribute('data-confirm')
      element.click()
    }
  })


}
