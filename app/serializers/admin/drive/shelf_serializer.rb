# == Schema Information
#
# Table name: admin_drive_shelves
#
#  id            :bigint           not null, primary key
#  created_by_id :integer
#  period_id     :integer
#  department_id :integer
#  is_active     :boolean
#  name          :string
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#
class Admin::Drive::ShelfSerializer
  include FastJsonapi::ObjectSerializer
  attributes :id, :is_active, :name

  attribute :rows do |object|
    if object.rows.present?
      Admin::Drive::RowSerializer.new(object.rows)
    else
      nil
    end
  end
end
