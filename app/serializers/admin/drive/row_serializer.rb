# == Schema Information
#
# Table name: admin_drive_rows
#
#  id         :bigint           not null, primary key
#  shelf_id   :integer
#  name       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
class Admin::Drive::RowSerializer
  include FastJsonapi::ObjectSerializer
  attributes :id, :name

  attribute :has_items do |object|
    if object.users.present?
      true
    elsif object.books.present?
      true
    else
      false
    end
  end


  attribute :count do |object|
    if object.users.present?
      object.users.count
    elsif object.books.present?
      object.books.count
    else
      0
    end
  end

end
