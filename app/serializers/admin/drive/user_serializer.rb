class Admin::Drive::UserSerializer
  include FastJsonapi::ObjectSerializer
  attributes :id, :identification_number, :email, :mobile_number, :house_number

  attribute :status do |object|
    object.status.name.humanize
  end

  attribute :role do |object|
    object.role.name.titleize
  end

  attribute :gender do |object|
    object.gender.name.humanize
  end

  attribute :full_name do |object|
    object.full_name.squish!
  end

end
