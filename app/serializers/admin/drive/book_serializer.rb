class Admin::Drive::BookSerializer
  include FastJsonapi::ObjectSerializer
  attributes :id, :is_active, :title, :subtitle,:code_dewey, :code_control, :pages, :height

  attribute :campus do |object|
    object.headquarter.name.titleize
  end

  attribute :editorial do |object|
    object.editorial.name.titleize
  end

  attribute :language do |object|
    object.language.name.titleize
  end

  attribute :main_author do |object|
    object.main_author.full_name.titleize.squish!
  end

  attribute :secondary_author do |object|
    if object.secondary_author.present?
      object.secondary_author.full_name.titleize.squish!
    else
      nil
    end
  end

  attribute :publish_type do |object|
    object.publish_type.name.titleize
  end

  attribute :row do |object|
    "#{object.row.name.titleize} - #{object.row.shelf.name.titleize}"
  end

  attribute :type do |object|
    object.type.name.titleize
  end

  attribute :publish_date do |object|
    object.publish_date.year
  end

  attribute :description do |object|
    object.description.body
  end

  attribute :resume do |object|
    object.resume.body
  end

  attribute :image do |object|
    object.image.service_url if object.image.attached?
  end

end
