# == Schema Information
#
# Table name: admin_drive_departments
#
#  id         :bigint           not null, primary key
#  is_active  :boolean
#  name       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
class Admin::Drive::DepartmentSerializer
  include FastJsonapi::ObjectSerializer
  attributes  :id, :is_active

  attribute :name do |object|
    object.name.titleize
  end
end
