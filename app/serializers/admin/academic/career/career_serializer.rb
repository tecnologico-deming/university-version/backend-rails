# == Schema Information
#
# Table name: admin_academic_career_careers
#
#  id            :bigint           not null, primary key
#  created_by_id :integer
#  period_id     :integer
#  is_active     :boolean
#  name          :string
#  code          :string
#  credits       :string
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#
class Admin::Academic::Career::CareerSerializer
  include FastJsonapi::ObjectSerializer
  attributes :id, :name

  attribute :semesters do |object|
    if object.semesters
      Admin::Academic::Career::SemesterSerializer.new(object.semesters)
    else
      nil
    end
  end
end
