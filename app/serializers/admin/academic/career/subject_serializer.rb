class Admin::Academic::Career::SubjectSerializer
  include FastJsonapi::ObjectSerializer
  attributes :id, :name, :code

end
