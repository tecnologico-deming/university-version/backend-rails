class Api::Binary::ActivationDateSerializer
  include FastJsonapi::ObjectSerializer
  attributes :id, :date
end
