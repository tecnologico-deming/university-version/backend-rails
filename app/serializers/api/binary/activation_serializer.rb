class Api::Binary::ActivationSerializer
  include FastJsonapi::ObjectSerializer
  attributes :id, :group_name, :student_identification_number, :teacher_identification_number,
             :student_exam_date, :student_extension_exam_date, :student_due_date, :teacher_due_date

  attribute :subject do |object|
    object.subject.name.titleize
  end

  attribute :period do |object|
    object.period.name.upcase
  end

  attribute :classes do |object|
    Api::Binary::ActivationDateSerializer.new(object.dates)
  end

end
