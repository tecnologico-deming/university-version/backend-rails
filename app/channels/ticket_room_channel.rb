class TicketRoomChannel < ApplicationCable::Channel
  def subscribed
    stream_from 'ticket_room_channel'
  end

  def unsubscribed
    # Any cleanup needed when channel is unsubscribed
  end
end
