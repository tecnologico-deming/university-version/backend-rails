module Student
  module Subject
    module ClassesHelper

      def subject_classes_hour(day)
        hour = '18:00'
        hour = '08:00' if day.date.to_date.saturday? || day.date.to_date.sunday?

        l(day.date, format: "%A, %d/%m/%Y #{hour}")
      end

      def subject_set_top_hour(day)
        top_hour = if day.date.to_date.wday == 6
                     17
                   elsif day.date.to_date.wday == 7
                     12
                   else
                     22
        end
      end

      def subject_class_status(room)
        hour = 17
        hour = 8 if room.date.to_date.saturday? || room.date.to_date.sunday?

        if room.date.to_date == ecuador_time.to_date
          link_to 'Ir a la tutoria',
                  admin_academic_virtual_room_virtual_room_path(id: room),
                  target: :_blank,
                  class: 'ui green medium label'
        end
      end

    end
  end
end
