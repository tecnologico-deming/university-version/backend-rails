module Student
  module Subject
    module ClassworksHelper

      def student_classwork_upload_status(file, subject, group)
        if student_classwork_upload(file).present? && (student_classwork_upload(file).classwork_id == file.id)
          link_to 'Ver archivo subido',
                  edit_student_subject_modules_classworks_path(upload_id: student_classwork_upload(file).id, subject_id: subject.id, group_id: group.id, classwork_id: file.id),
                  class: 'ui medium blue label', remote: true
        else
          student_classwork_date_validation(file, subject, group)
        end
      end

      def student_classwork_date_validation(file, subject, group)
        if file.created_at.to_date == ecuador_time.to_date
          link_to 'Cargar archivo',
                  new_student_subject_modules_classworks_path(subject_id: subject.id, group_id: group.id, classwork_id: file.id),
                  class: 'ui medium yellow label', remote: true
        elsif file.created_at.to_date < ecuador_time.to_date
          raw("<label class='ui red medium label'>Ya se ha pasado la fecha de carga</label>")
        end
      end

      def student_classwork_upload_score(file)
        if student_classwork_upload(file).present? && (student_classwork_upload(file).classwork_id == file.id) && (student_classwork_upload(file).score.present?)
          raw("<label class='ui green medium label'>#{student_classwork_upload(file).score}/ 10</label>")
        elsif student_classwork_upload(file).present? && (student_classwork_upload(file).classwork_id == file.id) && (student_classwork_upload(file).score.blank?)
          raw("<label class='ui yellow medium label'>Por calificar</label>")
        end
      end

      def student_classwork_upload(file)
        Admin::Academic::Student::Subject::TeacherClassworkUpload.where(classwork_id: file.id, student_id: current_user.id).first
      end

    end
  end
end
