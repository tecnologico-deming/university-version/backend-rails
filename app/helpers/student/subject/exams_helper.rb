module Student
  module Subject
    module ExamsHelper

      def student_exam_header(exam)
        render partial: 'student/subject/exams/forms/exam/header', locals: { exam: exam }
      end

      def student_exam_questions(exam)
        render partial: 'student/subject/exams/forms/exam/questions', locals: { exam: exam }
      end

      def student_exam_answers(question, answer)
        if @exam.present?
          Admin::Academic::Student::Exam::RegularDetail.create(exam_id: @exam.id, question_id: question.id, answer_id: answer.id)
        elsif @extension_exam.present?
          Admin::Academic::Student::Exam::ExtensionDetail.create(exam_id: @extension_exam.id, question_id: question.id, answer_id: answer.id)
        end
      end

      def student_exam_form_path
        if @exam.present?
          student_subject_modules_exams_path(exam_id: @exam)
        elsif @extension_exam.present?
          extension_exam_student_subject_modules_exams_path(extension_exam_id: @extension_exam)
        end
      end

      def student_exam_hour(exam)
        hour = '18:00'
        hour = '08:00' if exam.start_date.to_date.saturday? || exam.start_date.to_date.sunday?
        l(exam.start_date, format: "%A, %d/%m/%Y a las #{hour}")
      end

      def student_exam_status(exam)
        hour = 18
        hour = 8 if exam.start_date.to_date.saturday? || exam.start_date.to_date.sunday?
        top_hour = hour + 0.5

        if exam.score.present?
          raw("<label class='ui green medium label'>#{exam.score}/ 10</label>")
        elsif (exam.start_date.to_date == ecuador_time.to_date) && exam.teacher_survey_complete?
          link_to 'Presentar examen',
                  new_student_subject_modules_exams_path(exam_id: exam.id),
                  class: 'ui blue medium label', data: { confirm: 'Una vez seleccionada una respuesta no podra cambiar.' }
        elsif (exam.start_date.to_date == ecuador_time.to_date) && !exam.teacher_survey_complete
          link_to 'Evaluar docente y presentar',
                  teacher_survey_student_subject_modules_exams_path(exam_id: exam.id, subject_id: exam.subject.id),
                  class: 'ui blue medium label'
        elsif (ecuador_time.to_date > exam.start_date.to_date) && exam.score.nil?
          raw('<label class="ui medium red label">No has presentado este examen</label>')
        else
          raw('<label class="ui medium red label">Aun no es la fecha del examen</label>')
        end
      end

      def student_extension_exam_status(exam)
        hour = 18
        hour = 8 if exam.start_date.to_date.saturday? || exam.start_date.to_date.sunday?
        top_hour = hour + 0.5

        if exam.score.present?
          raw("<label class='ui green medium label'>#{exam.score}/ 10</label>")
        elsif exam.start_date.to_date == ecuador_time.to_date
          link_to 'Presentar examen',
                  extension_exam_student_subject_modules_exams_path(extension_exam_id: exam.id),
                  class: 'ui blue medium label'
        elsif (ecuador_time.to_date > exam.start_date.to_date) && exam.score.nil?
          raw('<label class="ui medium red label">No has presentado este examen</label>')
        else
          raw('<label class="ui medium red label">Aun no es la fecha del examen</label>')
        end
      end


    end
  end
end
