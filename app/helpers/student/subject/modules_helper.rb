module Student
  module Subject
    module ModulesHelper

      def subject_content_download_file(content)
        if content.file.present?
          link_to('Descargar', rails_blob_path(content.file, disposition: 'attachment'), download: '', class: 'ui medium grey label')
        end
      end

      def student_uploaded_file_status(subject, group, subject_module, content)
        if student_content_upload(content).present? && (student_content_upload(content).content_id == content.id) && upload_content_type(content)
          link_to 'Ver archivo subido',
                  edit_student_subject_modules_path(upload_id: student_content_upload(content).id, subject_id: subject.id, group_id: group.id, module_id: subject_module.id, content_id: content.id),
                  class: 'ui medium blue label', remote: true
        elsif !student_content_upload(content) && upload_content_type(content)
          link_to 'Cargar archivo',
                  new_student_subject_modules_path(subject_id: subject.id, group_id: group.id, module_id: subject_module.id, content_id: content.id),
                  class: 'ui medium yellow label', remote: true
        end
      end

      def student_online_content(subject, group, subject_module, content)
        if student_online_content_responses(content).present? && (student_online_content_responses(content).content_id == content.id) && online_content_type(content)

        elsif !student_online_content_responses(content) && online_content_type(content)
          link_to 'Completar reto en linea',
                  new_student_subject_modules_online_contents_path(subject_id: subject.id, group_id: group.id, module_id: subject_module.id, content_id: content.id),
                  class: 'ui medium blue label'
        end
      end

      def student_content_score(content)
        if student_online_content_responses(content).present? && (student_online_content_responses(content).content_id == content.id) && online_content_type(content)
          raw("<label class='ui green medium label'>#{student_online_content_responses(content).score}/ 10</label>")
        elsif student_content_upload(content).present? && (student_content_upload(content).content_id == content.id) && upload_content_type(content)
          student_content_upload(content).score.present? ?
            raw("<label class='ui green medium label'>#{student_content_upload(content).score}/ 10</label>") :
              raw("<label class='ui yellow medium label'>Por calificar</label>")
        end
      end

      # Queries

      def student_content_upload(content)
        Admin::Academic::Student::Subject::ContentUpload.where(
          period_id: current_period.id,
          student_id: current_user.id,
          content_id: content.id
        ).first
      end

      def student_online_content_responses(content)
        Admin::Academic::Student::Subject::ContentResponse.where(
          period_id: current_period.id,
          student_id: current_user.id,
          content_id: content.id
        ).first
      end

      def upload_content_type(content)
        content.type.name == 'Proyecto' || content.type.name == 'Reto'
      end

      def online_content_type(content)
        content.type.name == 'Reto en linea'
      end

    end
  end
end
