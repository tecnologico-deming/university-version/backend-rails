module Admin
  module Academic
    module Classes
      module VirtualRoomsHelper

        def virtual_room_subject(group)
          subject_id = group.classrooms.pluck(:subject_id).uniq
          subject = Admin::Academic::Subject::Subject.where(id: subject_id).first.name.titleize
        end

        def virtual_room_teacher(group)
          teacher_id = group.classrooms.pluck(:teacher_id).uniq
          teacher = Admin::User::User.where(id: teacher_id).first.full_name.titleize
        end

      end
    end
  end
end
