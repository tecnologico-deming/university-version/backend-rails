module Admin
  module Academic
    module Groups
      module StudentsHelper

        def admin?
          current_user.role.name == 'Administrador'
        end

        def assistant?
          current_user.role.name == 'Asistente'
        end

        def group_students_actions(user)
          if admin? && assistant?
            render 'admin/academic/group/students/partials/assign_extension_exam', user: user
          end
        end

        def group_student_time_extension(user)
          if admin?
            render 'admin/academic/group/students/partials/subject_extension', user: user
          end
        end

        def group_student_reschedule_exam(user)
          exam = Admin::Academic::Student::Exam::Regular.find_by(
            period_id: current_period.id, student_id: user.student_id, subject_id: @subject.id
          )

          if exam.present?
            if (admin? || assistant?) && exam.score.blank?
              render 'admin/academic/group/students/partials/reschedule_regular_exam', user: user
            else
              raw("<div class='item'
                          data-tooltip='Calificación #{exam.score.present? ? exam.score : ''} / 10'
                          data-inverted=''>
                          <i class='tasks icon'></i>Ya presento este examen.
                          </div>")
            end
          end

        end

        def group_student_ext_exam(user)
          if admin? || assistant?
            render 'admin/academic/group/students/partials/assign_extension_exam', user: user
          end
        end



      end
    end
  end
end
