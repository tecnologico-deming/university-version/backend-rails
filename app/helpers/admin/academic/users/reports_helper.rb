module Admin
  module Academic
    module Users
      module ReportsHelper

        def print_student_exam_questions(exam)
          question_ids = exam.details.order(id: :asc).pluck(:question_id).uniq
          Admin::Academic::Subject::ExamQuestion.where(id: question_ids)
        end

        def student_exam_group(exam)
          Admin::Academic::Binary::Activation.find_by(
            student_identification_number: exam.student.identification_number,
            subject_id: exam.subject_id,
            period_id: current_period.id
          )
        end

        def student_selected_answers(exam, answer)
          selected_answers = Admin::Academic::Student::Exam::RegularDetail.where(exam_id: exam.id).where.not(is_correct: nil)
          # binding.pry
          selected_answers_ids = selected_answers.pluck(:answer_id).uniq

          if selected_answers_ids.include? answer.id
            raw("<li><strong style='color: blue !important;'>#{answer.title}</strong></li>")
          else
            raw("<li>#{answer.title}</li>")
          end
        end

        def student_correct_answers(exam, question)
          correct_questions = Admin::Academic::Student::Exam::RegularDetail.where(exam_id: exam.id, is_correct: true)
          correct_question_ids = correct_questions.pluck(:question_id).uniq

          if correct_question_ids.include? question.id
            'Status: Correcto'
          elsif !correct_question_ids.include? question.id
            'Status: Incorrecto'
          else
            'Status: No contesto'
          end
        end

        def student_subject_dates(subject)
          Admin::Academic::Student::Subject::Class.find_by(
            period_id: current_period.id,
            student_id: subject.student_id,
            subject_id: subject.subject_id
          )
        end

        def student_subject_due_date(subject)
          if student_subject_dates(subject).present?
            l(student_subject_dates(subject).due_date, format: '%A, %d/%m/%Y')
          end
        end

        def student_subject_exam_date(subject)
          if student_subject_dates(subject).present?
            l(student_subject_dates(subject).exam_date, format: '%A, %d/%m/%Y')
          end
        end

        def student_subject_ext_exam_date(subject)
          if student_subject_dates(subject).present?
            l(student_subject_dates(subject).extension_exam_date, format: '%A, %d/%m/%Y')
          end
        end

        def student_content_show(content)
          if content.type.name == 'Reto' || content.type.name == 'Proyecto'
            render 'admin/user/reports/student/uploads', content: content
          elsif content.type.name == 'Reto en linea'
            render 'admin/user/reports/student/responses', content: content
          end
        end

        def student_report_correct_answers(exam, question)
          correct_questions = Admin::Academic::Student::Exam::RegularDetail.where(exam_id: exam.id, is_correct: true)
          correct_question_ids = correct_questions.pluck(:question_id).uniq

          if correct_question_ids.include? question.id
            raw('<i class="green large check circle outline icon"></i>')
          elsif !correct_question_ids.include? question.id
            raw('<i class="red large times circle outline icon"></i>')
          else
            'No contesto'
          end
        end

        def student_report_correct_answers_count(exam)
          Admin::Academic::Student::Exam::RegularDetail.where(exam_id: exam.id, is_correct: true).count
        end

        def student_report_wrong_answers_count(exam)
          Admin::Academic::Student::Exam::RegularDetail.where(exam_id: exam.id, is_correct: false).count
        end

      end
    end
  end
end


