module Admin
  module Academic
    module Users
      module Prints
        module ExtensionExamsHelper

          def extension_exam_correct_answers(exam, question)
            correct_questions = Admin::Academic::Student::Exam::ExtensionDetail.where(exam_id: exam.id, is_correct: true)
            correct_question_ids = correct_questions.pluck(:question_id).uniq

            if correct_question_ids.include? question.id
              'Status: Correcto'
            elsif !correct_question_ids.include? question.id
              'Status: Incorrecto'
            else
              'Status: No contesto'
            end
          end

          def extension_exam_selected_answers(exam, answer)
            selected_answers = Admin::Academic::Student::Exam::ExtensionDetail.where(exam_id: exam.id).where.not(is_correct: nil)
            # binding.pry
            selected_answers_ids = selected_answers.pluck(:answer_id).uniq

            if selected_answers_ids.include? answer.id
              raw("<li><strong style='color: blue !important;'>#{answer.title}</strong></li>")
            else
              raw("<li>#{answer.title}</li>")
            end
          end

        end
      end
    end
  end
end
