module Admin
  module Academic
    module Users
      module UsersHelper

        def user_status_class(user)
          if user.status.name == 'Activo'
            'green'
          end
        end

        def user_role_table
          if user_all_actions || current_user.role.name == 'Asistente'
            render partial: 'admin/user/users/partials/table'
          elsif current_user.role.name == 'Coordinador'
            render partial: 'admin/user/users/partials/coordinator_table'
          elsif current_user.role.name == 'Invitado'
            render partial: 'admin/user/users/partials/coordinator_table'
          end
        end

        def user_status_name(user)
          if user.status.name == 'Activo'
            'Activo'
          end
        end

        def user_all_actions
          current_user.role.name == 'Administrador'
        end

        def can_sign_in?
          current_user.role.name == 'Asistente' || user_all_actions
        end

        def authorized_options(user)
          if current_user.role.name == 'Asistente' || current_user.role.name == 'Coordinador' || user_all_actions
            render partial: 'admin/user/users/partials/table_actions', locals: { user: user }
          end
        end

      end
    end
  end
end
