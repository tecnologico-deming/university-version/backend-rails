module Admin
  module Academic
    module Subject
      module ExamsHelper

        def correct_answers(question)
          question.answers.where(is_correct: true).present? ? question.answers.where(is_correct: true).first.title : 'Asignar respuesta'
        end

      end
    end
  end
end
