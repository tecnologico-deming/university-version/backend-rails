module Admin
  module Academic
    module Subject
      module ContentsHelper

        def content_type_url(content)
          if content.type.name == 'Reto' || content.type.name == 'Proyecto'
            raw("<strong>Archivo cargado: </strong> #{link_to 'Descargar', rails_blob_path(content.file, disposition: 'attachment'), download: ''}")
          elsif content.type.name == 'Reto en linea'
            link_to 'Ver preguntas', '#'
          end
        end

      end
    end
  end
end

