module Admin
  module Support
    module TicketsHelper

      def role_ticket_tabs
        if current_user.role.name != 'Coordinador'
          render 'admin/support/tickets/partials/index/admin_tabs'
        else
          render 'admin/support/tickets/partials/index/coordinator_tabs'
        end
      end

      def user_ticket_access
        current_user.role.name == 'Administrador' || current_user.role.name == 'Bienestar' || current_user.role.name == 'Asistente'
      end

      def ticket_options_access
        link_to 'Tipos de casos', admin_support_ticket_types_path, class: 'item' if user_ticket_access
      end

      def ticket_data_access(ticket)
        if user_ticket_access
          raw("<small><strong>Creado por:</strong> #{ticket.created_by.full_name.titleize} </small><br>")
        end
      end


      def ticket_assign(ticket)
        if user_ticket_access && ticket.assigned_to.nil?
          link_to('Asignar caso',
                  assign_admin_support_ticket_path(id: ticket.id),
                  class: 'item',
                  remote: true)
        elsif user_ticket_access && (ticket.assigned_to_id == current_user.id)
          link_to('Reasignar caso',
                  assign_admin_support_ticket_path(id: ticket.id),
                  class: 'item',
                  remote: true)
        end
      end

      def close_ticket(ticket)
        if ticket.status == 'En proceso' && user_ticket_access
          link_to('Resolver ticket',
                  complete_admin_support_ticket_path(id: @ticket.id),
                  class: 'item',
                  data: { confirm: '¿Esta seguro de completar este ticket?', method: 'post' })
        end
      end

      def ticket_meetings_helper(ticket)
        if ticket.status != 'Solucionado' && user_ticket_access
          render partial: 'admin/support/tickets/forms/meetings'
        end
      end

      def ticket_status_class(ticket)
        if ticket.status == 'Por procesar'
          'blue'
        elsif ticket.status == 'En proceso'
          'yellow'
        elsif ticket.status == 'Solucionado'
          'green'
        end
      end

    end
  end
end
