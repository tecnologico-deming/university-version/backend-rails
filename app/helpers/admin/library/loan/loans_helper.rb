module Admin
  module Library
    module Loan
      module LoansHelper

        def library_loan_status_class(loan)
          if loan.return_date > ecuador_time.to_date && !loan.was_received
            'blue'
          elsif loan.return_date > ecuador_time.to_date && loan.was_received?
            'green'
          else
            'red'
          end
        end

        def library_loan_status_message(loan)
          if loan.return_date > ecuador_time.to_date && !loan.was_received
            'En regla'
          elsif loan.return_date > ecuador_time.to_date && loan.was_received?
            'Devuelto'
          else
            'Expirado'
          end
        end

      end
    end
  end
end
