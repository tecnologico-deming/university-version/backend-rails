module Admin
  module Library
    module Loan
      module ReservesHelper

        def library_reserve_status_class(reserve)
          if reserve.is_approved.nil?
            'blue'
          elsif reserve.is_approved?
            'green'
          elsif !reserve.is_approved
            'red'
          end
        end

        def library_reserve_status_message(reserve)
          if reserve.is_approved.nil?
            'Por procesar'
          elsif reserve.is_approved?
            'Aprobado'
          elsif !reserve.is_approved
            'Rechazado'
          end
        end

      end
    end
  end
end
