module Teacher
  module SubjectsHelper

    def subject_guide_download(subject)
      guide = subject.guides.find_by(is_active: true)
      if guide.present?
        link_to 'Descargar guia', rails_blob_path(guide.file, disposition: 'attachment'), download: '', class: 'ui medium blue label'
      elsif guide.blank? && current_user.role.name == 'Profesor'
        raw('<label class="ui medium red label"
            data-tooltip="Por favor comunicate con el departamento académico"
            data-position="left center"
            data-variation="mini">
            ¡Estamos trabajando en la guia!
            </label>')
      end
    end

    def teacher_module_action(subject_module)
      if subject_module.contents.first.type.name == 'Reto' || subject_module.contents.first.type.name == 'Proyecto'
        link_to 'Evaluar',
                new_teacher_subject_online_activities_path(content_id: subject_module.contents.first.id, group_id: @group, subject_id: @subject, period_id: current_period.id),
                class: 'ui fluid center aligned blue medium label', remote: true
      else
        'No es necesario evaluar este modulo.'
      end
    end

  end
end
