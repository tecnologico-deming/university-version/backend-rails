module Teacher
  module Subjects
    module ClassworksHelper

      def teacher_subject_classwork_limit(classworks, group, subject)
        if classworks.count < 5 && teacher_classwork_date_validation(group, subject)
          link_to 'Cargar otro taller',
                  new_teacher_subject_classworks_path(group_id: @group.id, subject_id: @subject.id),
                  class: 'ui fluid blue button', remote: true
        elsif !teacher_classwork_date_validation(group, subject)
        else
          link_to 'Ya haz cargado el maximo de talleres', '#', class: 'ui fluid yellow button', remote: true
        end
      end

      def teacher_classwork_date_validation(group, subject)
        classes = Admin::Academic::Class::Class.where(group_id: group.id, subject_id: subject.id, period_id: current_period.id)
        dates = Admin::Academic::Class::ClassDate.where(class_id: classes.pluck(:id))
        dates.pluck(:date).include? ecuador_time.to_date
      end

    end
  end
end
