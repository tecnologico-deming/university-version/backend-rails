module Layout
  module Navbar
    module NavbarHelper

      def nav_role_options
        if current_user.role.name == 'Administrador'
          render partial: 'layouts/navigation/role_options/administrador'
        elsif current_user.role.name == 'Bibliotecario'
          render partial: 'layouts/navigation/role_options/bibliotecario'
        end
      end

      def sidebar_role_options
        if current_user.role.name == 'Administrador'
          render partial: 'layouts/navigation/role_options/sidebar/administrador'
        elsif current_user.role.name == 'Bibliotecario'
          #render partial: 'layouts/navigation/role_options/bibliotecario'
        end
      end

    end
  end
end
