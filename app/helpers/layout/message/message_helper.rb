module Layout
  module Message
    module MessageHelper

      def error_message(object)
        if object.errors.any?
          render partial: 'layouts/message/error_message', locals: { object: object }
        end
      end

    end
  end
end
