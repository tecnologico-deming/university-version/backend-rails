module Layout
  module Common
    module CommonsHelper

      def object_status_name(value)
        if value.is_active?
          'Activo'
        else
          'Inactivo'
        end
      end

      def object_status_class(value)
        if value.is_active?
          'green'
        else
          'red'
        end
      end

    end
  end
end
