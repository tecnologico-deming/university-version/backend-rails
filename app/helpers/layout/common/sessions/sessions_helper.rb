module Layout
  module Common
    module Sessions
      module SessionsHelper

        def sessions_form_params
          if params[:ebsco].present?
            hidden_field_tag :ebsco, true
          elsif params[:online_catalogue].present?
            hidden_field_tag :online_catalogue, true
          end
        end

      end
    end
  end
end
