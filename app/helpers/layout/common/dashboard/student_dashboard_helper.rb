module Layout
  module Common
    module Dashboard
      module StudentDashboardHelper

        def student_dashboard_survey_message
          array = ['1500887193', '1726205931', '1500392772', '1500327281', '1500814312', '1500508419', '1723987283',
                   '1500930084', '1755091921', '1724322845', '1721651162', '0706620374', '1722327093', '1600431090',
                   '1500698202', '0982343497', '2100238415', '1715128672', 'estudiante']

          ids = Admin::User::User.where(identification_number: array).pluck(:id)
          render partial: 'layouts/dashboard/student/survey' if ids.include? current_user.id
        end

        def student_dashboard_subjects(current_period, current_user)
          Admin::Academic::Student::Subject::Class.where(
            period_id: current_period.id, student_id: current_user.id
          ).order(due_date: :desc)
        end

        def student_subject_status(subject)
          start_date = subject.due_date.to_date - 21.days
          extension_date = subject.extensions.order(date: :asc).last if subject.extensions.order(date: :asc).last.present?

          if extension_date.present?
            student_subject_with_extension(subject, extension_date)
          else
            student_subject_no_extension(subject, start_date)
          end
        end

        def student_subject_no_extension(subject, start_date)
          if (ecuador_time.to_date >= start_date) && (ecuador_time.to_date <= subject.due_date.to_date)
            link_to "Ir a la materia",
                    student_subject_modules_path(group_id: subject.group_id, subject_id: subject.subject_id),
                    class: 'ui huge basic green button'
          else
            link_to 'Materia cerrada', '#', class: 'ui huge basic red button'
          end
        end

        def student_subject_with_extension(subject, extension_date)
          if ecuador_time.to_date <= extension_date.date.to_date
            link_to "Ir a la materia",
                    student_subject_modules_path(group_id: subject.group_id, subject_id: subject.subject_id),
                    class: 'ui huge basic green button'
          else
            link_to 'Materia cerrada', '#', class: 'ui huge basic red button'
          end
        end

        def student_subject_extension_label(subject)
          if subject.extensions.order(date: :asc).last.present?
            raw("<div class='ui ui top attached red label'>Materia con prórroga hasta: #{l(subject.extensions.order(date: :asc).last.date, format: '%A, %d/%m/%Y')}</div>")
            # subject.extensions.order(date: :asc).last.date
          end
        end

        def student_subject_extension_date(subject)
          if subject.extensions.order(date: :asc).last.present?
            raw("<strong>Fecha tope de prórroga:</strong> #{l(subject.extensions.order(date: :asc).last.date, format: '%A, %d/%m/%Y')}")
          end
        end

        def student_subject_remaining_time(subject)
          can_view = ecuador_time.to_date >= (subject.due_date - 21.days)
          is_active = (ecuador_time.to_date <= subject.due_date)
          remaining_days = subject.due_date.to_date - ecuador_time.to_date
          if can_view && is_active && remaining_days.positive?
            raw("
              <div class='ui ui top attached red label'>
                Faltan #{subject.due_date.to_date.day - ecuador_time.to_date.day} dia(s) cierre de la materia.
              </div>")
          elsif can_view && is_active && remaining_days.zero?
            limit_hour = subject.due_date.to_datetime + 23.99.hours

            if (limit_hour.hour - ecuador_time.hour).positive? && !student_subject_extension_date(subject)
              raw("
              <div class='ui ui top attached red label'>
                Faltan #{limit_hour.hour - ecuador_time.hour} hora(s) y #{limit_hour.minute - 15} minuto(s)
                para el cierre de la materia, por favor apresurate en realizar todas las actividades.
              </div>")
            end
          end
        end

      end
    end
  end
end
