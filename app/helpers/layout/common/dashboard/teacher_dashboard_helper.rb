module Layout
  module Common
    module Dashboard
      module TeacherDashboardHelper

        def teacher_dashboard_subjects(current_period, current_user)
          Admin::Academic::Teacher::Subject::Class.where(
            period_id: current_period.id, teacher_id: current_user.id
          ).order(due_date: :desc)
        end

        def teacher_subject_status(subject)
          start_date = subject.due_date.to_date - 28.days

          #if ecuador_time.to_date <= subject.due_date.to_date
            link_to "Ir a la materia",
                    teacher_subject_online_activities_path(subject_id: subject.subject_id, group_id: subject.group_id),
                    class: 'ui huge basic green button'
            #else
          #link_to 'Materia cerrada', '#', class: 'ui huge basic red button'
          #end
        end

      end
    end
  end
end
