module Layout
  module Common
    module Library
      module CataloguesHelper

        def catalogue_next_available_book
          reserved = @book.book_copies.where(status: 'Reservado')
          last_reserved = reserved.order(updated_at: :desc).last
        end

        def catalogue_reserve_book_form
          copies = @book.book_copies.where(status: 'Disponible')
          if copies.present?
            render partial: 'common/library/catalogues/forms/reserve', locals: { copy: copies.first.id }
          else
            raw("<label class='ui red fluid button'>No hay libros disponibles</label>")
          end

        end

      end
    end
  end
end

