Rails.application.routes.draw do
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html

  namespace :admin do
    namespace :academic do

      namespace :career do
        resources :careers do
          resources :semesters do
            member do
              get 'subjects'
            end
          end
        end
      end

      namespace :group do
        resources :groups do
          resources :subjects do
            resources :students do
              member do
                get 'assign_extension_exam'
                post 'assign_extension_exam', to: 'students#assign_extension_exam_submit'

                get 'reschedule_exam'
                post 'reschedule_exam', to: 'students#reschedule_exam_submit'
              end
            end
          end
        end

        resources :class_extensions
      end

      namespace :period do
        resources :periods
      end


      namespace :subject do
        resources :subjects do
          resources :classworks
          resources :exams
          resources :exam_questions
          resources :guides

          resources :modules do
            resources :contents
            resources :content_questions
          end
        end
      end

      namespace :virtual_room do
        resources :virtual_rooms
      end

    end
  end

end
