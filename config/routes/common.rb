Rails.application.routes.draw do
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html

  root 'common/sessions#new'

  get 'public_library', to: 'common/public_library#index'
  get 'public_library_detail', to: 'common/public_library#show'


  namespace :common do
    resources :dashboards, only: :index do
      collection do
        get 'subject_dates'

        get 'change_password'
        post 'change_password', to: 'dashboards#change_password_submit'

        get 'update_info'
        post 'update_info', to: 'dashboards#update_info_submit'
      end
    end

    namespace :library do
      resources :catalogues do
        member do
          post 'reserve_book', to: 'catalogues#reserve_book'
        end
      end
    end

    resources :sessions, only: %i[] do
      collection do
        get 'private_login/:custom_token', to: 'sessions#private_login', as: 'private_login'

        get 'login', to: 'sessions#new'
        post 'login', to: 'sessions#create'
        delete 'logout', to: 'sessions#destroy'
      end
    end

  end

end
