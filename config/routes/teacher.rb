Rails.application.routes.draw do

  namespace :teacher do
    namespace :subject do
      resource :assistances
      resources :class_dates
      resource :class_files
      resource :classworks do
        member do
          get 'uploads'
          post 'uploads', to: 'classworks#uploads_submit'
        end
      end
      resource :online_activities
    end
  end

end
