Rails.application.routes.draw do

  namespace :student do
    namespace :subject do
      resource :modules do
        resource :classes, only: %i[show]

        resource :classworks

        resource :exams do
          collection do
            get 'extension_exam'
            post 'extension_exam', to: 'exams#extension_exam_submit'

            get 'results'

            get 'teacher_survey'
            post 'teacher_survey', to: 'exams#teacher_survey_submit'


          end
        end

        resource :online_contents
      end
    end
  end

end
