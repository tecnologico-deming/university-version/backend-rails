Rails.application.routes.draw do

  namespace :api do
    namespace :v2 do
      namespace :admin do
        namespace :academics do

          namespace :careers do
            resources :careers
          end

          namespace :subjects do
            resources :classworks
            resources :contents
            resources :content_questions, only: :create
            resources :content_question_answers, only: :create
            resources :exam_questions
            resources :guides
            resources :modules
            resources :subjects
          end
        end

        namespace :student do
          namespace :subject do
            resources :classwork_uploads
            resources :contents
            resources :content_responses
            resources :exam_scores do
              collection do
                put 'update_exam_scores', to: 'exam_scores#update_exam_scores'
              end
            end
          end
        end

        namespace :ticket do
          resources :tickets
        end

      end

      namespace :binary do
        namespace :activations do
          resources :activations
        end
      end

      namespace :drive do
        resources :departments, only: :index
        resources :shelves, only: :index do
          member do
            get 'scan_qr'
          end
        end
        resources :row_items, only: :index
      end

    end
  end

end
