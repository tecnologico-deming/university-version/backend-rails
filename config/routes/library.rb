Rails.application.routes.draw do

  namespace :admin do
    namespace :library do

      namespace :authors do
        resources :authors do
          collection do
            get 'author_excel_upload'
            post 'author_excel_upload', to: 'authors#author_excel_upload_submit'
          end
        end
      end

      namespace :books do
        resources :books do
          collection do
            get 'book_excel_upload'
            post 'book_excel_upload', to: 'books#book_excel_upload_submit'
          end
        end
      end

      namespace :categories do
        resources :categories do
          collection do
            get 'category_excel_upload'
            post 'category_excel_upload', to: 'categories#category_excel_upload_submit'
          end
        end

        resources :tags do

        end
      end

      namespace :editorials do
        resources :editorials do
          collection do
            get 'editorial_excel_upload'
            post 'editorial_excel_upload', to: 'editorials#editorial_excel_upload_submit'
          end
        end
      end

      namespace :loans do
        resources :loans
        resources :reserves
      end

      namespace :shelves do
        resources :shelves
      end
    end
  end

end
