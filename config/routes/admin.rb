Rails.application.routes.draw do
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html

  mount ActionCable.server, at: '/cable'

  namespace :admin do
    namespace :config do
      namespace :country do
        resources :countries do
          resources :states do
            resources :cities
          end
        end
      end
    end

    namespace :drive do
      resources :shelves do
        collection do
          get 'print_qr'
        end
      end
    end

    namespace :server do
      resources :servers
    end

    namespace :support do
      resources :tickets do
        collection do
          get 'ticket_period_search'
        end

        member do
          get 'assign'
          post 'assign', to: 'tickets#assign_submit'
          post 'complete', to: 'tickets#complete_submit'
          get 'details'
          post 'messages', to: 'tickets#ticket_messages_submit'
        end
      end
      resources :ticket_details
      resources :ticket_meetings
      resources :ticket_types
    end

    namespace :user do
      resources :users do
        member do
          get 'assign_group'
          post 'assign_group', to: 'users#assign_group_submit'

          get 'assign_shelf'
          post 'assign_shelf', to: 'users#assign_shelf_submit'

          get 'send_score_to_binary'
          post 'send_score_to_binary', to: 'users#send_score_to_binary_submit'

          post 'update_info', to: 'users#update_info_submit'
          post 'reset_password', to: 'users#reset_password_submit'
        end

        resources :reports do
          collection do
            get 'student_progress'
            get 'student_subject_progress'
          end
        end

        resources :prints do
          collection do
            get 'student_exam'
            get 'student_extension_exam'
          end
        end
      end
    end
  end

end
