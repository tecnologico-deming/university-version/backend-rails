# Use this file to easily define all of your cron jobs.
#
# It's helpful, but not entirely necessary to understand cron before proceeding.
# http://en.wikipedia.org/wiki/Cron

# Example:

set :output, 'log/whenever.log'
set :environment, 'production'

every :monday, at: '00:00 am' do
  rake 'binary:online_activities_score'
end

every :monday, at: '00:30 am' do
  rake 'binary:class_work_score'
end

# every 2.hours do
#   command "/usr/bin/some_great_command"
#   runner "MyModel.some_method"
#   rake "some:great:rake:task"
# end
#
# every 4.days do
#   runner "AnotherModel.prune_old_records"
# end

# Learn more: http://github.com/javan/whenever
