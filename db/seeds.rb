# Academic
Admin::Academic::Headquarter::Headquarter.create([{created_by_id: 1, is_active: true, name: 'Matriz', address: 'Capitan Edmundo Chiriboga y Jorge Paéz', lat: 0.153877, long: 78.489878}])

# Academic/Subjects
Admin::Academic::Subject::ContentType.create([{is_active: true, name: 'Proyecto'}, {is_active: true, name: 'Reto'}, {is_active: true, name: 'Reto en linea'}])

# Basic configs
Admin::Config::Language.create([{created_by_id: 1, is_active: true, name: 'Español'}, {created_by_id: 1, is_active: true, name: 'Ingles'}])

# Drive
Admin::Drive::Department.create([{is_active: true, name: 'Academico'}, {is_active: true, name: 'Biblioteca'}, {is_active: true, name: 'Talento Humano'}])

# User
Admin::User::Gender.create([{is_active: true, name: 'Masculino'}, {is_active: true, name: 'Femenino'}, {is_active: true, name: 'No especificar'}])
Admin::User::IdentificationType.create([{created_by_id: 1, name: 'Username'}, {created_by_id: 1, name: 'Pasaporte'}, {created_by_id: 1, name: 'Cédula'}])
Admin::User::Role.create([{name: 'Estudiante'}, {name: 'Profesor'}, {name: 'Bienestar'}, {name: 'Talento humano'}, {name: 'Coordinador'}, {name: 'Administrador'}, {name: 'Asistente'}, {name: 'Rector'}, {name: 'Vicerrector'}, {name: 'Bibliotecario'}, {name: 'Empleador'}])
Admin::User::Status.create([{name: 'Activo'}, {name: 'Bloqueado por pago'}, {name: 'Inactivo en el periodo'}, {name: 'No matriculado'}, {name: 'Graduado'}, {name: 'Matriculado'}, {name: 'Inactivo'}])
Admin::User::User.create(status_id: 2, role_id: 1, gender_id: 1, identification_type_id: 1, created_by_admin: true, identification_number: 'admin', password: 'admin', first_name: 'Leonardo', first_last_name: ' Barroeta', birth_date: '1990-12-10', custom_token: nil, mobile_number: '0979258160', house_number: '022814383', email: 'barroetaleonardo@hotmail.com')

# Library
Admin::Library::Book::Origin.create([{created_by_id: 1, name: 'Compra', is_active: true}, {created_by_id: 1, name: 'Donación', is_active: true}, {created_by_id: 1, name: 'Canje', is_active: true}])
Admin::Library::Book::Type.create([{created_by_id: 1, name: 'Fisico', is_active: true}, {created_by_id: 1, name: 'Digital', is_active: true}, {created_by_id: 1, name: 'Virtual', is_active: true}])
Admin::Library::Book::PublishType.create([{created_by_id: 1, name: 'Libros', is_active: true}, {created_by_id: 1, name: 'Tesis', is_active: true}, {created_by_id: 1, name: 'Publicaciones periodicas', is_active: true}])

# Teacher survey
Admin::Survey::Teacher::Survey.create([{name: 'Estudiante'}, {name: 'Autoridad'}, {name: 'Auto evaluacion'}, {name: 'Par academico'}])
