class CreateAdminDriveShelves < ActiveRecord::Migration[6.0]
  def change
    create_table :admin_drive_shelves do |t|
      t.integer :created_by_id
      t.integer :period_id
      t.integer :department_id
      t.boolean :is_active
      t.string :name

      t.timestamps
    end
  end
end
