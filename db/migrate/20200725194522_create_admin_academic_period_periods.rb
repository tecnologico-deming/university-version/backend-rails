class CreateAdminAcademicPeriodPeriods < ActiveRecord::Migration[6.0]
  def change
    create_table :admin_academic_period_periods do |t|
      t.integer :created_by_id
      t.boolean :is_active
      t.string :name
      t.date :start_date
      t.date :end_date

      t.timestamps
    end
  end
end
