class AddOrderToSubjectModuleContents < ActiveRecord::Migration[6.0]
  def change
    add_column :admin_academic_subject_contents, :order, :integer
  end
end
