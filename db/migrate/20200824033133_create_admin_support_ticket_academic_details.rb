class CreateAdminSupportTicketAcademicDetails < ActiveRecord::Migration[6.0]
  def change
    create_table :admin_support_ticket_academic_details do |t|
      t.integer :created_by_id
      t.integer :ticket_id

      t.timestamps
    end
  end
end
