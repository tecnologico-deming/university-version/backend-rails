class CreateAdminConfigCities < ActiveRecord::Migration[6.0]
  def change
    create_table :admin_config_cities do |t|
      t.integer :created_by_id
      t.integer :state_id
      t.string :name

      t.timestamps
    end
  end
end
