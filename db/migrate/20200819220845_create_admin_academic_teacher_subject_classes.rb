class CreateAdminAcademicTeacherSubjectClasses < ActiveRecord::Migration[6.0]
  def change
    create_table :admin_academic_teacher_subject_classes do |t|
      t.integer :activation_id
      t.integer :period_id
      t.integer :subject_id
      t.integer :group_id
      t.integer :teacher_id
      t.date :due_date
      t.date :exam_date
      t.timestamps
    end
  end
end
