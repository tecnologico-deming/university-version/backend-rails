class CreateAdminLibraryBookBookTags < ActiveRecord::Migration[6.0]
  def change
    create_table :admin_library_book_book_tags do |t|
      t.integer :book_id
      t.integer :tag_id

      t.timestamps
    end
  end
end
