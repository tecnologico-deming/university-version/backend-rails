class CreateAdminAcademicSubjectSubjects < ActiveRecord::Migration[6.0]
  def change
    create_table :admin_academic_subject_subjects do |t|
      t.integer :created_by_id
      t.integer :semester_id
      t.string :name
      t.string :code
      t.integer :credits
      t.float :hours

      t.timestamps
    end
  end
end
