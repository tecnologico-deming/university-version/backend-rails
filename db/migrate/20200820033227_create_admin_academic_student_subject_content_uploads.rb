class CreateAdminAcademicStudentSubjectContentUploads < ActiveRecord::Migration[6.0]
  def change
    create_table :admin_academic_student_subject_content_uploads do |t|
      t.integer :period_id
      t.integer :group_id
      t.integer :subject_id
      t.integer :module_id
      t.integer :content_id
      t.integer :teacher_id
      t.integer :student_id
      t.float :score
      t.text :teacher_comment

      t.timestamps
    end
  end
end
