class CreateAdminAcademicHeadquarterHeadquarters < ActiveRecord::Migration[6.0]
  def change
    create_table :admin_academic_headquarter_headquarters do |t|
      t.integer :created_by_id
      t.boolean :is_active
      t.string :name
      t.string :address
      t.float :lat
      t.float :long

      t.timestamps
    end
  end
end
