class CreateAdminLibraryLoanLoans < ActiveRecord::Migration[6.0]
  def change
    create_table :admin_library_loan_loans do |t|
      t.integer :reserve_id
      t.integer :was_received_by_id
      t.boolean :was_received
      t.datetime :was_received_date
      t.date :return_date
      t.timestamps
    end
  end
end
