class CreateAdminAcademicGroupGroups < ActiveRecord::Migration[6.0]
  def change
    create_table :admin_academic_group_groups do |t|
      t.integer :created_by_id
      t.integer :period_id
      t.integer :city_id
      t.string :name

      t.timestamps
    end
  end
end
