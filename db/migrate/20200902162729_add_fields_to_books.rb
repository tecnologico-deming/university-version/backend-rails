class AddFieldsToBooks < ActiveRecord::Migration[6.0]
  def change
    add_column :admin_library_book_books, :copies, :integer
    remove_column :admin_library_book_books, :cutter_code
    remove_column :admin_library_book_books, :code_control
  end
end
