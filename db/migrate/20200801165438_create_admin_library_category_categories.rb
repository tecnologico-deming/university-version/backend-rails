class CreateAdminLibraryCategoryCategories < ActiveRecord::Migration[6.0]
  def change
    create_table :admin_library_category_categories do |t|
      t.integer :created_by_id
      t.boolean :is_active
      t.string :code
      t.string :name

      t.timestamps
    end
  end
end
