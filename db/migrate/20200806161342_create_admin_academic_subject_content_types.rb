class CreateAdminAcademicSubjectContentTypes < ActiveRecord::Migration[6.0]
  def change
    create_table :admin_academic_subject_content_types do |t|
      t.boolean :is_active
      t.string :name

      t.timestamps
    end
  end
end
