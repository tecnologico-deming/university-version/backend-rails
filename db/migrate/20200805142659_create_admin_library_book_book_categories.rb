class CreateAdminLibraryBookBookCategories < ActiveRecord::Migration[6.0]
  def change
    create_table :admin_library_book_book_categories do |t|
      t.integer :book_id
      t.integer :category_id

      t.timestamps
    end
  end
end
