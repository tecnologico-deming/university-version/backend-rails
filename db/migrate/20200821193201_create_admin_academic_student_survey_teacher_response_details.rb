class CreateAdminAcademicStudentSurveyTeacherResponseDetails < ActiveRecord::Migration[6.0]
  def change
    create_table :admin_academic_student_survey_teacher_response_details do |t|
      t.integer :response_id
      t.integer :question_id
      t.integer :answer_id
      t.integer :score
      t.timestamps
    end
  end
end
