class CreateAdminSurveyTeacherQuestionTypes < ActiveRecord::Migration[6.0]
  def change
    create_table :admin_survey_teacher_question_types do |t|
      t.string :name

      t.timestamps
    end
  end
end
