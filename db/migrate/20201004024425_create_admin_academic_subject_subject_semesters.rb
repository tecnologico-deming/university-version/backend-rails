class CreateAdminAcademicSubjectSubjectSemesters < ActiveRecord::Migration[6.0]
  def change
    create_table :admin_academic_subject_subject_semesters do |t|
      t.integer :subject_id
      t.integer :semester_id

      t.timestamps
    end
  end
end
