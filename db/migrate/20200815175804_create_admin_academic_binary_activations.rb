class CreateAdminAcademicBinaryActivations < ActiveRecord::Migration[6.0]
  def change
    create_table :admin_academic_binary_activations do |t|
      t.integer :period_id
      t.integer :subject_id
      t.integer :studymode_id
      t.string :group_name
      t.string :student_identification_number
      t.string :teacher_identification_number
      t.string :student_exam_date
      t.string :student_extension_exam_date
      t.string :student_due_date
      t.string :teacher_due_date
      t.timestamps
    end
  end
end
