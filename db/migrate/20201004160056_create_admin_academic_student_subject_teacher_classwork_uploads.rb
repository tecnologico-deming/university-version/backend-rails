class CreateAdminAcademicStudentSubjectTeacherClassworkUploads < ActiveRecord::Migration[6.0]
  def change
    create_table :admin_academic_student_subject_teacher_classwork_uploads do |t|
      t.integer :classwork_id
      t.integer :student_id
      t.float :score
      t.text :comments

      t.timestamps
    end
  end
end
