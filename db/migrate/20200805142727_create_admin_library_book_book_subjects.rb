class CreateAdminLibraryBookBookSubjects < ActiveRecord::Migration[6.0]
  def change
    create_table :admin_library_book_book_subjects do |t|
      t.integer :book_id
      t.integer :subject_id

      t.timestamps
    end
  end
end
