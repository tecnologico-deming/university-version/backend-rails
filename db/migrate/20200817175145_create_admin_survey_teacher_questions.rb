class CreateAdminSurveyTeacherQuestions < ActiveRecord::Migration[6.0]
  def change
    create_table :admin_survey_teacher_questions do |t|
      t.integer :survey_id
      t.integer :question_type_id
      t.boolean :is_active
      t.text :title

      t.timestamps
    end
  end
end
