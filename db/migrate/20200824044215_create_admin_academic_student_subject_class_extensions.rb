class CreateAdminAcademicStudentSubjectClassExtensions < ActiveRecord::Migration[6.0]
  def change
    create_table :admin_academic_student_subject_class_extensions do |t|
      t.integer :created_by_id
      t.integer :student_class_id
      t.date :date

      t.timestamps
    end
  end
end
