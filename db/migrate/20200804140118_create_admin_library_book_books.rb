class CreateAdminLibraryBookBooks < ActiveRecord::Migration[6.0]
  def change
    create_table :admin_library_book_books do |t|
      t.integer :created_by_id
      t.integer :period_id
      t.integer :campus_id
      t.integer :editorial_id
      t.integer :language_id
      t.integer :main_author_id
      t.integer :origin_id
      t.integer :publish_type_id
      t.integer :row_id
      t.integer :secondary_author_id
      t.integer :type_id
      t.boolean :is_active
      t.string :code_dewey
      t.string :code_control
      t.string :cutter_code
      t.string :title
      t.string :subtitle
      t.integer :pages
      t.float :height
      t.date :publish_date
      t.string :publish_place
      t.timestamps
    end
  end
end
