class CreateAdminLibraryBookPublishTypes < ActiveRecord::Migration[6.0]
  def change
    create_table :admin_library_book_publish_types do |t|
      t.integer :created_by_id
      t.string :name
      t.boolean :is_active

      t.timestamps
    end
  end
end
