class AddFieldToVirtualRooms < ActiveRecord::Migration[6.0]
  def change
    add_column :admin_academic_class_classes, :room_code, :string
    add_column :admin_academic_class_classes, :server_id, :integer
  end
end
