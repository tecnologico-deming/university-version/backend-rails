class CreateAdminLibraryBookCopies < ActiveRecord::Migration[6.0]
  def change
    create_table :admin_library_book_copies do |t|
      t.integer :created_by_id
      t.integer :book_id
      t.boolean :is_active
      t.string :status

      t.timestamps
    end
  end
end
