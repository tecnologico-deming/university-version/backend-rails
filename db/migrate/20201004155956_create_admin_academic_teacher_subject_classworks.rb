class CreateAdminAcademicTeacherSubjectClassworks < ActiveRecord::Migration[6.0]
  def change
    create_table :admin_academic_teacher_subject_classworks do |t|
      t.integer :period_id
      t.integer :teacher_id
      t.integer :group_id
      t.integer :subject_id
      t.string :title
      t.timestamps
    end
  end
end
