class AddContentTypeToStudentContentUploads < ActiveRecord::Migration[6.0]
  def change
    add_column :admin_academic_student_subject_content_uploads, :content_type_id, :integer
  end
end
