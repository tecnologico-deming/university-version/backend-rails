class CreateAdminAcademicTeacherSurveyAutoResponses < ActiveRecord::Migration[6.0]
  def change
    create_table :admin_academic_teacher_survey_auto_responses do |t|
      t.integer :activation_id
      t.integer :period_id
      t.integer :survey_id
      t.integer :subject_id
      t.integer :group_id
      t.integer :teacher_id
      t.date :start_date
      t.datetime :fill_date
      t.float :score
      t.timestamps
    end
  end
end
