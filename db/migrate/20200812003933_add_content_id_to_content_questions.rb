class AddContentIdToContentQuestions < ActiveRecord::Migration[6.0]
  def change
    add_column :admin_academic_subject_content_questions, :content_id, :integer
    add_column :admin_academic_subject_content_question_answers, :content_question_id, :integer
    add_column :admin_academic_subject_content_question_answers, :is_correct, :boolean
    remove_column :admin_academic_subject_content_question_answers, :question_id
  end
end
