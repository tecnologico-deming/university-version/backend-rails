class CreateAdminConfigStates < ActiveRecord::Migration[6.0]
  def change
    create_table :admin_config_states do |t|
      t.integer :created_by_id
      t.integer :country_id
      t.string :name

      t.timestamps
    end
  end
end
