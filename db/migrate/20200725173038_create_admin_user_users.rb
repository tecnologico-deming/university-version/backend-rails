class CreateAdminUserUsers < ActiveRecord::Migration[6.0]
  def change
    create_table :admin_user_users do |t|
      t.integer :status_id
      t.integer :role_id
      t.integer :gender_id
      t.integer :identification_type_id
      t.boolean :created_by_admin
      t.string :identification_number
      t.string :password_digest
      t.string :full_name
      t.date :birth_date
      t.string :custom_token
      t.string :mobile_number
      t.string :house_number
      t.string :email

      t.timestamps
    end
  end
end
