class CreateAdminSupportTicketTypes < ActiveRecord::Migration[6.0]
  def change
    create_table :admin_support_ticket_types do |t|
      t.integer :created_by_id
      t.boolean :is_active
      t.string :title

      t.timestamps
    end
  end
end
