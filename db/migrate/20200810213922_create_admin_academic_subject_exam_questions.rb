class CreateAdminAcademicSubjectExamQuestions < ActiveRecord::Migration[6.0]
  def change
    create_table :admin_academic_subject_exam_questions do |t|
      t.integer :created_by_id
      t.integer :exam_id
      t.boolean :is_active

      t.timestamps
    end
  end
end
