class CreateAdminAcademicSubjectContentQuestionAnswers < ActiveRecord::Migration[6.0]
  def change
    create_table :admin_academic_subject_content_question_answers do |t|
      t.integer :question_id
      t.boolean :is_active
      t.string :title

      t.timestamps
    end
  end
end
