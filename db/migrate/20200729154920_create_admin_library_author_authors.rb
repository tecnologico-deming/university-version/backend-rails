class CreateAdminLibraryAuthorAuthors < ActiveRecord::Migration[6.0]
  def change
    create_table :admin_library_author_authors do |t|
      t.integer :created_by_id
      t.boolean :is_active
      t.string :code
      t.string :last_name
      t.string :first_name

      t.timestamps
    end
  end
end
