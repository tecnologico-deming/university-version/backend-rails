class RemoveColumnForBookEditorials < ActiveRecord::Migration[6.0]
  def change
    remove_column :admin_library_editorial_editorials, :country_id
    remove_column :admin_library_editorial_editorials, :address
    add_column :admin_library_editorial_editorials, :city_id, :integer
  end
end
