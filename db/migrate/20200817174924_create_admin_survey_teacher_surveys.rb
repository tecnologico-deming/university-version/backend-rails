class CreateAdminSurveyTeacherSurveys < ActiveRecord::Migration[6.0]
  def change
    create_table :admin_survey_teacher_surveys do |t|
      t.string :name

      t.timestamps
    end
  end
end
