class CreateAdminAcademicStudentExamRegulars < ActiveRecord::Migration[6.0]
  def change
    create_table :admin_academic_student_exam_regulars do |t|
      t.integer :activation_id
      t.integer :period_id
      t.integer :subject_id
      t.integer :group_id
      t.integer :teacher_id
      t.integer :student_id
      t.boolean :teacher_survey_complete
      t.date :start_date
      t.date :dead_line
      t.date :end_date
      t.date :delayed_date
      t.float :score
      t.timestamps
    end
  end
end
