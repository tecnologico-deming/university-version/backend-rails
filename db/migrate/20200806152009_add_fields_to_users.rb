class AddFieldsToUsers < ActiveRecord::Migration[6.0]
  def change
    add_column :admin_user_users, :password_changed, :boolean
    add_column :admin_user_users, :row_id, :integer
  end
end
