class CreateAdminAcademicStudentSurveyTeacherResponses < ActiveRecord::Migration[6.0]
  def change
    create_table :admin_academic_student_survey_teacher_responses do |t|
      t.integer :activation_id
      t.integer :period_id
      t.integer :survey_id
      t.integer :subject_id
      t.integer :group_id
      t.integer :teacher_id
      t.integer :student_id
      t.date :start_date
      t.datetime :fill_date
      t.float :score
      t.timestamps
    end
  end
end
