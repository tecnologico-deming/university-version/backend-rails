class CreateAdminLibraryLoanReserves < ActiveRecord::Migration[6.0]
  def change
    create_table :admin_library_loan_reserves do |t|
      t.integer :period_id
      t.integer :user_id
      t.integer :copy_id
      t.integer :is_approved_by_id
      t.boolean :is_approved
      t.text :comments

      t.timestamps
    end
  end
end
