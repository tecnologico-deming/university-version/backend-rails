class CreateAdminLibraryShelfRows < ActiveRecord::Migration[6.0]
  def change
    create_table :admin_library_shelf_rows do |t|
      t.integer :created_by_id
      t.integer :shelf_id
      t.boolean :is_active
      t.string :name

      t.timestamps
    end
  end
end
