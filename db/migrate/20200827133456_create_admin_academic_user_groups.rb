class CreateAdminAcademicUserGroups < ActiveRecord::Migration[6.0]
  def change
    create_table :admin_academic_user_groups do |t|
      t.integer :created_by_id
      t.integer :user_id
      t.integer :group_id

      t.timestamps
    end
  end
end
