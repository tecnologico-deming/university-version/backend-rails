class AddNeedUpdateFieldToUsers < ActiveRecord::Migration[6.0]
  def change
    add_column :admin_user_users, :need_update, :boolean
  end
end
