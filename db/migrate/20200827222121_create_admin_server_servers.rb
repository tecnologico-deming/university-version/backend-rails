class CreateAdminServerServers < ActiveRecord::Migration[6.0]
  def change
    create_table :admin_server_servers do |t|
      t.integer :created_by_id
      t.boolean :is_active
      t.string :name
      t.string :base_url
      t.string :secret_key

      t.timestamps
    end
  end
end
