class CreateAdminAcademicClassClasses < ActiveRecord::Migration[6.0]
  def change
    create_table :admin_academic_class_classes do |t|
      t.integer :activation_id
      t.integer :period_id
      t.integer :group_id
      t.integer :subject_id
      t.integer :teacher_id
      t.datetime :date
      t.timestamps
    end
  end
end
