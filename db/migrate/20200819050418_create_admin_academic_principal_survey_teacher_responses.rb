class CreateAdminAcademicPrincipalSurveyTeacherResponses < ActiveRecord::Migration[6.0]
  def change
    create_table :admin_academic_principal_survey_teacher_responses do |t|
      t.integer :period_id
      t.integer :activation_id
      t.integer :survey_id
      t.integer :rector_id
      t.integer :teacher_id
      t.datetime :fill_date
      t.float :score
      t.timestamps
    end
  end
end
