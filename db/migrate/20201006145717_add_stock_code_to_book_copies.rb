class AddStockCodeToBookCopies < ActiveRecord::Migration[6.0]
  def change
    add_column :admin_library_book_copies, :stock_code, :integer
    add_column :admin_library_book_copies, :edition_number, :string
  end
end
