class RenameFieldsToBooks < ActiveRecord::Migration[6.0]
  def change
    remove_column :admin_library_book_books, :publish_date
    add_column :admin_library_book_books, :publish_date, :integer
  end
end
