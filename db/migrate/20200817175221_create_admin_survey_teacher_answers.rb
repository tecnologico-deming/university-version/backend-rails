class CreateAdminSurveyTeacherAnswers < ActiveRecord::Migration[6.0]
  def change
    create_table :admin_survey_teacher_answers do |t|
      t.integer :question_id
      t.text :title
      t.float :score

      t.timestamps
    end
  end
end
