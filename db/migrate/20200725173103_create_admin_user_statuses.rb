class CreateAdminUserStatuses < ActiveRecord::Migration[6.0]
  def change
    create_table :admin_user_statuses do |t|
      t.string :name

      t.timestamps
    end
  end
end
