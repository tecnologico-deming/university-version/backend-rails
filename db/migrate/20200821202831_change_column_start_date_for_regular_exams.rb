class ChangeColumnStartDateForRegularExams < ActiveRecord::Migration[6.0]
  def change
    change_column :admin_academic_student_exam_regulars, :start_date, :datetime
  end
end
