class CreateAdminAcademicCareerCareers < ActiveRecord::Migration[6.0]
  def change
    create_table :admin_academic_career_careers do |t|
      t.integer :created_by_id
      t.integer :period_id
      t.boolean :is_active
      t.string :name
      t.string :code
      t.string :credits

      t.timestamps
    end
  end
end
