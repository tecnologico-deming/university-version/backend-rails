class CreateAdminAcademicClassClassDates < ActiveRecord::Migration[6.0]
  def change
    create_table :admin_academic_class_class_dates do |t|
      t.integer :created_by_id
      t.integer :class_id
      t.integer :server_id
      t.string :room_code
      t.date :date
      t.timestamps
    end
  end
end
