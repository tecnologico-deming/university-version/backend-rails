class CreateAdminAcademicStudentSubjectContentResponseDetails < ActiveRecord::Migration[6.0]
  def change
    create_table :admin_academic_student_subject_content_response_details do |t|
      t.integer :response_id
      t.integer :question_id
      t.integer :answer_id
      t.boolean :is_correct
      t.timestamps
    end
  end
end
