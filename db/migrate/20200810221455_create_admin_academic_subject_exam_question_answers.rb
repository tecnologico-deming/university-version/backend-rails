class CreateAdminAcademicSubjectExamQuestionAnswers < ActiveRecord::Migration[6.0]
  def change
    create_table :admin_academic_subject_exam_question_answers do |t|
      t.integer :exam_question_id
      t.boolean :is_correct
      t.text :title

      t.timestamps
    end
  end
end
