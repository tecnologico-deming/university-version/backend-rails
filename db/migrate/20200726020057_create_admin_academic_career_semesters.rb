class CreateAdminAcademicCareerSemesters < ActiveRecord::Migration[6.0]
  def change
    create_table :admin_academic_career_semesters do |t|
      t.integer :created_by_id
      t.integer :career_id
      t.integer :order
      t.string :name
      t.float :hours
      t.integer :credits

      t.timestamps
    end
  end
end
