class CreateAdminUserRoles < ActiveRecord::Migration[6.0]
  def change
    create_table :admin_user_roles do |t|
      t.integer :created_by_id
      t.boolean :is_active
      t.string :name

      t.timestamps
    end
  end
end
