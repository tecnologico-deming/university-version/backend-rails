class CreateAdminLibraryEditorialEditorials < ActiveRecord::Migration[6.0]
  def change
    create_table :admin_library_editorial_editorials do |t|
      t.integer :created_by_id
      t.integer :country_id
      t.boolean :is_active
      t.string :name
      t.text :address

      t.timestamps
    end
  end
end
