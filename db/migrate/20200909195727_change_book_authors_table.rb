class ChangeBookAuthorsTable < ActiveRecord::Migration[6.0]
  def change
    rename_column :admin_library_author_authors, :last_name, :full_name
    remove_column :admin_library_author_authors, :first_name
  end
end
