class CreateAdminAcademicBinaryActivationDates < ActiveRecord::Migration[6.0]
  def change
    create_table :admin_academic_binary_activation_dates do |t|
      t.integer :activation_id
      t.datetime :date

      t.timestamps
    end
  end
end
