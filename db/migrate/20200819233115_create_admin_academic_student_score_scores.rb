class CreateAdminAcademicStudentScoreScores < ActiveRecord::Migration[6.0]
  def change
    create_table :admin_academic_student_score_scores do |t|
      t.integer :activation_id
      t.integer :period_id
      t.integer :subject_id
      t.integer :teacher_id
      t.integer :student_id
      t.string :final_status
      t.float :class_score
      t.float :assistance_score
      t.float :online_activities_score
      t.float :exam_score
      t.float :extension_exam_score
      t.float :remedial_exam_score
      t.float :total_score
      t.timestamps
    end
  end
end
