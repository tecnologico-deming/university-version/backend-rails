class CreateAdminSupportTicketDetails < ActiveRecord::Migration[6.0]
  def change
    create_table :admin_support_ticket_details do |t|
      t.integer :ticket_id
      t.integer :created_by_id
      t.timestamps
    end
  end
end
