class RemoveLibraryShelfAndRowsTable < ActiveRecord::Migration[6.0]
  def change
    drop_table :admin_library_shelf_shelves
    drop_table :admin_library_shelf_rows
  end
end
