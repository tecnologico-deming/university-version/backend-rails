class CreateAdminDriveRows < ActiveRecord::Migration[6.0]
  def change
    create_table :admin_drive_rows do |t|
      t.integer :shelf_id
      t.string :name

      t.timestamps
    end
  end
end
