class CreateAdminAcademicSubjectExams < ActiveRecord::Migration[6.0]
  def change
    create_table :admin_academic_subject_exams do |t|
      t.integer :created_by_id
      t.integer :period_id
      t.integer :subject_id
      t.boolean :is_active
      t.string :name

      t.timestamps
    end
  end
end
