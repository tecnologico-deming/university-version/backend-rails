class SeparateFullNameAtUsersTable < ActiveRecord::Migration[6.0]
  def change
    remove_column :admin_user_users, :full_name
    add_column :admin_user_users, :first_last_name, :string
    add_column :admin_user_users, :second_last_name, :string
    add_column :admin_user_users, :first_name, :string
    add_column :admin_user_users, :second_name, :string
  end
end
