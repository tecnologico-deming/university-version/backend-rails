class AddFieldsToStudentUploads < ActiveRecord::Migration[6.0]
  def change
    add_column :admin_academic_student_subject_content_uploads, :content_type_name, :string
    add_column :admin_academic_student_subject_content_uploads, :module_name, :string
  end
end
