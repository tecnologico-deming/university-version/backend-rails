class CreateAdminDriveDepartments < ActiveRecord::Migration[6.0]
  def change
    create_table :admin_drive_departments do |t|
      t.boolean :is_active
      t.string :name

      t.timestamps
    end
  end
end
