class CreateAdminSupportTickets < ActiveRecord::Migration[6.0]
  def change
    create_table :admin_support_tickets do |t|
      t.integer :period_id
      t.integer :type_id
      t.integer :created_by_id
      t.integer :assigned_to_id
      t.string :code
      t.string :status
      t.string :title
      t.timestamps
    end
  end
end
