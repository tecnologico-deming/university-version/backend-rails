class CreateAdminAcademicSubjectContentQuestions < ActiveRecord::Migration[6.0]
  def change
    create_table :admin_academic_subject_content_questions do |t|
      t.integer :created_by_id
      t.integer :period_id
      t.boolean :is_active

      t.timestamps
    end
  end
end
