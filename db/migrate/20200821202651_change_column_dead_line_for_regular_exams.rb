class ChangeColumnDeadLineForRegularExams < ActiveRecord::Migration[6.0]
  def change
    change_column :admin_academic_student_exam_regulars, :dead_line, :datetime
    change_column :admin_academic_student_exam_regulars, :end_date, :datetime
  end
end
