class CreateAdminAcademicSubjectModules < ActiveRecord::Migration[6.0]
  def change
    create_table :admin_academic_subject_modules do |t|
      t.integer :created_by_id
      t.integer :subject_id
      t.integer :period_id
      t.boolean :is_active
      t.integer :order
      t.string :name

      t.timestamps
    end
  end
end
