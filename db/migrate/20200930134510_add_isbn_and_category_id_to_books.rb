class AddIsbnAndCategoryIdToBooks < ActiveRecord::Migration[6.0]
  def change
    add_column :admin_library_book_books, :category_id, :integer
    add_column :admin_library_book_books, :isbn_code, :string
  end
end
