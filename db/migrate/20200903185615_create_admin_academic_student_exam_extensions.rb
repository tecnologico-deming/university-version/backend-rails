class CreateAdminAcademicStudentExamExtensions < ActiveRecord::Migration[6.0]
  def change
    create_table :admin_academic_student_exam_extensions do |t|
      t.integer :created_by_id
      t.integer :period_id
      t.integer :subject_id
      t.integer :student_id
      t.datetime :start_date
      t.datetime :end_date
      t.datetime :dead_line
      t.float :score
      t.timestamps
    end
  end
end
