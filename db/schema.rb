# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `rails
# db:schema:load`. When creating a new database, `rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2021_03_10_193631) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "action_text_rich_texts", force: :cascade do |t|
    t.string "name", null: false
    t.text "body"
    t.string "record_type", null: false
    t.bigint "record_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["record_type", "record_id", "name"], name: "index_action_text_rich_texts_uniqueness", unique: true
  end

  create_table "active_storage_attachments", force: :cascade do |t|
    t.string "name", null: false
    t.string "record_type", null: false
    t.bigint "record_id", null: false
    t.bigint "blob_id", null: false
    t.datetime "created_at", null: false
    t.index ["blob_id"], name: "index_active_storage_attachments_on_blob_id"
    t.index ["record_type", "record_id", "name", "blob_id"], name: "index_active_storage_attachments_uniqueness", unique: true
  end

  create_table "active_storage_blobs", force: :cascade do |t|
    t.string "key", null: false
    t.string "filename", null: false
    t.string "content_type"
    t.text "metadata"
    t.bigint "byte_size", null: false
    t.string "checksum", null: false
    t.datetime "created_at", null: false
    t.index ["key"], name: "index_active_storage_blobs_on_key", unique: true
  end

  create_table "admin_academic_binary_activation_dates", force: :cascade do |t|
    t.integer "activation_id"
    t.datetime "date"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "admin_academic_binary_activations", force: :cascade do |t|
    t.integer "period_id"
    t.integer "subject_id"
    t.integer "studymode_id"
    t.string "group_name"
    t.string "student_identification_number"
    t.string "teacher_identification_number"
    t.string "student_exam_date"
    t.string "student_extension_exam_date"
    t.string "student_due_date"
    t.string "teacher_due_date"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "admin_academic_career_careers", force: :cascade do |t|
    t.integer "created_by_id"
    t.integer "period_id"
    t.boolean "is_active"
    t.string "name"
    t.string "code"
    t.string "credits"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "admin_academic_career_semesters", force: :cascade do |t|
    t.integer "created_by_id"
    t.integer "career_id"
    t.integer "order"
    t.string "name"
    t.float "hours"
    t.integer "credits"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "admin_academic_class_class_dates", force: :cascade do |t|
    t.integer "created_by_id"
    t.integer "class_id"
    t.integer "server_id"
    t.string "room_code"
    t.date "date"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "admin_academic_class_classes", force: :cascade do |t|
    t.integer "activation_id"
    t.integer "period_id"
    t.integer "group_id"
    t.integer "subject_id"
    t.integer "teacher_id"
    t.datetime "date"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.string "room_code"
    t.integer "server_id"
  end

  create_table "admin_academic_group_groups", force: :cascade do |t|
    t.integer "created_by_id"
    t.integer "period_id"
    t.integer "city_id"
    t.string "name"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "admin_academic_headquarter_headquarters", force: :cascade do |t|
    t.integer "created_by_id"
    t.boolean "is_active"
    t.string "name"
    t.string "address"
    t.float "lat"
    t.float "long"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "admin_academic_period_periods", force: :cascade do |t|
    t.integer "created_by_id"
    t.boolean "is_active"
    t.string "name"
    t.date "start_date"
    t.date "end_date"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "admin_academic_principal_survey_teacher_responses", force: :cascade do |t|
    t.integer "period_id"
    t.integer "activation_id"
    t.integer "survey_id"
    t.integer "rector_id"
    t.integer "teacher_id"
    t.datetime "fill_date"
    t.float "score"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "admin_academic_student_exam_extension_details", force: :cascade do |t|
    t.integer "exam_id"
    t.integer "question_id"
    t.integer "answer_id"
    t.boolean "is_correct"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "admin_academic_student_exam_extensions", force: :cascade do |t|
    t.integer "created_by_id"
    t.integer "period_id"
    t.integer "subject_id"
    t.integer "student_id"
    t.datetime "start_date"
    t.datetime "end_date"
    t.datetime "dead_line"
    t.float "score"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "admin_academic_student_exam_regular_details", force: :cascade do |t|
    t.integer "exam_id"
    t.integer "question_id"
    t.integer "answer_id"
    t.boolean "is_correct"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "admin_academic_student_exam_regulars", force: :cascade do |t|
    t.integer "activation_id"
    t.integer "period_id"
    t.integer "subject_id"
    t.integer "group_id"
    t.integer "teacher_id"
    t.integer "student_id"
    t.boolean "teacher_survey_complete"
    t.datetime "start_date"
    t.datetime "dead_line"
    t.datetime "end_date"
    t.date "delayed_date"
    t.float "score"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "admin_academic_student_score_scores", force: :cascade do |t|
    t.integer "activation_id"
    t.integer "period_id"
    t.integer "subject_id"
    t.integer "teacher_id"
    t.integer "student_id"
    t.string "final_status"
    t.float "class_score"
    t.float "assistance_score"
    t.float "online_activities_score"
    t.float "exam_score"
    t.float "extension_exam_score"
    t.float "remedial_exam_score"
    t.float "total_score"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "admin_academic_student_subject_class_extensions", force: :cascade do |t|
    t.integer "created_by_id"
    t.integer "student_class_id"
    t.date "date"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "admin_academic_student_subject_classes", force: :cascade do |t|
    t.integer "activation_id"
    t.integer "period_id"
    t.integer "subject_id"
    t.integer "group_id"
    t.integer "teacher_id"
    t.integer "student_id"
    t.date "due_date"
    t.date "exam_date"
    t.date "extension_exam_date"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "admin_academic_student_subject_classwork_uploads", force: :cascade do |t|
    t.integer "period_id"
    t.integer "subject_id"
    t.integer "teacher_id"
    t.integer "student_id"
    t.integer "group_id"
    t.integer "classwork_id"
    t.float "score"
    t.text "comment"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "admin_academic_student_subject_content_response_details", force: :cascade do |t|
    t.integer "response_id"
    t.integer "question_id"
    t.integer "answer_id"
    t.boolean "is_correct"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "admin_academic_student_subject_content_responses", force: :cascade do |t|
    t.integer "period_id"
    t.integer "group_id"
    t.integer "subject_id"
    t.integer "module_id"
    t.integer "content_id"
    t.integer "student_id"
    t.float "score"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "admin_academic_student_subject_content_uploads", force: :cascade do |t|
    t.integer "period_id"
    t.integer "group_id"
    t.integer "subject_id"
    t.integer "module_id"
    t.integer "content_id"
    t.integer "teacher_id"
    t.integer "student_id"
    t.float "score"
    t.text "teacher_comment"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.integer "content_type_id"
    t.string "content_type_name"
    t.string "module_name"
  end

  create_table "admin_academic_student_subject_teacher_classwork_uploads", force: :cascade do |t|
    t.integer "classwork_id"
    t.integer "student_id"
    t.float "score"
    t.text "comments"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "admin_academic_student_survey_teacher_response_details", force: :cascade do |t|
    t.integer "response_id"
    t.integer "question_id"
    t.integer "answer_id"
    t.integer "score"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "admin_academic_student_survey_teacher_responses", force: :cascade do |t|
    t.integer "activation_id"
    t.integer "period_id"
    t.integer "survey_id"
    t.integer "subject_id"
    t.integer "group_id"
    t.integer "teacher_id"
    t.integer "student_id"
    t.date "start_date"
    t.datetime "fill_date"
    t.float "score"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "admin_academic_subject_classworks", force: :cascade do |t|
    t.integer "created_by_id"
    t.integer "period_id"
    t.integer "subject_id"
    t.boolean "is_active"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "admin_academic_subject_content_question_answers", force: :cascade do |t|
    t.boolean "is_active"
    t.string "title"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.integer "content_question_id"
    t.boolean "is_correct"
  end

  create_table "admin_academic_subject_content_questions", force: :cascade do |t|
    t.integer "created_by_id"
    t.integer "period_id"
    t.boolean "is_active"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.integer "content_id"
  end

  create_table "admin_academic_subject_content_types", force: :cascade do |t|
    t.boolean "is_active"
    t.string "name"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "admin_academic_subject_contents", force: :cascade do |t|
    t.integer "created_by_id"
    t.integer "period_id"
    t.integer "module_id"
    t.integer "type_id"
    t.boolean "is_active"
    t.string "name"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.integer "order"
  end

  create_table "admin_academic_subject_exam_question_answers", force: :cascade do |t|
    t.integer "exam_question_id"
    t.boolean "is_correct"
    t.text "title"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "admin_academic_subject_exam_questions", force: :cascade do |t|
    t.integer "created_by_id"
    t.integer "exam_id"
    t.boolean "is_active"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "admin_academic_subject_exams", force: :cascade do |t|
    t.integer "created_by_id"
    t.integer "period_id"
    t.integer "subject_id"
    t.boolean "is_active"
    t.string "name"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "admin_academic_subject_guides", force: :cascade do |t|
    t.integer "created_by_id"
    t.integer "period_id"
    t.integer "subject_id"
    t.integer "teacher_id"
    t.boolean "is_active"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "admin_academic_subject_modules", force: :cascade do |t|
    t.integer "created_by_id"
    t.integer "subject_id"
    t.integer "period_id"
    t.boolean "is_active"
    t.integer "order"
    t.string "name"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "admin_academic_subject_standard_classes", force: :cascade do |t|
    t.integer "created_by_id"
    t.integer "period_id"
    t.integer "subject_id"
    t.integer "teacher_id"
    t.boolean "is_active"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "admin_academic_subject_subject_semesters", force: :cascade do |t|
    t.integer "subject_id"
    t.integer "semester_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "admin_academic_subject_subjects", force: :cascade do |t|
    t.integer "created_by_id"
    t.integer "semester_id"
    t.string "name"
    t.string "code"
    t.integer "credits"
    t.float "hours"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "admin_academic_teacher_subject_class_files", force: :cascade do |t|
    t.integer "period_id"
    t.integer "group_id"
    t.integer "subject_id"
    t.integer "teacher_id"
    t.string "title"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "admin_academic_teacher_subject_classes", force: :cascade do |t|
    t.integer "activation_id"
    t.integer "period_id"
    t.integer "subject_id"
    t.integer "group_id"
    t.integer "teacher_id"
    t.date "due_date"
    t.date "exam_date"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "admin_academic_teacher_subject_classworks", force: :cascade do |t|
    t.integer "period_id"
    t.integer "teacher_id"
    t.integer "group_id"
    t.integer "subject_id"
    t.string "title"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "admin_academic_teacher_survey_auto_responses", force: :cascade do |t|
    t.integer "activation_id"
    t.integer "period_id"
    t.integer "survey_id"
    t.integer "subject_id"
    t.integer "group_id"
    t.integer "teacher_id"
    t.date "start_date"
    t.datetime "fill_date"
    t.float "score"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "admin_academic_user_groups", force: :cascade do |t|
    t.integer "created_by_id"
    t.integer "user_id"
    t.integer "group_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "admin_config_cities", force: :cascade do |t|
    t.integer "created_by_id"
    t.integer "state_id"
    t.string "name"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "admin_config_countries", force: :cascade do |t|
    t.integer "created_by_id"
    t.boolean "is_active"
    t.string "name"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "admin_config_languages", force: :cascade do |t|
    t.integer "created_by_id"
    t.string "name"
    t.boolean "is_active"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "admin_config_states", force: :cascade do |t|
    t.integer "created_by_id"
    t.integer "country_id"
    t.string "name"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "admin_drive_departments", force: :cascade do |t|
    t.boolean "is_active"
    t.string "name"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "admin_drive_rows", force: :cascade do |t|
    t.integer "shelf_id"
    t.string "name"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "admin_drive_shelves", force: :cascade do |t|
    t.integer "created_by_id"
    t.integer "period_id"
    t.integer "department_id"
    t.boolean "is_active"
    t.string "name"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "admin_library_author_authors", force: :cascade do |t|
    t.integer "created_by_id"
    t.boolean "is_active"
    t.string "code"
    t.string "full_name"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "admin_library_book_book_categories", force: :cascade do |t|
    t.integer "book_id"
    t.integer "category_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "admin_library_book_book_subjects", force: :cascade do |t|
    t.integer "book_id"
    t.integer "subject_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "admin_library_book_book_tags", force: :cascade do |t|
    t.integer "book_id"
    t.integer "tag_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "admin_library_book_books", force: :cascade do |t|
    t.integer "created_by_id"
    t.integer "period_id"
    t.integer "campus_id"
    t.integer "editorial_id"
    t.integer "language_id"
    t.integer "main_author_id"
    t.integer "origin_id"
    t.integer "publish_type_id"
    t.integer "row_id"
    t.integer "secondary_author_id"
    t.integer "type_id"
    t.boolean "is_active"
    t.string "code_dewey"
    t.string "title"
    t.string "subtitle"
    t.integer "pages"
    t.float "height"
    t.string "publish_place"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.integer "copies"
    t.integer "publish_date"
    t.integer "category_id"
    t.string "isbn_code"
  end

  create_table "admin_library_book_copies", force: :cascade do |t|
    t.integer "created_by_id"
    t.integer "book_id"
    t.boolean "is_active"
    t.string "status"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.integer "stock_code"
    t.string "edition_number"
  end

  create_table "admin_library_book_origins", force: :cascade do |t|
    t.integer "created_by_id"
    t.string "name"
    t.boolean "is_active"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "admin_library_book_publish_types", force: :cascade do |t|
    t.integer "created_by_id"
    t.string "name"
    t.boolean "is_active"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "admin_library_book_types", force: :cascade do |t|
    t.integer "created_by_id"
    t.string "name"
    t.boolean "is_active"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "admin_library_category_categories", force: :cascade do |t|
    t.integer "created_by_id"
    t.boolean "is_active"
    t.string "code"
    t.string "name"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "admin_library_category_tags", force: :cascade do |t|
    t.integer "created_by_id"
    t.boolean "is_active"
    t.string "name"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "admin_library_editorial_editorials", force: :cascade do |t|
    t.integer "created_by_id"
    t.boolean "is_active"
    t.string "name"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.integer "city_id"
  end

  create_table "admin_library_loan_loans", force: :cascade do |t|
    t.integer "reserve_id"
    t.integer "was_received_by_id"
    t.boolean "was_received"
    t.datetime "was_received_date"
    t.date "return_date"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "admin_library_loan_reserves", force: :cascade do |t|
    t.integer "period_id"
    t.integer "user_id"
    t.integer "copy_id"
    t.integer "is_approved_by_id"
    t.boolean "is_approved"
    t.text "comments"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "admin_server_servers", force: :cascade do |t|
    t.integer "created_by_id"
    t.boolean "is_active"
    t.string "name"
    t.string "base_url"
    t.string "secret_key"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "admin_support_ticket_academic_details", force: :cascade do |t|
    t.integer "created_by_id"
    t.integer "ticket_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "admin_support_ticket_details", force: :cascade do |t|
    t.integer "ticket_id"
    t.integer "created_by_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "admin_support_ticket_meetings", force: :cascade do |t|
    t.integer "created_by_id"
    t.integer "ticket_id"
    t.datetime "date"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "admin_support_ticket_types", force: :cascade do |t|
    t.integer "created_by_id"
    t.boolean "is_active"
    t.string "title"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "admin_support_tickets", force: :cascade do |t|
    t.integer "period_id"
    t.integer "type_id"
    t.integer "created_by_id"
    t.integer "assigned_to_id"
    t.string "code"
    t.string "status"
    t.string "title"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "admin_survey_teacher_answers", force: :cascade do |t|
    t.integer "question_id"
    t.text "title"
    t.float "score"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "admin_survey_teacher_question_types", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "admin_survey_teacher_questions", force: :cascade do |t|
    t.integer "survey_id"
    t.integer "question_type_id"
    t.boolean "is_active"
    t.text "title"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "admin_survey_teacher_surveys", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "admin_user_genders", force: :cascade do |t|
    t.integer "created_by_id"
    t.boolean "is_active"
    t.string "name"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "admin_user_identification_types", force: :cascade do |t|
    t.integer "created_by_id"
    t.boolean "is_active"
    t.string "name"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "admin_user_roles", force: :cascade do |t|
    t.integer "created_by_id"
    t.boolean "is_active"
    t.string "name"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "admin_user_statuses", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "admin_user_users", force: :cascade do |t|
    t.integer "status_id"
    t.integer "role_id"
    t.integer "gender_id"
    t.integer "identification_type_id"
    t.boolean "created_by_admin"
    t.string "identification_number"
    t.string "password_digest"
    t.date "birth_date"
    t.string "custom_token"
    t.string "mobile_number"
    t.string "house_number"
    t.string "email"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.string "first_last_name"
    t.string "second_last_name"
    t.string "first_name"
    t.string "second_name"
    t.boolean "password_changed"
    t.integer "row_id"
    t.boolean "need_update"
  end

  add_foreign_key "active_storage_attachments", "active_storage_blobs", column: "blob_id"
end
