# == Schema Information
#
# Table name: admin_library_book_books
#
#  id                  :bigint           not null, primary key
#  created_by_id       :integer
#  period_id           :integer
#  campus_id           :integer
#  editorial_id        :integer
#  language_id         :integer
#  main_author_id      :integer
#  origin_id           :integer
#  publish_type_id     :integer
#  row_id              :integer
#  secondary_author_id :integer
#  type_id             :integer
#  is_active           :boolean
#  code_dewey          :string
#  title               :string
#  subtitle            :string
#  pages               :integer
#  height              :float
#  publish_place       :string
#  created_at          :datetime         not null
#  updated_at          :datetime         not null
#  copies              :integer
#  publish_date        :integer
#  category_id         :integer
#  isbn_code           :string
#
require 'test_helper'

class Admin::Library::Book::BookTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end
end
