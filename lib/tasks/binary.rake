require 'binary_api_client'

namespace :binary do
  task online_activities_score: :environment do
    Rails.logger.info 'Started task'
    Admin::Academic::Student::Score::Score.nil_online_activities_score
    Rails.logger.info 'Finished task'
  end

  task class_work_score: :environment do
    Rails.logger.info 'Started task'
    Admin::Academic::Student::Score::Score.nil_classwork_uploads
    Rails.logger.info 'Finished task'
  end
end
