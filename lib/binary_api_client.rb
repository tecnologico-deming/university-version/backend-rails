module Communicator
  module Binary
    class HTTP
      include HTTParty

      base_uri 'https://binary.ec'

      def self.make_call(path, options = {})
        method = options[:method] || :post
        Rails.logger.info "API call to BINARY: #{path}"

        Rails.logger.info "Payload: #{options}"

        if Rails.env == 'production'
          response = send(method, path, body: options[:data])
          Rails.logger.info response.body
          response.body
        else
          'API call mocked'
        end
      end

      def self.send_online_activities_score(options = {})
        path = '/ACADEMOS/deming/index.php/academico/academico/promedio_retos_proyectos'
        make_call(path, options)
      end

      # Exams
      def self.send_exam_score(options = {})
        path = '/ACADEMOS/deming/index.php/academico/academico/promedio_examen'
        make_call(path, options)
      end

      def self.send_extension_exam_score(options = {})
        path = '/ACADEMOS/deming/index.php/academico/academico/promedio_supletorio'
        make_call(path, options)
      end

      def self.send_remedial_exam_score(options = {})
        path = '/ACADEMOS/deming/index.php/academico/academico/promedio_supletorio'
        make_call(path, options)
      end

      # Classwork
      def self.send_classwork_score(options = {})
        path = '/ACADEMOS/deming/index.php/academico/academico/promedio_taller'
        make_call(path, options)
      end

      def self.send_admission(options = {})
        path = '/index.php/academico/academico/inscripcion_estudiante'
        make_call(path, options)
      end

    end
  end
end
